%vidObj = GetVideoObj(3);
N = vidObj.NumberOfFrames;
H = vidObj.Height;
W = vidObj.Width;

numOfPoints = 5;

pointW = floor(linspace(1, W, numOfPoints));
pointH = floor(H/2);
compVals = zeros(3, numOfPoints, N);

for i=floor(N*0.5):N*0.8
    for j=1:numOfPoints
        z = videoCube(:,:,:,i);% vidObj.read(i);
        z = z(pointH, pointW(j), :);
        
        compVals(1,j,i) = z(1);
        
        zz = rgb2ycbcr(z);
        compVals(2,j,i) = zz(1)*(255/234);
    
        zz = rgb2hsv(z);
        compVals(3,j,i) = zz(2)*255;
    end
end
%%
close all;
normFun = @(y) (y - min(y))./(max(y) - min(y));
[nrComps, nrPoints, nrFrames] = size(compVals);

X = 1:nrFrames;
Name = strrep(vidObj.Name,'_','-');

for p=1:nrPoints
    figure;
    for c=1:nrComps
        z = compVals(c,p,:);
        plot(X, normFun(z(:)))
        hold on;
    end
    title(['(' num2str(pointH) ',' num2str(pointW(p)) ') in ' Name]);
    legend('R in RGB','Y in YCbCr','S in HSV');
    xlabel('frame');
end

%%
normFun = @(y) (y - min(y))./(max(y) - min(y));

compVals = compValsCopy(:,:,1:176);

for p=1:numOfPoints
    %compVals(1,p,:) = compVals(1,p,:)./255;
    for c=2:3
        compVals(c,p,:) = normFun(compVals(c,p,:)).*255;
    end
end
    
    
    
    
    
