close all;
%clear all;

load('SavedMatFiles\ComponentSamplerInVideo.mat')

[nrComps, nrPoints, nrFrames] = size(compVals);

X = 1:nrFrames;

for p=1:nrPoints
    figure;
    for c=1:nrComps
        z = compVals(c,p,:);
        plot(X, z(:))
        hold on;
    end
    title(['(' num2str(pointW(p)) ',' num2str(pointH) ') in ' Name]);
    legend('R in RGB','Y in YCbCr','S in HSV');
    xlabel('frame');
end
