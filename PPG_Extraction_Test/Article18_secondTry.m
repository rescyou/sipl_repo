function [rawPPG_tec1, rawPPG_tec2, bpfPPG_tec1, bpfPPG_tec2] = ...
    Article18_secondTry(videoCube, frameRate, opt)
    %% Params
    [Sx, Sy, ~, N] = size(videoCube);
    p = 5;
    
    %% calc Threshold
    fewSec = round(frameRate * 2);
    tempVideo = zeros(Sx, Sy, fewSec);
    runner = floor(N/2 - fewSec/2):floor(N/2 + fewSec/2);
    for i=1:length(runner)
    	tempVideo(:,:,i) = GetFrameAs(videoCube(:,:,:,runner(i)), 1, opt);
    end
    T = prctile(tempVideo(:), p);
    if T == 255;
        T = 254;
    end
    
    %% calc raw PPG
    dist_mat = dist_matrix(Sx, Sy);
    rawPPG_tec1 = zeros(1, N);
    rawPPG_tec2 = zeros(1, N);

    for i=1:N
        z = GetFrameAs(videoCube(:,:,:,i), 1, opt);
        z = z > T;
        z = edge(z, 'sobel'); % or 'canny'

        %% Technic #1
        temp = dist_mat.*z;
        rawPPG_tec1(i) = mean(temp(:)); % or max

        %% Technic #2
        [xs, ys] = find(z);
        %[~,~,R,~] = circfit(xs, ys);
        x = xs(:); y = ys(:);
        a = [x y ones(size(x))] \ [-(x.^2+y.^2)];
        R = sqrt((a(1)^2+a(2)^2)/4-a(3));
        rawPPG_tec2(i) = R;
    end
    
    %% BPF
    bpfPPG_tec1 = butterBandpass(rawPPG_tec1 - mean(rawPPG_tec1), frameRate, 4, 0.3, 12.5);
    bpfPPG_tec2 = butterBandpass(rawPPG_tec2 - mean(rawPPG_tec2), frameRate, 4, 0.3, 12.5);

    %% Aux functions
    function   [xc,yc,R,a] = circfit(x,y)
    %
    %   [xc yx R] = circfit(x,y)
    %
    %   fits a circle  in x,y plane in a more accurate
    %   (less prone to ill condition )
    %  procedure than circfit2 but using more memory
    %  x,y are column vector where (x(i),y(i)) is a measured point
    %
    %  result is center point (yc,xc) and radius R
    %  an optional output is the vector of coeficient a
    % describing the circle's equation
    %
    %   x^2+y^2+a(1)*x+a(2)*y+a(3)=0
    %
    %  By:  Izhak bucher 25/oct /1991, 
        x = x(:); y = y(:);
        a = [x y ones(size(x))] \ [-(x.^2+y.^2)];
        xc = -.5*a(1);
        yc = -.5*a(2);
        R = sqrt((a(1)^2+a(2)^2)/4-a(3));
    end
end

