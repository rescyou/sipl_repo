function [ outPPG, baseline, lowPeaksLocs, outX ] = ...
    RemoveBaselineAlg( inPPG, clipEdges, draw, samplingRate)
%RemoveBaselineAlg removes baseline drift
%   inPPG       - the input signal
%   clipEdges   - if true then cuts off the edges that weren't detected by
%                   the cycles detector
%                   false by default
%   draw        - if true then plots the signal with the vertical lines at
%                   the peaks
%                   by default if there are no output args, then true

    if nargin < 4
        samplingRate = 1;
    end
    if nargin < 3
        draw = (nargout == 0);
    end
    if nargin < 2
        clipEdges = false;
    end

    lowPeaksLocs = cycle_detect(inPPG);
    
    [outPPG, baseline] = removeBaseline(inPPG, lowPeaksLocs);
    
    if clipEdges
        xRunner = lowPeaksLocs(1):lowPeaksLocs(end);
        outPPG = outPPG(xRunner);
        baseline = baseline(xRunner);
        lowPeaksLocs = lowPeaksLocs - lowPeaksLocs(1);
    end
    
    outX = (1:length(outPPG)) ./ samplingRate;

    if draw
        drawVerLinesOnGraph(lowPeaksLocs, outPPG, samplingRate);
    end

end

