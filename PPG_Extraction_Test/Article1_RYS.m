%clear all
clc

load('SavedMatFiles\Art1_RYS.mat');

%%
RED = [0.85 0.325 0.098];
YELLOW = [0.929 0.694 0.125];
BLUE = [0 0.447 0.741];

colors = {'r', 'm', 'b'};
colors = {RED, [0 218 21]./255, BLUE};
colors = {RED, [17 195 34]./255, BLUE};
colors = {RED, [195 134 17]./255, BLUE};
colors = {RED, YELLOW, BLUE};
colors = {RED, BLUE, YELLOW };

leg = {'R from RGB','Y from YCbCr','S from HSV'};

%%
factor = [1, 1.45, -1];
interval = 110:length(afterNoiseCleaning_RYS{1})-14;

factor = [1 1 -1];
interval = 105:length(afterNoiseCleaning_RYS{1})-10;

figure;
hold on;
runner = 3:-1:1;
for i=runner
    [ outPPG, baseline, lowPeaksLocs, outX ] = ...
        RemoveBaselineAlg( factor(i)*afterNoiseCleaning_RYS{i}, false, false, vidObj.FrameRate);
    
    h = plot(outX(interval), outPPG(interval));
    h.LineWidth = 2;
    h.Color = colors{i};
end
hold off;

h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
%h.Color = ones(3,1)*0.98;

h = legend(leg{runner});
h.FontSize = 18;
h.FontWeight = 'b';
%h.Color='w';

xlim([outX(interval(1)), 30]);
xlabel('time [sec]', 'FontSize', 23);
title('Technic #1 in Different Color Spaces', 'FontSize', 35);


