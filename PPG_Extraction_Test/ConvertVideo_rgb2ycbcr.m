function [ outvideo ] = ConvertVideo_rgb2ycbcr( videoCube )
N = size(videoCube,4);
outvideo = zeros(size(videoCube));
for i=1:N
  outvideo(:,:,:,i)=rgb2ycbcr(videoCube(:,:,:,i));  
end
end

