function [ vidObj, vidName ] = GetVideoObj( index )

    folder = 'FingerVideos';
    videoFilenames = dir([folder '\*.MOV']);
    videoFilenames = {videoFilenames.name};

    vidName = videoFilenames{index};    
    vidObj = VideoReader([folder '\' vidName]);
    
    if nargout <= 1
        disp(['video selected: ' videoFilenames{index}]);
    end

end

