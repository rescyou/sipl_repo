% Article 15 - PPG Extraction
function [outPPG] = TestArticle15(videoCube, opt, k)
    %% Param init
    N = size(videoCube, 4);
    if nargin <= 2
        k = 100;
    end
    if nargin <= 1
        opt = 0;
    end

    getFrameAt = @(i) GetFrameAs(videoCube, i, opt);
    
    %% Initialization stage - calc threshold
    trdQuartilIntensitys = zeros(1, k);
    for i=1:k
        frame = getFrameAt(i);
        trdQuartilIntensitys(i) = prctile(frame(:), 75);
    end
    
    adaptiveThreshold = min(trdQuartilIntensitys);

    %% Get R and remove bad frames
    R = zeros(1,N);
    Time = zeros(1,N);

    frameNumel = numel(videoCube(:,:,1,1));
    
    i = 1;% output time
    t = 1;% input video time
    while t<N
        v = getFrameAt(t) >= adaptiveThreshold;
        v = sum(v(:));
        
        if v < 0.25*frameNumel || v == frameNumel
            t = t + 1;
            continue;
        end

        R(i) = v;
        Time(i) = t;

        t = t + 1;
        i = i + 1;
    end

    R = R(1:i-1);
    Time = Time(1:i-1);

    %% Extraction of PPG
    outPPG = NaN(1,N);
    outPPG(Time) = (max(R) - R)./(max(R) - min(R));
    
    outPPG = max(outPPG) - outPPG;

end


