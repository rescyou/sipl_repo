%clear all
clc

%% Read Data
vidObj = GetVideoObj(3);
N = vidObj.NumberOfFrames;
videoCube = zeros(vidObj.Height, vidObj.Width, 3, vidObj.NumberOfFrames, 'uint8');

for i=1:N
    videoCube(:,:,:,i) = vidObj.read(i);
end

%% Run Tests;
for i=0:2
ppg_Art{i+1,1} = {TestArticle1(videoCube, vidObj.FrameRate,i)};%1
ppg_Art{i+1,2} = {TestArticle2(videoCube, vidObj.FrameRate,i)};%2
ppg_Art{i+1,3} = {TestArticle7(videoCube, vidObj.FrameRate,i)};%7
ppg_Art{i+1,4} = {TestArticle11(videoCube, vidObj.FrameRate, i)};%11
ppg_Art{i+1,5} = {TestArticle15(videoCube, i)};%15
ppg_Art{i+1,6} = {TestArticle18(videoCube, vidObj.FrameRate, i)};%18
end

%% Plot
normFun = @(y) (y - min(y))./(max(y) - min(y));

X = (1:N) ./ vidObj.FrameRate;
figure;
plot(X, normFun(ppg_Art1), ...
    X, normFun(RemoveBaselineAlg(ppg_Art2)), ...
    X, normFun(RemoveBaselineAlg(-ppg_Art7)), ...
    X, normFun(RemoveBaselineAlg(ppg_Art11)), ...
    X, normFun(RemoveBaselineAlg(ppg_Art15)), ...
    X, normFun(ppg_Art18));
title('PPG signal');
legend('1', '2', '7', '11', '15', '18');
xlabel('Time [sec]');
%%
normFun = @(y) (y - min(y))./(max(y) - min(y));
%normFun = @(y) y-mean(y);

articles = {'article 1','article 2','article 7','article 11','article 15','article 18'};
for j = 1:6
    figure
    for i=1:3
       z = ppg_Art{i,j};
       plot(X,normFun(z{:}));
       hold on;
    end
    legend('r','y','s');
    title(articles{j});
    hold off;
end