%clear all
clc

load('SavedMatFiles\Art1_RYS.mat');
fr = vidObj.FrameRate;

%%
RED = [0.85 0.325 0.098];
YELLOW = [0.929 0.694 0.125];
BLUE = [0 0.447 0.741];

colors = {RED, BLUE, YELLOW};

leg = {'R from RGB','Y from YCbCr','S from HSV'};

%%
figure;

%% article 1
subplot(2, 1, 1);
factor = [1, 1.45, -1];
interval = 110:length(afterNoiseCleaning_RYS{1})-14;

factor = [1 1 -1];
interval = 105:length(afterNoiseCleaning_RYS{1})-10;

hold on;
runner = 3:-1:1;
for i=runner
    [ outPPG, baseline, lowPeaksLocs, outX ] = ...
        RemoveBaselineAlg( factor(i)*afterNoiseCleaning_RYS{i}, false, false, fr);
    
    h = plot(outX(interval), outPPG(interval));
    h.LineWidth = 2;
    h.Color = colors{i};
end
hold off;

h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
%h.Color = ones(3,1)*0.98;

h = legend(leg{runner});
h.FontSize = 18;
h.FontWeight = 'b';
%h.Color='w';

xlim([outX(interval(1)), 30]);
xlabel('time [sec]', 'FontSize', 23);
title('Technic #1 in Different Color Spaces', 'FontSize', 35);

%% article 15
subplot(2, 1, 2);

load('SavedMatFiles\ppg_Art_Vr2.mat');

ppgs = cell(1, 3);
for i=1:3
    z = ppg_Art{i, ArtIndex};
    ppgs{i} = z{:};
end
ppgs{3} = max(ppgs{3})-ppgs{3};

%------------------------------------------------------------
runner = 1:3;
hold on;

N = length(ppgs{1});

intervals = 418:N-1;
X = (1:N) ./ fr;

for i=runner
    interval = find(isnan(ppgs{i}));
    if length(interval) == 1
        interval = 105:interval(end)-1;
    else
        interval = max(interval(end-1)+1,105):interval(end)-1;
    end
    
    signal = ppgs{i}(interval);
    signal = butterBandpass(signal-mean(signal), fr, 4, 0.5, 5);
    [ outPPG, baseline, lowPeaksLocs, outX ] = ...
        RemoveBaselineAlg( signal, false, false, fr);
    
    h = plot(X(interval), outPPG);
    h.LineWidth = 2;
    h.Color = colors{i};
end
yl = ylim;
ylim([0, yl(2)]);

h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
%h.Color = ones(3,1)*0.98;

h = legend(leg{runner});
h.FontSize = 18;
h.FontWeight = 'b';
%h.Color='w';

xlim([outX(105), 30]);
xlabel('time [sec]', 'FontSize', 23);
title('Technic #2 in Different Color Spaces', 'FontSize', 35);









