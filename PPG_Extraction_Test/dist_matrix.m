function [ dist_mat ] = dist_matrix( Nx,Ny )
a = -floor(Nx/2):floor(Nx/2)-(1-mod(Nx,2));
b = -floor(Ny/2):floor(Ny/2)-(1-mod(Ny,2));
[m,n] = meshgrid(b,a);
dist_mat = sqrt(m.^2+n.^2);
end

