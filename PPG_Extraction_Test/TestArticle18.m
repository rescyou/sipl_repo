% Article 18 - PPG Extraction Vr1 - R2014a
function [outPPG] = TestArticle18(videoCube, frameRate, opt)
    [Sx, Sy, ~, N] = size(videoCube);
    %% calc Threshold
    fiveSec = round(frameRate* 5);
    outvideo = zeros(Sx,Sy,fiveSec);
    for i=1:fiveSec
    	outvideo(:,:,i) = GetFrameAs(videoCube, i, opt);  
    end
    T = prctile(outvideo(:), 5);

    %% calc raw PPG
    PPG = zeros(1, N);
    dist_mat = dist_matrix(Sx,Sy);
    for i=1:N
        z = GetFrameAs(videoCube, i, opt);
        w = z > T;% pixel which are bigger then T
        temp = w.*dist_mat;% matrix of distances from center of pixels which are bigger then T
        PPG(i) = max(temp(:));
    end
    outPPG = PPG;
end
