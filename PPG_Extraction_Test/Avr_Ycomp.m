function [ outSignal ] = Avr_Ycomp(videoCube, p )

    if nargin == 1
        p = 1;
    end
    
    [H, W, ~, N] = size(videoCube);
    
    outSignal = zeros(1, N);
        
    ROIx = floor(H/2 - p*H/2+1) : floor(H/2 + p*H/2);
    ROIy = floor(W/2 - p*W/2+1) : floor(W/2 + p*W/2);
    
    for i=1:N
        z = rgb2ycbcr(videoCube(:,:,:,i));
        z = z(ROIx, ROIy,:);
        outSignal(i) = mean2(z(:,:,1));
    end

end

