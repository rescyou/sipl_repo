clc
vidObj = GetVideoObj(3);
v = VideoWriter('FingerVideoOut.mp4', 'MPEG-4');
v.open;
warning off;

p=3;
X = 1:235;

i = 1;
h = figure;
while vidObj.hasFrame
    z = vidObj.readFrame;
    z = rgb2ycbcr(z);
    
    y = double(z(:,:,1));
    M = max(y(:));
    m = min(y(:));
    %c=z;
    y = (y-M) .* (M - 0) ./ (M-m) + M;
    y = imgaussfilt(y,8);
    z(:,:,1) = uint8(y);
    %y1 = y; y1(y1
    
    %z(:,:,1) = reshape(histeq(y(y<255)), size(y)) + y(y==255);
    %z(:,:,1) = histeq(y);
    %z(:,:,1) = (z(:,:,1).^p) ./ (255^p);
    c = ycbcr2rgb(z);
    imshow(c);
    h.Name = num2str(i);
    i = i+1;
    
    v.writeVideo(c);
end

warning on;
