% Article 7 - PPG Extraction Test
function [outPPG] = TestArticle7(videoCube, frameRate, opt)

    %% read PPG by averaging the Y component in each frame
    % not sure if article 7 uses the Y component... 
    % but they are based on article 1, so it is probably correct.
    
    PPG = AvrComp(videoCube, opt);

    zeroMeanPPG = PPG-mean(PPG);

    %% apply band-pass filter
    % using 4th order Butterworth band-pass filter having cutoff frequencies of 0.75 Hz and 5 Hz
    PPG_filtered = butterBandpass(zeroMeanPPG, frameRate, 4, 0.75, 5);

    %% Create Baseline
    % not sure about butterworth or the order number
    baseline = butterBandpass(zeroMeanPPG, frameRate, 4, NaN, 0.3); % LPF

    %% Baseline Removal
    PPG_filtered2 = PPG_filtered - baseline;

    outPPG = PPG_filtered2;
    %% Plot
    % normFun = @(y) (y - min(y))./(max(y) - min(y));
    % 
    % X = (1:N) ./ vidObj.FrameRate;
    % figure;
    % plot(X, normFun(PPG), X, normFun(PPG_filtered), X, normFun(PPG_filtered2));
    % title(['PPG signal - Article 7 - ' name]);
    % legend('Raw PPG', 'After BPF', 'After Baseline Removal');
    % xlabel('Time [sec]');
end


