function [ outSignal, baseline ] = removeBaseline( signal, baselineIndexes )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    baseline = zeros(size(signal));
    
    for i=1:length(baselineIndexes)-1
        X = baselineIndexes(i):baselineIndexes(i+1);
        X = double(X);
        
        M = (signal(X(end)) - signal(X(1))) ./ (X(end) - X(1));
        
        baseline(X) = (X - X(1)).*M + signal(X(1));
    end
    
    baseline(1:baselineIndexes(1)) = baseline(baselineIndexes(1));
    baseline(baselineIndexes(end):end) = baseline(baselineIndexes(end));
    
    outSignal = signal - baseline;

end

