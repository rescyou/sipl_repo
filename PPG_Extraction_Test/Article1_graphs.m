close all;
clc;

load('SavedMatFiles\Art1Graphs_vr2.mat')

final = afterNoiseCleaning - baseline;

X = (1:length(final)) ./ FrameRate;

titleFontSize = 35;
xlabelFontSize = 23;
lWidth = 2;

%%
figure
T = 'Raw signal';
plot(X, beforeNoiseCleaning - mean(beforeNoiseCleaning), 'LineWidth', lWidth);
h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
xlabel('time [sec]', 'FontSize', xlabelFontSize);
title(T, 'FontSize', titleFontSize);

figure
T = 'After 4th order Butterworth (0.5 Hz - 5 Hz)';
plot(X, afterNoiseCleaning, 'LineWidth', lWidth);
h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
xlabel('time [sec]', 'FontSize', xlabelFontSize);
title(T, 'FontSize', titleFontSize);

figure
T = 'After baseline removal';
plot(X, final, 'LineWidth', lWidth);
h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
xlabel('time [sec]', 'FontSize', xlabelFontSize);
title(T, 'FontSize', titleFontSize);

%%
[ outPPG, baseline, lowPeaksLocs, outX ] = ...
    RemoveBaselineAlg( afterNoiseCleaning, false, false, FrameRate );

figure
T = 'Detecting the minimum peaks in each cycle';
h = drawVerLinesOnGraph(lowPeaksLocs, afterNoiseCleaning, FrameRate);
h.LineWidth = lWidth;
h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
xlabel('time [sec]', 'FontSize', xlabelFontSize);
title(T, 'FontSize', titleFontSize);

figure
T = 'The baseline of the signal';
plot(outX, afterNoiseCleaning, outX, baseline, 'LineWidth', lWidth);
h = gca;
h.YTick = [];
h.LineWidth = 1.5;
h.FontSize = 18;
xlabel('time [sec]', 'FontSize', xlabelFontSize);
title(T, 'FontSize', titleFontSize);

%%
T = 'Signal in frequency domain after BPF';
signal = afterNoiseCleaning;
%signal = beforeNoiseCleaning - mean(beforeNoiseCleaning);

figure;
F = fft(signal);
fs = FrameRate;
N = length(F);
F = abs(F(1:ceil(end/2)));
xf = ((1:length(F)) - 1) ./ N .* fs;
plot(xf, F, 'LineWidth', lWidth);
hold on;
[mY, mX] = max(F);
mX = xf(mX);
plot(mX, mY, 'ro', 'LineWidth', lWidth);
text(mX, mY, ['   ', 'Dominant Frequency = ', num2str(mX), ' Hz'], 'FontSize', 22);
hold off;

h = gca;
h.YTick = [];
h.XTick = h.XTick(1):(h.XTick(2)-h.XTick(1))/4:h.XTick(end);
h.LineWidth = 1.5;
h.FontSize = 18;
xlabel('frequency [Hz]', 'FontSize', xlabelFontSize);
title(T, 'FontSize', titleFontSize);

%%


