close all;
%clear all;

load('SavedMatFiles\ppg_Art_Vr2.mat');

N = vidObjUsed.NumberOfFrames;
X = (1:N) ./ vidObjUsed.FrameRate;

ArtIndex = 5;

%%
ppgs = cell(1, 3);
for i=1:3
    z = ppg_Art{i, ArtIndex};
    ppgs{i} = z{:};
end
ppgs{3} = -ppgs{3};

%%
titleFontSize = 35;
xlabelFontSize = 23;
lWidth = 2;

%%
X = (1:N) ./ vidObjUsed.FrameRate;

sI = 170;
eI = length(X) - 30;

ppg = ppgs{1};
%ppg = ppg(sI:eI);
%X = X(sI:eI);

figure
T = 'Output signal';
SetPlotInFigure(gca, plot(X, ppg), T, [0.8510, 0.3255, 0.0980]);

%%
T = 'Signal after BPF (not part of the technic)';
figure
signal = ppg(1:end-1);
X = (1:length(signal)) ./ vidObjUsed.FrameRate;
signal = butterBandpass(signal-mean(signal), FrameRate, 4, 0.5, 5);
SetPlotInFigure(gca, plot(X, signal), T, [0.8510, 0.3255, 0.0980]);

%%
T = 'After BPF and baseline removal (not part of the technic)';
figure
signal = ppg(1:end-1);
X = (1:length(signal)) ./ vidObjUsed.FrameRate;
signal = butterBandpass(signal-mean(signal), FrameRate, 4, 0.5, 5);


[ outPPG, baseline, lowPeaksLocs, outX ] = ...
        RemoveBaselineAlg( signal, false, false, FrameRate);
SetPlotInFigure(gca, plot(outX, outPPG), T, [0.8510, 0.3255, 0.0980]);

%%
T = 'Signal in frequency domain after BPF';
figure;
PrintDominantFrq(signal, vidObjUsed.FrameRate, T, [0.8510, 0.3255, 0.0980]);



