function [ outSignal ] = AvrComp(videoCube, opt, p )

    if nargin <= 2
        p = 1;
    end
    if nargin <= 1
        opt = 0;
    end
    
    [H, W, ~, N] = size(videoCube);
    
    outSignal = zeros(1, N);
        
    ROIx = floor(H/2 - p*H/2+1) : floor(H/2 + p*H/2);
    ROIy = floor(W/2 - p*W/2+1) : floor(W/2 + p*W/2);
    
    for i=1:N
        z = GetFrameAs(videoCube, i, opt);
        z = z(ROIx, ROIy,:);
        outSignal(i) = mean2(z(:,:,1));
    end

end

