% Article 1 - PPG Extraction Test
function [outPPG, beforeNoiseCleaning, afterNoiseCleaning] = TestArticle1(videoCube, frameRate, opt)

    %% read PPG by averaging the Y component in each frame 
    PPG = AvrComp(videoCube, opt);
    beforeNoiseCleaning = PPG;

    %% apply band-pass filter
    % using 4th order Butterworth band-pass filter having cutoff frequencies of 0.5 Hz and 5 Hz
    PPG_filtered = butterBandpass(PPG-mean(PPG), frameRate, 4, 0.5, 5);
    afterNoiseCleaning = PPG_filtered;

    %%
    lowPeaksLocs = cycle_detect(PPG_filtered);

    %%
    PPG_filtered2 = removeBaseline(PPG_filtered, lowPeaksLocs);
    %PPG_filtered2 = PPG_filtered2(lowPeaksLocs(1):lowPeaksLocs(end));

    %%
    %drawVerLinesOnGraph(lowPeaksLocs-lowPeaksLocs(1), PPG_filtered2);
    
    outPPG = PPG_filtered2;

end

