% Article 11 - PPG Extraction
function [outPPG] = TestArticle11(videoCube, frameRate, opt)
    if nargin <= 2
        opt = 0;
    end
    
    N = size(videoCube,4);
    %% calc F(t)
    %F = robustFilter_art11( videoCube );

    %% calculate treshold
    fiveSec = round(frameRate* 5);
    I_Max = zeros([1 fiveSec]);
    I_Min = zeros([1 fiveSec]);
    for i=1:fiveSec
        z = GetFrameAs(videoCube, i, opt);
        I_Max(i) = max(z(:));
        I_Min(i) = min(z(:));
    end
    T = mean(I_Max) - 1/20*(mean(I_Max)-mean(I_Min));

    %% calculate PPG
    PPG = zeros(1, N);
    for i=1:N
        z = GetFrameAs(videoCube, i, opt);
        PPG(i) = sum(sum(z > T));
    end
    outPPG = PPG;
end

