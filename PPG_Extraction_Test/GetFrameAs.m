function [ outFrame ] = GetFrameAs( videoCube, index, opt )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    %types = {'RGB', 'YUV', 'HSV'};

    switch opt
        case 0  % RGB - red (R) comp
            outFrame = videoCube(:,:,1,index);
        case 1  % YUV - luma (Y) comp
            t = rgb2ycbcr(videoCube(:,:,:,index));
            outFrame = t(:,:,1)*(255/234);
        case 2  % HSV - saturation (S) comp
            t = rgb2hsv(videoCube(:,:,:,index));
            outFrame = t(:,:,2)*255;
    end

end


