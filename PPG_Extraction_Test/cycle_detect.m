function [ cycles ] = cycle_detect( signal )

    signal = signal - mean(signal);
    N = length(signal);

    fSignal = fft(signal);
    [~, f0_index] = max(abs(fSignal( 1 : floor(length(fSignal)/2)+1 )));%find dominat frecuency
    
    f0 = (f0_index - 1) / N;% convert to real frecuency value
    T0 = 1 / f0;
    
    [peaks , peaks_in] = findpeaks(-signal);% find min peaks
    peaks = -peaks;
    
    [~, min_peak_in] = min(peaks);% global min
    min_peak_in = peaks_in(min_peak_in);

    so = int32(round(T0/2));%searchOffset 

    boundLowIndex = so;
    boundHighIndex = length(signal)-so;% bounderies

    T0 = int32(round(T0));

    cycles = min_peak_in;
    runningIndex = min_peak_in + T0;
    RunSearch(min_peak_in, 1);
    RunSearch(min_peak_in, -1);
    
    function RunSearch(startIndex, direction)
        runningIndex = startIndex + direction*T0;
        while runningIndex >= boundLowIndex && runningIndex <= boundHighIndex
            i = runningIndex;

            [peaks_foc, peaks_foc_in] = findpeaks(-signal((i-so):((i+so))));
            peaks_foc = -peaks_foc;
            if isempty(peaks_foc)% problem with emptypicks check!
                runningIndex = runningIndex + T0;
                continue;
            end
            [~,j] = min(peaks_foc);%find min in nhiberhood
            min_peak_foc_in = peaks_foc_in(j) + i-so - 1;
            if direction == 1
                cycles = [cycles, min_peak_foc_in];
            else
                cycles = [min_peak_foc_in, cycles];
            end
            runningIndex = min_peak_foc_in + direction*T0;
        end
    end

    % for i=min_peak_in:T0:(length(ppg_signal)-so)
    %    [peaks_foc, peaks_foc_in] = findpeaks(ppg_signal((i-so):((i+so))));
    %     min_paek_foc_in = peaks_foc_in(find(peaks_foc==min(peaks_foc)));
    %     cycles = [cycles,min_paek_foc_in];
    % end
    % for i=min_peak_in:(-T0):round(T0/7)
    %    [peaks_foc, peaks_foc_in] = findpeaks(ppg_signal((i-round(T0/7)):((i+round(T0/7)))));
    %     min_paek_foc_in = peaks_foc_in(find(peaks_foc==min(peaks_foc)));
    %     cycles = [min_paek_foc_in,cycles];
    % end
    
    
end

