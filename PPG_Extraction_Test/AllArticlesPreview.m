%% Init
close all;
%clear all;

load('SavedMatFiles\ppg_Art_Vr2.mat');

N = vidObjUsed.NumberOfFrames;
X = (1:N) ./ vidObjUsed.FrameRate;

normFun1 = @(y) (y - min(y))./(max(y) - min(y));
normFun2 = @(y) y - mean(y);

%% Edit Data
ppgs = cell(size(ppg_Art));
for i=1:numel(ppg_Art)
    z = ppg_Art{i};
    ppgs{i} = z{:};
end

ppgs{3,1} = -ppgs{3,1};
ppgs{3,2} = -ppgs{3,2};
ppgs{3,3} = -ppgs{3,3};
ppgs{3,5} = -ppgs{3,5};

%% Preview
sI = 170;
eI = 30;

for j = 5%1:5
    figure;
    for i=1:3
        [ppg, baseline] = RemoveBaselineAlg(ppgs{i,j}(sI:end-eI),false,false);
        %ppg = baseline;
        if j ~= 5 %article 15
            h = plot(X(sI:end-eI), normFun2(ppg));
            h.LineWidth = 1.5;
        else
            h = plot(X(sI:end-eI), normFun1(ppg));
            h.LineWidth = 1.5;
        end
        hold on;
    end
    h = legend('R from RGB','Y from YCbCr','S from HSV');
    h.FontSize = 18;
    h.FontWeight = 'b';
    h = title(articles{j});
    h.FontSize = 18;
    h = xlabel('time [sec]');
    h.FontSize = 16;
    hold off;
end


