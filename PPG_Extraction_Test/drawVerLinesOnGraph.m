function h = drawVerLinesOnGraph( lineIndexes, y, samplingRate )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

    X = 1:length(y);
    if ~(nargin < 3 || isempty(samplingRate) || samplingRate == 0)
        X = X ./ samplingRate;
    end
    
    h = plot(X, y);
    
    yL = get(gca,'YLim');
    for i=1:length(lineIndexes)
        line([X(lineIndexes(i)) X(lineIndexes(i))], yL, 'Color', 'r');
    end
    
end

