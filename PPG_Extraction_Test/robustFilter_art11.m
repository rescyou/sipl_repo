function [ outFunction ] = robustFilter_art11( videoCube )
N = size(videoCube,4);
F = zeros(1, N);
for i=1:N
    z = videoCube(:,:,:,i);
    Er = mean2(z(:,:,1));
    SDr = std2(z(:,:,1));
    
    Eg = mean2(z(:,:,2));
    SDg = std2(z(:,:,2));
    
    Eb = mean2(z(:,:,3));
    SDb = std2(z(:,:,3));
    
    F(i) = Er-SDr>128 && Eg+SDg<128 && Eb+SDb<128;
end
    outFunction = F;
end

