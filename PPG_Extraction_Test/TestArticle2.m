% Article 2 - PPG Extraction Test
function [outPPG] = TestArticle2(videoCube, frameRate, opt)

    %% read PPG by averaging the Y component in each frame
    % not sure if article 2 uses the Y component... 
    PPG = AvrComp(videoCube, opt);

    %% apply band-pass filter
    % using 4th order Butterworth band-pass filter having cutoff frequencies of 0.25 Hz and 7 Hz
    PPG_filtered = butterBandpass(PPG - mean(PPG), frameRate, 4, 0.25, 7);

    %% apply moving average filter
    PPG_filtered2 = movingAverage(10, PPG_filtered);
    
    outPPG = PPG_filtered2;

    %% Plot
%     normFun = @(y) (y - min(y))./(max(y) - min(y));
% 
%     X = (1:N) ./ vidObj.FrameRate;
%     figure;
%     plot(X, normFun(PPG), X, normFun(PPG_filtered), X, normFun(PPG_filtered2));
%     title(['PPG signal - Article 2 - ' name]);
%     legend('Raw PPG', 'After BPF', 'After Avg Filter');
%     xlabel('Time [sec]');

end
