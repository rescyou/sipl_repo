function [ fig_handle, plot_handle ] = SetPlotInFigure( ...
    fig_handle, plot_handle, titleStr, lineColor, xlabelStr )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
    if nargin < 5
        xlabelStr = 'time [sec]';
    elseif nargin < 4
        lineColor = [];
    end
    
    %%
    titleFontSize = 35;
    xlabelFontSize = 23;
    lWidth = 2;

    %%
    plot_handle.LineWidth = lWidth;
    if ~isempty(lineColor)
        plot_handle.Color = lineColor;
    end
    
    h = fig_handle;
    h.YTick = [];
    h.LineWidth = 1.5;
    h.FontSize = 18;
    xlabel(xlabelStr, 'FontSize', xlabelFontSize);
    title(titleStr, 'FontSize', titleFontSize);

end

