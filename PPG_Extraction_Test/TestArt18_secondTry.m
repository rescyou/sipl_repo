
titleFontSize = 35;
xlabelFontSize = 23;
lWidth = 2;

%%
% rawPPGs = cell(2,3);
% bpfPPGs = cell(2,3);
% 
% X = 1:size(videoCube,4);
% 
% for i=1:3
%     opt = i-1;
%     [rawPPGs{1, i}, rawPPGs{2, i}, bpfPPGs{1, i}, bpfPPGs{2, i}] = ...
%         Article18_secondTry(videoCube, FrameRate, opt);
%     figure
%     plot(X, rawPPGs{1, i}, X, rawPPGs{2, i});
%     figure
%     plot(X, bpfPPGs{1, i}, X, bpfPPGs{2, i});
% end

%%
load('SavedMatFiles\article18_secondTry_RYS_2Tecnics.mat');
%%
normFun1 = @(y) (y - min(y))./(max(y) - min(y));
normFun2 = @(y) y - mean(y);

for i=1:3
    figure
    plot(X, normFun1(bpfPPGs{1, i}));
    figure
    plot(X, normFun1(bpfPPGs{2, i}));
end

%%
bpfPPGs{1, 1} =  butterBandpass(rawPPGs{1, 1}-mean(rawPPGs{1, 1}), ...
    FrameRate, 4, 0.3, 12.5);
bpfPPGs{2, 1} =  butterBandpass(rawPPGs{2, 1}-mean(rawPPGs{2, 1}), ...
    FrameRate, 4, 0.3, 12.5);

%%
figure
T = '1st Approach - BPF on R(t)';
SetPlotInFigure(gca, plot(X, bpfPPGs{1, 1}), T, [0, 0.4980, 0]);

figure
T = '2nd Approach - BPF on R(t)';
SetPlotInFigure(gca, plot(X, bpfPPGs{2, 1}), T, [0, 0.4980, 0]);

%% preview ppg building
close all
clc
[Sx, Sy, ~, N] = size(videoCube);

dist_mat = dist_matrix(Sx, Sy);

h1 = figure;
h2 = figure;
h3 = figure;

%X = (1:N);
rawPPG_tec1 = NaN(1,N);
rawPPG_tec2 = NaN(1,N);

T = 254;
for i=1:N
    z = videoCube(:,:,1,i);
    z = z > T;
    z = edge(z, 'sobel'); % or 'canny'
    set(0, 'currentfigure', h1);
    imshow(z,[]);

    %% Technic #1
    temp = dist_mat.*z;
    rawPPG_tec1(i) = mean(temp(:)); % or max
    set(0, 'currentfigure', h2);
    plot(rawPPG_tec1);
    xlim([0 N])

    %% Technic #2
    [xs, ys] = find(z);
    %[~,~,R,~] = circfit(xs, ys);
    x = xs(:); y = ys(:);
    a = [x y ones(size(x))] \ [-(x.^2+y.^2)];
    R = sqrt((a(1)^2+a(2)^2)/4-a(3));
    rawPPG_tec2(i) = R;
    set(0, 'currentfigure', h3);
    plot(rawPPG_tec2);
    xlim([0 N])
end

%% preview stages
close all
clc
f = 72;

figure;
z = double(videoCube(:,:,:,f));
z = z.^(1.3);
fil = fspecial('average',10);
z = imfilter(z, fil);
ma = max(max(z(:)));
mi = min(min(z(:)));
for i=1:3
    %z(:,:,i) = 255 .* ((ma - z(:,:,i)) ./ (ma - mi));
end
imshow(uint8(z));
%title('Frame from video', 'FontSize', titleFontSize);

figure;
z = videoCube(:,:,1,f);
imshow(z,[0 258]);
%title('Red component', 'FontSize', titleFontSize);

figure
z = z > T;
imshow(z,[0 1.5]);

figure
z = edge(z, 'sobel'); % or 'canny'
imshow(z,[0 1.5]);

%%
clc
T = 'Signal in frequency domain after BPF';
figure;
PrintDominantFrq(bpfPPGs{1, 1}, FrameRate, T, [0, 0.4980, 0])*60

figure;
PrintDominantFrq(bpfPPGs{2, 1}, FrameRate, T, [0, 0.4980, 0])*60








