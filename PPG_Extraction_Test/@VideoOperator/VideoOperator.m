classdef VideoOperator < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        VideoFolderName = 'FingerVideos';
        ColorMap = struct(...
            'RGB', {1}, ...
            'YUV', {2}, ...
            'HSV', {3});
    end
    
    properties
        videoObj
        videoCube
        componentCube
        
        nrFrames
        width
        height
    end
    
    methods
        function obj = VideoOperator(index)
            obj.GetVideoObj(index);
            
            obj.nrFrames = vidObj.NumberOfFrames;
            obj.width = vidObj.Width;
            obj.height = vidObj.Height;            
        end
        
        function buidComponentCube(obj)
            nrComps = numel(fieldnames(VideoOperator.ColorMap));
            obj.componentCube = zeros(obj.height, ...
                obj.width, ...
                nrComps, ...
                obj.nrFrames);
            
            for i=1:obj.nrFrames
                for c=1:nrComps
                    obj.componentCube(:,:,c,i) = 
                end
            end
            function outFrame = getCompFrame()
                switch opt
                    case VideoOperator.ColorMap.RGB  % RGB - red (R) comp
                        outFrame = videoCube(:,:,1,frame_index);
                    case VideoOperator.ColorMap.YUV  % YUV - luma (Y) comp
                        t = rgb2ycbcr(videoCube(:,:,:,frame_index));
                        outFrame = t(:,:,1)*(235/234);
                    case VideoOperator.ColorMap.HSV  % HSV - saturation (S) comp
                        t = rgb2hsv(videoCube(:,:,:,frame_index));
                        outFrame = t(:,:,2)*255;
                end
            end
        end
    end
    
    methods (Access = private)
        function GetVideoObj(obj, index)
            folder = VideoOperator.VideoFolderName;
            videoFilenames = dir([folder '\*.MOV']);
            videoFilenames = {videoFilenames.name};

            vidName = videoFilenames{index};    
            obj.videoObj = VideoReader([folder '\' vidName]);

            disp(['video selected: ' videoFilenames{index}]);
        end
    end
    
end

