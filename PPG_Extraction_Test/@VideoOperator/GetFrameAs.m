function [ outFrame ] = GetFrameAs( obj, frame_index, opt )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    switch opt
        case VideoOperator.ColorMap.RGB  % RGB - red (R) comp
            outFrame = videoCube(:,:,1,frame_index);
        case VideoOperator.ColorMap.YUV  % YUV - luma (Y) comp
            t = rgb2ycbcr(videoCube(:,:,:,frame_index));
            outFrame = t(:,:,1)*(235/234);
        case VideoOperator.ColorMap.HSV  % HSV - saturation (S) comp
            t = rgb2hsv(videoCube(:,:,:,frame_index));
            outFrame = t(:,:,2)*255;
    end

end


