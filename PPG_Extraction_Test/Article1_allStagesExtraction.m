%clear all
clc

%% Read Data
vidObj = GetVideoObj(3);
N = vidObj.NumberOfFrames;
videoCube = zeros(vidObj.Height, vidObj.Width, 3, vidObj.NumberOfFrames, 'uint8');

for i=1:N
    videoCube(:,:,:,i) = vidObj.read(i);
end

%%
[final, beforeNoiseCleaning, afterNoiseCleaning] = ...
    TestArticle1(videoCube, vidObj.FrameRate,1); % Y comp

%%
X = (1:length(final)) ./ vidObj.FrameRate;
figure
plot(X, beforeNoiseCleaning - mean(beforeNoiseCleaning));
xlabel('time [sec]', 'FontSize', 15);
title('Before Noise Cleaning', 'FontSize', 18);
h = gca;
h.YTick = [];

figure
plot(X, afterNoiseCleaning);
xlabel('time [sec]', 'FontSize', 15);
title('After Applying Bandpass Filter', 'FontSize', 18);
h = gca;
h.YTick = [];

figure
plot(X, final);
xlabel('time [sec]', 'FontSize', 15);
title('After Baseline Removal', 'FontSize', 18);
h = gca;
h.YTick = [];

%%
figure
[ outPPG, baseline, lowPeaksLocs, outX ] = ...
    RemoveBaselineAlg( afterNoiseCleaning, false, true, vidObj.FrameRate );
xlabel('time [sec]', 'FontSize', 15);
title('After Applying Bandpass Filter', 'FontSize', 18);

figure
plot(outX, afterNoiseCleaning, outX, baseline);
%%
signal = afterNoiseCleaning;
%signal = beforeNoiseCleaning - mean(beforeNoiseCleaning);

figure;
F = fft(signal);
fs = vidObj.FrameRate;
N = length(F);
F = abs(F(1:ceil(end/2)));
xf = ((1:length(F)) - 1) ./ N .* fs;
plot(xf, F);
hold on;
[mY, mX] = max(F);
mX = xf(mX);
plot(mX, mY, 'ro');
text(mX, mY, ['   ', 'Dominant Frequency = ', num2str(mX), ' Hz'], 'FontSize', 14);
hold off;

h = gca;
h.YTick = [];
h.XTick = h.XTick(1):(h.XTick(2)-h.XTick(1))/4:h.XTick(end);
xlabel('frequency [Hz]', 'FontSize', 15);
title('After Applying Bandpass Filter', 'FontSize', 18);

%%





