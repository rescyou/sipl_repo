clc
addpath Ifreq_Spectrogram

load('../Main/cleanExperiments_1_4.mat')
e = exps(1);
NumberOfCycles = 15;
overLapCyclePrecent = 0.2;
Signal =  e.oxSig1(:)';
% Signal = e.ipSig(:)';
fs = e.oximeterSR;
% fs =e.iphoneSR;
basicTF.win = round(NumberOfCycles*fs);
basicTF.hop = round(fs*overLapCyclePrecent);
basicTF.fs = fs; 
basicTF.fr = 1/(basicTF.fs/2);
basicTF.feat = 'STFT';

advTF.num_tap = 1;
advTF.win_type = 'Flattop';
advTF.Smo = 0;
advTF.Rej = 0;
advTF.ths = 1.0000e-06;
advTF.HighFreq = 1/3;
advTF.LowFreq = 1/60;
advTF.lpc = 0;

cepR.g = 0.3;
cepR.Tc = 0;

P.num_s = 1;
P.num_c = 1;

[~, ~, ~, tfrr, rtfrF, ~, tfrtic, t] = CFPH(Signal, basicTF, advTF, cepR, P);

figure;
imagesc(t*1/basicTF.fs,tfrtic*basicTF.fs,tfrr);

%%
Spectrogram = tfrr;
dtw = 1;
IF = CurveExt_M(Spectrogram', dtw);
%%
figure;
plot(t*1/basicTF.fs,IF);










