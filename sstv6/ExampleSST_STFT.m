%clear ; close all ;

addpath('./tool') ;

scrsz = get(groot,'ScreenSize');

	%% generate the simulated data
	%% she sampling time (100Hz sampling rate)
	%% high sampling rate to avoid sampling issue
    %% You can generate other different kinds using genSignal.m

Hz = 100 ;
L = 12 ;
N = Hz * L ;


opts.WinLen = 301 ;
        %% Setup parameters
        %% alpha is the grid size in the freq axis
alpha = 0.01/Hz ;


initstate(1) ;


	%% the amplitude modulation of the simulated signal
am1 = smooth(cumsum(randn(N,1)) ./ Hz, 200, 'loess') ;
am1 = 2 + am1 ./ max(abs(am1)) ;
am2 = smooth(cumsum(randn(N,1)) ./ Hz, 200, 'loess') ;
am2 = 2 + am2 ./ max(abs(am2)) ;
am1(1:300) = 0 ;
am2(end-400:end) = 0 ;


    %% the instantaneous frequency of the simulated signal
if1 = smooth(cumsum(randn(N,1)) ./ Hz, 400, 'loess') ;
if1 = 10 + 6 * if1 ./ max(abs(if1)) ;
if2 = smooth(cumsum(randn(N,1)) ./ Hz, 300, 'loess') ;
if2 = pi + 3 * if2 ./ max(abs(if2)) ;
phi1 = cumsum(if1) / Hz ; 
phi2 = cumsum(if2) / Hz ; 
if1(1:300) = nan ;
if2(end-400:end) = nan ;

	%% the simulated signal.
clean = am1 .* cos(2*pi*phi1) + am2 .* cos(2*pi*phi2) ; 
	%% add noise (Gaussian white noise)

noise = 0 * randn(N, 1) ; 
%dis = random('t',4,N, 1) ;
%e = armaxfilter_simulate(dis, .5, 1, .5, 1, -.5) ;
%noise = sigma * e ./ std(e) ;
snrdb = 20 * log10(std(clean)./std(noise)) ;
fprintf(['snrdb = ',num2str(snrdb),'\n']) ;


	%% simulated observed time series
xm = clean + noise ;


time = [1/Hz:1/Hz:L]' ;

%%
%%%% call sqSTFT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[h,Dh,t] = hermf(opts.WinLen*3, 1, 6) ;

xm_oxi = oximeterSignal(5848:4:end);
addZeros = @(x) [x(:); zeros(length(xm_oxi) - length(x), 1)];
xm_iphone_raw = (raw_signal_iphone);
xm_iphone_signal = (iphoneSignal);

figure;
DrawSST( xm_oxi, h, Dh, Hz, length(xm_oxi) ./ length(xm_iphone_raw) );

figure;
[h,Dh,t] = hermf(opts.WinLen, 1, 6) ;
DrawSST( xm_iphone_raw, h, Dh, Hz, 1 );

figure;
DrawSST( xm_iphone_signal, h, Dh, Hz, 1 );

