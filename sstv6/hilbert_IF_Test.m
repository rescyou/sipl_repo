xm_oxi = oximeterSignal(5848:4:end);
addZeros = @(x) [x(:); zeros(length(xm_oxi) - length(x), 1)];
xm_iphone_raw = (raw_signal_iphone);
xm_iphone_signal = (iphoneSignal);

figure;
z = hilbert(xm_oxi);
instfreq1 = 500/(2*pi)*diff(unwrap(angle(z)));
plot(instfreq1);

figure;
z = hilbert(xm_iphone_raw);
instfreq2 = srIphone/(2*pi)*diff(unwrap(angle(z)));
plot(instfreq2);

figure;
z = hilbert(xm_iphone_signal);
instfreq3 = srIphone/(2*pi)*diff(unwrap(angle(z)));
plot(instfreq3);
