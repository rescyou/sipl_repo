function [ tfr, tfrtic, tfrsq, tfrsqtic ] = DrawSST( xm, h, Dh, Hz, scale )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

xm = xm(:);
L = 12 ;
time = (1/Hz:1/Hz:L)' ;
[tfr, tfrtic, tfrsq, tfrsqtic] = sqSTFT(xm, 0/Hz, 20/Hz, 0.5/length(xm)/2, 1, h', Dh') ; 

tfrsqtic = tfrsqtic .* scale;

subplot(1,2,1) ;
imageSQ(time, tfrsqtic*Hz, log(1+abs(tfrsq))) ; %colormap(1-gray) ;
set(gca,'fontsize', 20) ; xlabel('Time (sec)') ; ylabel('Frequency (Hz)') ;
title('SST on STFT\newline(displayed in the log scale)') ;

subplot(1,2,2) ;
imageSQ(time, tfrsqtic*Hz, abs(tfrsq)) ; %colormap(1-gray) ;
set(gca,'fontsize', 20) ; xlabel('Time (sec)') ; ylabel('Frequency (Hz)') ;
title('SST on STFT\newline(displayed in the linear scale)') ;

end

