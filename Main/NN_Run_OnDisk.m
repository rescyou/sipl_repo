function [ finalNet ] = NN_Run_OnDisk(imdb, imdbv, layers, trainingOpt, testName, functions)
%NN_RUN_ONDISK Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 5
        testName = 'TEST';
    end
    if nargin < 6
        functions = {};
    end
    
    %% get test name
    testName = sprintf('NN_Run_OnDisk__%s__nInputs_%d__nCls_%d__net_%s', ...
        testName, length(imdb.Files), numel(unique(imdb.Labels)), layers(1).Name);
    
    %% get image info
    info = imfinfo(imdb.Files{1});
    
    %% NN_Run_Aux
    validationSet = {};
    if ~isempty(imdbv.Files)
        validationSet = {imdbv};
    end
    
    finalNet = NN_Run_Aux( {imdb}, validationSet, ...
        length(imdb.Files), length(imdbv.Files), ...
        @predictFnc, ...
        [info.Height info.Width 3], layers, trainingOpt, testName, functions );

end

function [accuracy_train, accuracy_validation, nnInfo] = ...
            predictFnc(finalNet, inputCellTrain, inputCellValidation)
    imdb = inputCellTrain{:};
    YPred = classify(finalNet, imdb);
    accuracy_train = sum(YPred == imdb.Labels)/numel(imdb.Labels);
    accuracy_validation = -1;
    
    if ~isempty(inputCellValidation)
        imdbv = inputCellValidation{1};
        YPred = classify(finalNet, imdbv);
        accuracy_validation = sum(YPred == imdbv.Labels)/numel(imdbv.Labels);
    end

    nnInfo = [];
    nnInfo.accuracy_train = accuracy_train;
    nnInfo.accuracy_validation = accuracy_validation;
end
