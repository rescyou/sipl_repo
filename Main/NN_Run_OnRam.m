function [ finalNet ] = NN_Run_OnRam( X, Y, Xv, Yv, layers, trainingOpt, testName, functions )
%NN_RUN_ONRAM Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 7
        testName = 'TEST';
    end
    if nargin < 8
        functions = {};
    end
    
    %% get test name
    testName = sprintf('OnRam__%s__nTrain_%d__nVal_%d__nBins_%d__Net_%s', ...
        testName, size(X,4), size(Xv,4), numel(unique([Y; Yv])), layers(1).Name);
    
    %% NN_Run_Aux
    validationSet = {};
    if ~isempty(Xv)
        validationSet = {Xv, Yv};
    end
    
    inputSize = size(X);
    inputSize = inputSize(1:3);
    
    finalNet = NN_Run_Aux( {X, Y}, validationSet, ...
        size(X,4), size(Xv,4), ...
        @predictFnc, ...
       inputSize, layers, trainingOpt, testName, functions, @plotConfusionMatrixFunc );
   
    %% aux functions
    function [accuracy_train, accuracy_validation, nnInfo] = predictFnc(finalNet, ~, ~)
        if iscategorical(Y)
            YPred = classify(finalNet, X);
        else
            YPred = predict(finalNet, X);
        end
        accuracy_train = sum(YPred == Y)/numel(Y);
        accuracy_validation = -1;

        nnInfo = [];
        nnInfo.trainTarget = Y;
        nnInfo.trainResults = YPred;
        nnInfo.validationTarget = [];
        nnInfo.validationResults = [];
        
        if ~isempty(Xv)
            if iscategorical(Yv)
                YPred = classify(finalNet, Xv);
            else
                YPred = predict(finalNet, Xv);
            end
            accuracy_validation = sum(YPred == Yv)/numel(Yv);
            nnInfo.validationTarget = Yv;
            nnInfo.validationResults = YPred;
        end

        nnInfo.accuracy_train = accuracy_train;
        nnInfo.accuracy_validation = accuracy_validation;
    end

    function [f_train, f_validation] = plotConfusionMatrixFunc(~, ~, ~, nnInfo)
        f_validation = figure;
        f_train = figure;
        if ~isempty(Xv) || iscategorical(Yv)
            figure(f_validation);
            plotConfMat(nnInfo.validationTarget, nnInfo.validationResults);
        end
        if ~isempty(X) || iscategorical(Y)
            figure(f_train);
            plotConfMat(nnInfo.trainTarget, nnInfo.trainResults);
        end
    end

end


