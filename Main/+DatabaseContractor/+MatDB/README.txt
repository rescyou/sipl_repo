This is our newest database extraction technic!

Contains all of the following data in independent mat files:

    'ppg_raw'
    'ppg_clean'
    'ppg_sr'
    'stft_abs'
    'stft_angle'
    'stft_frqs'
    'stft_time'
    'systolic'
    'diastolic'
    'pulse'

Problem: takes a lot of memory on disk

We tried to use it directly with trainNetwork but it was too slow.

We can use the data to make new databases easily...
