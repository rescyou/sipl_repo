function MatDbToImages(inputMatFilesFolder, outputFolder, cutoffFrqs)

    if nargin < 3
        cutoffFrqs = PARAMS.CUTOFF_FRQS;
    end

    Utils.MakeFolder(outputFolder);

    files = dir(inputMatFilesFolder);
    files = files(~[files.isdir]);
    N = length(files);
    
    bitsToEncode = 16;
    angNormFactor = (2^bitsToEncode - 1) ./ (2*pi);
    absNormMax = (2^bitsToEncode - 1);

    for i = 1:N
        fn = files(i).name;
        
        s = load([inputMatFilesFolder '\' fn]);
        dataStruct = s.data;

        idxs = dataStruct.stft_frqs >= cutoffFrqs(1) & dataStruct.stft_frqs <= cutoffFrqs(2);
        img = cat(3, ...
            normAbs(dataStruct.stft_abs(idxs, :)), ...
            normAngle(dataStruct.stft_angle(idxs, :)), ...
            zeros(size(dataStruct.stft_angle(idxs, :))));
        img = uint16(img);
        
        imwrite(img, [outputFolder '\' fn '.png'], 'BitDepth', bitsToEncode);
    end

    %%
    function nrAngIm = normAngle(angIm)
        nrAngIm = (angIm + pi) .* angNormFactor;
    end
    function nrAbsIm = normAbs(absIm)
        %absIm = absIm ./ sum(absIm(:));
        nrAbsIm = absIm .* absNormMax ./ max(absIm(:));
    end
end
