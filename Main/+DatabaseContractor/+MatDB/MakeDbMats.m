function MakeDbMats( organizedDbInputFolder, outputFolder, ...
    window_size_in_sec, ...
    stft_window_size_in_sec, stft_cutoff_freq, stft_overlap)
%MAKEDBMATS Make Mat DB for NN

    %% init 1
    startIndex = 1;
    stopIndex = inf;
    
    %% init 2
    windowSize = [];
    
    %% make output folder
    if ~exist(outputFolder,'dir')
        disp(['Making folder: ' outputFolder]);
        mkdir(outputFolder);
    end
    
    %% read inputs
    files = dir(organizedDbInputFolder);

    filenames = Utils.sort_nat({files.name});
    filenames = filenames(3:end);
    
    %%
    counter = 1;
    
    h2 = waitbar(100, 'current file progress');
    
    for ifile = 1:length(filenames)
        disp(['Loading... ' filenames{ifile}]);
        s = load([organizedDbInputFolder '/' filenames{ifile}]);
        dbData = s.dbData;
        
        if isempty(windowSize)
            windowSize = floor(window_size_in_sec * dbData(1).samplingRate);
        end
        
        for idbDataItem = 1:length(dbData)
            waitbar(idbDataItem/length(dbData), h2);
            
            dbDataItem = dbData(idbDataItem);
            sigLen = length(dbDataItem.PPG);
            
            for iwin = 1:windowSize:sigLen
                if iwin+windowSize-1 > sigLen
                    continue
                end

                range = iwin:iwin+windowSize-1;

                makeAndSaveWindow(dbDataItem, range);
                if counter >= stopIndex
                    return;
                end
            end
        end
    end
    
    %% aux functions
    function makeAndSaveWindow(dbDataItem, range)
        if counter >= startIndex
            if counter == startIndex
                disp(['STARTED from ' num2str(counter)]);
            end
            ppg_win = dbDataItem.PPG(range);
            bp_win = dbDataItem.BP(range);

            [stft, t, f] = ppgStft(ppg_win, dbDataItem.samplingRate);
            [sys, dias] = bpCalc(bp_win);
            
            if ~(sys >= 70 && sys <= 190 && dias >= 40 && dias <= 100)
                return;
            end
            
            pulse = pulseCalc(bp_win, dbDataItem.samplingRate);
            
            fileName1 = sprintf('data_%0.8d__win_%0.3d__sys_%g__dias_%g__pulse_%g__ovlp_%g', counter, stft_window_size_in_sec, sys, dias, pulse, stft_overlap);
            ff = sprintf('%s/%s.%s', outputFolder, fileName1, 'mat');
            
            try
            [ cleanPpg, ~, ~, ~, ~ ] = ...
            PPGAnalyzer.DenoiseAndRemoveBaseline( ppg_win, dbDataItem.samplingRate );
            catch e
                cleanPpg = nan;
            end
            
            data = [];
            data.ppg_raw = ppg_win;
            data.ppg_clean = cleanPpg;
            data.ppg_sr = dbDataItem.samplingRate;
            data.stft_abs = abs(stft);
            data.stft_angle = angle(stft);
            data.stft_frqs = f;
            data.stft_time = t;
            data.systolic = sys;
            data.diastolic = dias;
            data.pulse = pulse;
            
            save(ff, 'data');
        end
        counter = counter + 1;
    end
    
    function [s, t, f] = ppgStft(ppgWin, sampling_rate)
        % create STFT image
        [s, t, f] = Stft.CreateStftAdvImg(ppgWin, sampling_rate, ...
            stft_window_size_in_sec, stft_overlap, stft_cutoff_freq);
    end

    function [systolic, diastolic] = bpCalc(bpWin)
        [systolic, diastolic] = DatabaseContractor.Common.signal2bp(bpWin);
    end

    function pulse = pulseCalc(bpWin, sr)
        pulse = DatabaseContractor.Common.signal2pulse(bpWin, sr);
    end
end



