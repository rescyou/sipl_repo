function [ systolic, diastolic, pulse ] = MakeLabels( imagesFolderPath )
%MAKEDATABASESTRUCTFROMIMAGES Summary of this function goes here
%   Detailed explanation goes here


%     imdb = imageDatastore(imagesFolderPath, 'FileExtensions', '.mat');
%     N = length(imdb.Files);
    
    files = dir(imagesFolderPath);
    files = files(~[files.isdir]);
    N = length(files);
    
    systolic = nan(N, 1);
    diastolic = nan(N, 1);
    pulse = nan(N, 1);
    
    for fi = 1:N
        filename = files(fi).name(1:end-4);
        parts = strsplit(filename,{'_'});
        systolic(fi) = str2double(parts{6});
        diastolic(fi) = str2double(parts{8});
        pulse(fi) = str2double(parts{10});
    end
end
