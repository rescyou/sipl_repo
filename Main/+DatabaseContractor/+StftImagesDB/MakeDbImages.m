function MakeDbImages( organizedDbInputFolder, outputFolder, ...
    window_size_in_sec, ...
    stft_window_size_in_sec, stft_cutoff_freq, stft_overlap)
%MAKEDBIMAGES Make db for NN

    %% init 1
    extention = 'png';
    
    startIndex = 1;
    stopIndex = inf;
    
    %% init 2
    windowSize = [];
    
    %% make output folder
    if ~exist(outputFolder,'dir')
        disp(['Making folder: ' outputFolder]);
        mkdir(outputFolder);
    end
    
    %% read inputs
    files = dir(organizedDbInputFolder);

    filenames = Utils.sort_nat({files.name});
    filenames = filenames(3:end);
    
    %%
    counter = 1;
    
    for ifile = 1:length(filenames)
        disp(['Loading... ' filenames{ifile}]);
        s = load([organizedDbInputFolder '/' filenames{ifile}]);
        dbData = s.dbData;
        
        if isempty(windowSize)
            windowSize = floor(window_size_in_sec * dbData(1).samplingRate);
        end
        
        for dbDataItem = dbData
            sigLen = length(dbDataItem.PPG);
            
            for iwin = 1:windowSize:sigLen
                if iwin+windowSize-1 > sigLen
                    continue
                end

                range = iwin:iwin+windowSize-1;

                makeAndSaveWindow(dbDataItem, range);
                if counter >= stopIndex
                    return;
                end
            end
        end
    end
    
    %% aux functions
    function makeAndSaveWindow(dbDataItem, range)
        if counter >= startIndex
            if counter == startIndex
                disp(['STARTED from ' num2str(counter)]);
            end
            ppg_win = dbDataItem.PPG(range);
            bp_win = dbDataItem.BP(range);

            [stftImage, cutoffFreq] = ppgStft(ppg_win, dbDataItem.samplingRate);
            [sys, dias] = bpCalc(bp_win);
            pulse = pulseCalc(bp_win, dbDataItem.samplingRate);
            
            fileName1 = sprintf('im_%0.8d__win_%0.3d__sys_%g__dias_%g__pulse_%g__ovlp_%g__fs_%g_%g', counter, stft_window_size_in_sec, sys, dias, pulse, stft_overlap, cutoffFreq);
            ff = sprintf('%s/%s.%s', outputFolder, fileName1, 'mat');

            % norm and save:
            stftImage = (stftImage - min(stftImage(:)))./max(stftImage(:));

            isFailing = true;
            while isFailing
                try
                    imwrite(stftImage, ff);
                    isFailing = false;
                catch e
                    disp(['Catched error (' e.identifier '): ' e.message]);
                    disp(e.stack);
                    java.lang.System.gc();
                end
            end
        end
        counter = counter + 1;
    end
    
    function [stftImage, cutoffFreq] = ppgStft(ppgWin, sampling_rate)
        % create STFT image
        [~,~,stftImage,~,~, cutoffFreq,~] = ...
            Stft.CreateStft(ppgWin, sampling_rate, ...
            stft_window_size_in_sec, stft_overlap, stft_cutoff_freq);
    end

    function [systolic, diastolic] = bpCalc(bpWin)
        % continuous BP to numeric sys, diast bp values
        p = findpeaks(bpWin);
        systolic = mean(p(p >= mean(p)));
        p = -findpeaks(-bpWin);
        diastolic = mean(p(p <= mean(p)));
        
        if nargout == 1
            systolic = [systolic, diastolic];
        end
    end

    function pulse = pulseCalc(bpWin, sr)
        % get pulse in BPM
        pulse = SignalAnalyzer.FindDominantFrequency(bpWin, sr)*60;
    end
end



