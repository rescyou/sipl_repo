function QuickAddOfPulse_Temp( organizedDbInputFolder, outputFolder, ...
    window_size_in_sec, ...
    stft_window_size_in_sec, stft_cutoff_freq, stft_overlap)
%MAKEDBIMAGES Make db for NN

    %% init 1
    extention = 'png';
    
    startIndex = 1;%750064;
    
    %% init 2
    windowSize = [];
    
    %% make output folder
    if ~exist(outputFolder,'dir')
        disp(['Making folder: ' outputFolder]);
        mkdir(outputFolder);
    end
    
    %% read inputs
    files = dir(organizedDbInputFolder);

    filenames = Utils.sort_nat({files.name});
    filenames = filenames(3:end);
    
    %%
    counter = 1;
    
    for ifile = 1:length(filenames)
        disp(['Loading... ' filenames{ifile}]);
        s = load([organizedDbInputFolder '/' filenames{ifile}]);
        dbData = s.dbData;
        
        if isempty(windowSize)
            windowSize = floor(window_size_in_sec * dbData(1).samplingRate);
        end
        
        for dbDataItem = dbData
            sigLen = length(dbDataItem.PPG);
            
            for iwin = 1:windowSize:sigLen
                if iwin+windowSize-1 > sigLen
                    continue
                end

                range = iwin:iwin+windowSize-1;

                makeAndSaveWindow(dbDataItem, range);
            end
        end
    end
    
    %% aux functions
    function makeAndSaveWindow(dbDataItem, range)
        if counter >= startIndex
            if counter == startIndex
                disp(['STARTED from ' num2str(counter)]);
            end
            %ppg_win = dbDataItem.PPG(range);
            bp_win = dbDataItem.BP(range);

            %[stftImage, cutoffFreq] = ppgStft(ppg_win, dbDataItem.samplingRate);
            %[sys, dias] = bpCalc(bp_win);
            pulse = pulseCalc(bp_win, dbDataItem.samplingRate);
            
            pulseTxt = sprintf('__pulse_%g', pulse);
            
            dd = dir(sprintf('%s/im_%0.8d*', outputFolder, counter));
            
            idxes = strfind(dd.name, '__');
            
            if (length(idxes) == 3)
                fileName1 = [dd.name(1:end-4) pulseTxt dd.name(end-3:end)];
            else
                fileName1 = [dd.name(1:idxes(4)-1) pulseTxt dd.name(idxes(4):end)];
            end
            
            old = sprintf('%s/%s', outputFolder, dd.name);
            new = sprintf('%s/%s', outputFolder, fileName1);
            
            %disp(['Moving ''' old ''' to ''' new '''']);
            movefile(old, new);
        end
        counter = counter + 1;
    end
    
    function [stftImage, cutoffFreq] = ppgStft(ppgWin, sampling_rate)
        % create STFT image
        [~,~,stftImage,~,~, cutoffFreq,~] = ...
            Stft.CreateStft(ppgWin, sampling_rate, ...
            stft_window_size_in_sec, stft_overlap, stft_cutoff_freq);
    end

    function [systolic, diastolic] = bpCalc(bpWin)
        % continuous BP to numeric sys, diast bp values
        p = findpeaks(bpWin);
        systolic = mean(p(p >= mean(p)));
        p = -findpeaks(-bpWin);
        diastolic = mean(p(p <= mean(p)));
        
        if nargout == 1
            systolic = [systolic, diastolic];
        end
    end

    function pulse = pulseCalc(bpWin, sr)
        % get pulse in BPM
        pulse = SignalAnalyzer.FindDominantFrequency(bpWin, sr)*60;
    end
end



