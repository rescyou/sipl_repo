function outputFolder = FindMeanImageAndSubtract( imagesFolderPath )
%MakeDatabaseStructFromImages make mean image, subtract from all images,
%and save in a new folder: <imagesFolderPath>_nomean

    files = dir(imagesFolderPath);
    files = files(~[files.isdir]);
    N = length(files);
    
    meanImg = 0;
    
    for i = 1:N
        meanImg = meanImg + imread(strcat(files(i).folder,'\',files(i).name));
    end
    meanImg = meanImg ./ N;
    imshow(meanImg);
    
    outputFolder = [imagesFolderPath '_nomean'];
    
    if ~exist(outputFolder,'dir')
        disp(['Making folder: ' outputFolder]);
        mkdir(outputFolder);
    end
    
    for i = 1:N
        im = imread(strcat(files(i).folder,'\',files(i).name), 'png');
        im = im - meanImg;
        
        im = (im - min(im(:)));
        im = im ./ max(im(:)) * 255;
        
        imwrite(im, strcat(outputFolder,'\',files(i).name));
    end

end

