function [ databaseStruct ] = MakeDatabaseStructFromImages( imagesFolderPath, outputfileName )
%MAKEDATABASESTRUCTFROMIMAGES Makes a struct with a table of all paths to
%images
% DEPRECATED
    if nargin < 2
        outputfileName = 'databaseStruct';
    end

    files = dir(imagesFolderPath);
    files = files(~[files.isdir]);
    N = length(files);
    
    fullPathFnc = @(filename) strcat(files(1).folder,'\',filename);
    %imInfo = imfinfo(fullPathFnc(files(1).name));
    
    systolic = nan(N, 1);
    diastolic = nan(N, 1);
    pulse = nan(N, 1);
    
    stftParams = [];
    
    for fi = 1:N
        parts = strsplit(files(fi).name(1:end-4),{'_'});
        systolic(fi) = str2double(parts{6});
        diastolic(fi) = str2double(parts{8});
        pulse(fi) = str2double(parts{10});
        if isempty(stftParams) && length(parts) > 10
            stftParams.windowSizeInSec = str2double(parts{4});
            stftParams.cutoffFrequencies = str2double(parts(14:15));
            stftParams.overlap = str2double(parts{12});
        end
    end
    
    fileName = {files.name}';
    databaseStruct.bigDataTbl = table(fileName, systolic, diastolic, pulse);
    databaseStruct.imagesFolder = files(1).folder;
    %databaseStruct.imageType = imInfo.Format;
    databaseStruct.imageSize = [1 3750 1];%[imInfo.Height, imInfo.Width, 1];
    databaseStruct.stftParams = stftParams;
    databaseStruct.getTblFunc_SysOnly = @(ds) fixTbl(ds, 1);
    databaseStruct.getTblFunc_DiasOnly = @(ds) fixTbl(ds, 2);
    databaseStruct.getTblFunc_PulseOnly = @(ds) fixTbl(ds, 3);
    databaseStruct.fullGetTblFunc = @fixTbl;
    databaseStruct.getTblFunc = @(ds) fixTbl(ds);
    f = sprintf([outputfileName '.mat']);
    disp(['Saving MAT: ' f]);
    save(f, 'databaseStruct');

end

function t = fixTbl(ds, idxes, noNans)
    if nargin < 3 || isempty(noNans)
        noNans = true;
    end
    
    if nargin < 2 || isempty(idxes)
        idxes = size(ds.bigDataTbl, 2) - 1;
    end

    idxes = idxes + 1;

    if noNans
        selectedIdx = any(~isnan(ds.bigDataTbl{:,idxes}), 2);
    else
        selectedIdx = 1:size(ds.bigDataTbl, 1);
    end

    fileName = strcat(ds.imagesFolder, '\', ds.bigDataTbl{selectedIdx,1});

    t = [table(fileName) ds.bigDataTbl(selectedIdx,idxes)];

end

