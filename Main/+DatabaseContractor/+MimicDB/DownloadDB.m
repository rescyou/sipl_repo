function [ outputFolder, BP_Type ] = ...
    DownloadDB( outputFolder, BP_Type, N, N0, recordsToDownloadIdx, useCompressedVersion, sizeOfPartsToDownload, override )
%DOWNLOADDB download the BP and PPG records from the DB
%   Creates mat files in outputFolder:
%   record_XXX.mat containing BP and PPG structs
%
%   useCompressedVersion - removes unnecessary data from the structs if true
%       true by default

    %% init
    if nargin < 2 || isempty(BP_Type)
        BP_Type = 'ABP';
    end
    
    if nargin < 3
        N = [];
    end
    
    if nargin < 4
        N0 = [];
    end
    
    if nargin < 5 || isempty(recordsToDownloadIdx)
        recordsToDownloadIdx = 1;
    end
    
    if nargin < 6 || isempty(useCompressedVersion)
        useCompressedVersion = true;
    end
    
    if nargin < 7 || isempty(sizeOfPartsToDownload)
        sizeOfPartsToDownload = 1e6;
    end
    
    if nargin < 8 || isempty(override)
        override = false;
    end
    
    %% make output folder
    if ~exist(outputFolder,'dir')
        disp(['Making folder: ' outputFolder]);
        mkdir(outputFolder);
    end
    
    %% load record names
    load('+MimicDbToolbox\recordsInfos.mat');
    record = recordsInfos(cellfun(@(x) strcmp(x, BP_Type), {recordsInfos.type}));
    assert(length(record) == 1);
    
    %% download DB - init and go over all records
    disp(['Downloading DB of ' record.type]);
    
    if useCompressedVersion
        warning('Using Comppressed Mode');
    end
    
    if isscalar(recordsToDownloadIdx)
        recordsToDownloadIdx = recordsToDownloadIdx:length(record.recordNames);
    else
        recordsToDownloadIdx = unique(min(recordsToDownloadIdx, length(record.recordNames)));
    end
    
    recordNames = record.recordNames;
    recordType = record.type;
    %progressCounter = [];
    %progressBitmap = zeros(1, length(startIdx:endIdx));
    %progressBitmapLen = length(progressBitmap);
    parfor ii = 1:length(recordsToDownloadIdx)
        i = recordsToDownloadIdx(ii);
        currJob = getCurrentTask();
        prefix = sprintf('%d: record#%d >> ', currJob.ID, i);
        
        disp ([prefix '====== Starting ' num2str(i) ' out of ' num2str(length(recordsToDownloadIdx)) ' | at ' datestr(now, 'dd-mmm-yyyy HH:MM:SS') ' ======']);
        rn = recordNames{i};
        t = tic();
        
        %% split the download into groups (smaller files to download to avoid out of memory errors)
        groupsStartIdx = round(linspace(N0, N, (N-N0)/sizeOfPartsToDownload + 1));
        groupsEndIdx = N;%groupsStartIdx(2:end)-1;
        groupsStartIdx = N0;%groupsStartIdx(1:end-1);
        
        for ig = 1:length(groupsStartIdx)
            tt = tic;
            startPoint = groupsStartIdx(ig);
            endPoint = groupsEndIdx(ig);
            
            outputFileName = sprintf('%s/record_%0.8d_part_%0.8d', outputFolder, i, ig);
            if ~override && ~~exist([outputFileName '.mat'], 'file')
                fprintf('%s     ====== Skipping part %d... %d sec ======\n', prefix, ig, toc(tt));
                continue;
            end
            
            fprintf('%s     ====== Reading part %d out of %d parts | [%d, %d] => %.1e samples ====== at %s\n', ...
                prefix, ig, length(groupsStartIdx), startPoint, endPoint, endPoint-startPoint+1, datestr(now, 'dd-mmm-yyyy HH:MM:SS'));
            
            sigs = MimicDbToolbox.DownloadSignals(rn, {recordType, 'PLETH'}, endPoint, startPoint, false, [prefix '     ']);
            
            if isempty(sigs)
                fprintf('%s     ====== OUT OF REGION - skipped part %d... %d sec ======\n', prefix, ig, toc(tt));
                break;
            end

            BP = sigs(1);
            PPG = sigs(2);

            if useCompressedVersion
                BP = compressStruct(BP);
                PPG = compressStruct(PPG);
            end
            
            parsave(outputFileName, BP, PPG);
            
            fprintf('%s     ====== Done part %d... %d sec ======\n', prefix, ig, toc(tt));
            %disp(' ');
        end
        
        t = toc(t);
        disp ([prefix '====== Done ' num2str(i) ' - ' num2str(t) ' sec ======']);
        
        %% calc progress
        %progressCounter(getCurrentJob().ID) = progressCounter(getCurrentJob().ID)+1;
        %progressBitmap(i) = 1;
        %pSum = sum(progressBitmap);
        
        %fprintf('>>>>> PROGRESS %d% (%d out of %d) <<<<<\n', pSum/progressBitmapLen, pSum, progressBitmapLen);
        %disp(' ');
        %disp(' ');
    end

end

function parsave(fname, BP, PPG)
    save(fname, 'BP', 'PPG')
end

function s = compressStruct(s)
    s.timeInSec = nan;
end
