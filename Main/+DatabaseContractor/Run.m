clc

% install mimic toolbox
%MimicDbToolbox.InstallMimicToolbox();
addpath('wfdb-app-toolbox-0-9-10/mcode');

% set params
dbRawFolder = 'DB_Raw_Data';
dbOrganizedFolder = 'DB_Organized_Data';
dbStftImagesFolder = 'DB_Stft_Images';
dbLabeledSignalsFolder = 'DB_Labeled_Signals';
dbMatDataFolder = 'DB_All_Data_Mats';

partsToDo = 1;

% more settings - don't touch
getDiaryName = @(x) ['LOGS/Log__' strrep(x,' ','_') '__' datestr(now, 'dd-mm-yyyy_HH-MM-SS') '.txt'];
shouldIRun = @(myPartsIndex) any(partsToDo == myPartsIndex);
diary off

%% Download DB
% MIMIC_Online_Database -> dbRawFolder
if exist('shouldIRun', 'var') && shouldIRun(1)
    diary(getDiaryName('Download DB'));
    disp('### Download DB');
    
    N0 = 5e5;
    N = 1e7 + N0;
    sizeOfEachPart = []; % 1e6 by default
    recordsIndexes = 1:1700; %existingRecordsIdx
    
    t = tic;
    DatabaseContractor.MimicDB.DownloadDB(dbRawFolder, 'ABP', N, N0, 1, true, [], false);
    disp(['time: ' num2str(toc(t))]);
    
    diary off
end

%% Organize DB
% dbRawFolder -> dbOrganizedFolder
if exist('shouldIRun', 'var') && shouldIRun(2)
    diary(getDiaryName('Organize DB'));
    disp('### Organize DB');
    
    t = tic;
    DatabaseContractor.OrganizedDB.OrganizeDB(dbRawFolder, dbOrganizedFolder);
    disp(['time: ' num2str(toc(t))]);

    diary off
end

%% Make stft images
% dbOrganizedFolder -> dbStftImagesFolder
if exist('shouldIRun', 'var') && shouldIRun(3)
    diary(getDiaryName('Make data for the NN'));
    disp('### Make data for the NN');
    
    window_size_in_sec = 30;
    stft_window_size_in_sec = 5;
    %stft_cutoff_freq = [0.5 5];
    stft_cutoff_freq = [-inf 12];
    stft_overlap = 0.95;
    
    t = tic;
    DatabaseContractor.StftImagesDB.MakeDbImages(dbOrganizedFolder, dbStftImagesFolder, ...
        window_size_in_sec, ...
        stft_window_size_in_sec, stft_cutoff_freq, stft_overlap);
    disp(['time: ' num2str(toc(t))]);

    % Make DatabaseStruct
    % Makes a struct with a table of all paths to the images
    t = tic;
    DatabaseContractor.StftImagesDB.MakeDatabaseStructFromImages(dbStftImagesFolder, [dbStftImagesFolder '_Table']);
    disp(['time: ' num2str(toc(t))]);
    
    diary off
end

%% Make raw signals data
% dbOrganizedFolder -> dbLabeledSignalsFolder
if exist('shouldIRun', 'var') && shouldIRun(4)
    diary(getDiaryName('Make data for the NN - VECTORS 2'));
    disp('### Make data for the NN - VECTORS');
    
    window_size_in_sec = 30;
    
    t = tic;
    DatabaseContractor.RawSignalsDB.MakeDbForNN(dbOrganizedFolder, dbLabeledSignalsFolder, window_size_in_sec);
    disp(['time: ' num2str(toc(t))]);
    
    diary off
end

%% Make mat files data
% dbOrganizedFolder-> dbMatDataFolder
if exist('shouldIRun', 'var') && shouldIRun(5)
    diary(getDiaryName('Make data for the NN'));
    disp('### Make data for the NN');
    
    window_size_in_sec = 30;
    stft_window_size_in_sec = 5;
    stft_cutoff_freq = [0.5 5];
    stft_cutoff_freq = [-inf 30];
    stft_overlap = 0.95;
    
    t = tic;
    DatabaseContractor.MatDB.MakeDbMats(dbOrganizedFolder, dbMatDataFolder, ...
        window_size_in_sec, ...
        stft_window_size_in_sec, stft_cutoff_freq, stft_overlap);
    disp(['time: ' num2str(toc(t))]);

    diary off
end
