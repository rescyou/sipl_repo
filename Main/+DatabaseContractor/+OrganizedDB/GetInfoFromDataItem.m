function [ data ] = GetInfoFromDataItem( dataItemFromOrganizedDb, ...
    range_in_secs, ...
    stft_window_size_in_sec, stft_cutoff_freq, stft_overlap, ...
    skipIfNotInNormalRange, plotMe )
%GETINFOFROMDATAITEM Summary of this function goes here
%   Detailed explanation goes here

    %% init
    errs = [];
    dbDataItem = dataItemFromOrganizedDb;
    
    if nargin < 6 || isempty(skipIfNotInNormalRange)
        skipIfNotInNormalRange = false;
    end
    if nargin < 7 || isempty(plotMe)
        plotMe = nargout == 0;
    end
    
    range = floor(range_in_secs .* dbDataItem.samplingRate) + 1;
    range = range(1):range(end);

    %% get windows
    ppg_win = dbDataItem.PPG(range);
    bp_win = dbDataItem.BP(range);
    
    %% sys and dias
    [sys, dias] = bpCalc(bp_win);
    is_bp_in_normal_range = true;
    
    if ~(sys >= 70 && sys <= 190 && dias >= 40 && dias <= 100)
        is_bp_in_normal_range = false;
        if skipIfNotInNormalRange
            data = [];
            return;
        end
    end

    %% stft
    [stft, t, f, stftPower] = ppgStft(ppg_win, dbDataItem.samplingRate);

    %% pulse
    pulse = pulseCalc(bp_win, dbDataItem.samplingRate);

    %% clean ppg
    try
        [ cleanPpg, ~, ~, ~, ~ ] = ...
            PPGAnalyzer.DenoiseAndRemoveBaseline( ppg_win, dbDataItem.samplingRate );
    catch e
        errs = [errs e];
        cleanPpg = nan;
    end

    %% make data
    data = [];
    data.ppg_raw = ppg_win;
    data.ppg_clean = cleanPpg;
    data.ppg_sr = dbDataItem.samplingRate;
    data.stft_abs = abs(stft);
    data.stft_angle = angle(stft);
    data.stft_frqs = f;
    data.stft_time = t;
    data.systolic = sys;
    data.diastolic = dias;
    data.pulse = pulse;
    data.extraction_errors = errs;
    data.is_bp_in_normal_range = is_bp_in_normal_range;
    
    %% plot
    if plotMe
        titleTxt = sprintf('From record %s | BP %g/%g | Pulse %g BPM', ...
            dbDataItem.fromRecord, data.systolic, data.diastolic, data.pulse);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        figure;
        subplot(2,2,1);
        ax1 = plotStft();
        
        subplot(2,2,2);
        ax2 = plotStftAngle();
        
        subplot(2,2,3);
        ax3 = plotRawSig();
        
        subplot(2,2,4);
        ax4 = plotBP();
        
        linkaxes([ax1, ax2], 'xy');
        linkaxes([ax1, ax2, ax3, ax4], 'x');
        axes(ax1)
        axis tight
        suptitle(titleTxt);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        figure;
        subplot(2,1,1);
        ax3 = plotRawSig();
        
        subplot(2,1,2);
        ax4 = plotBP();
        
        linkaxes([ax3, ax4], 'x');
        suptitle(titleTxt);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
    end

    %% aux functions
    function [s, t, f, p] = ppgStft(ppgWin, sampling_rate)
        % create STFT image
        [s, t, f, ~, p] = Stft.CreateStftAdvImg(ppgWin, sampling_rate, ...
            stft_window_size_in_sec, stft_overlap, stft_cutoff_freq);
    end

    function [systolic, diastolic] = bpCalc(bpWin)
        % continuous BP to numeric sys, diast bp values
        p = findpeaks(bpWin);
        systolic = mean(p(p >= mean(p)));
        p = -findpeaks(-bpWin);
        diastolic = mean(p(p <= mean(p)));

        if nargout == 1
            systolic = [systolic, diastolic];
        end
    end

    function pulse = pulseCalc(bpWin, sr)
        % get pulse in BPM
        pulse = SignalAnalyzer.FindDominantFrequency(bpWin, sr)*60;
    end

    %% plot funcs
    function ax = plotStft()
        imagesc(data.stft_time, data.stft_frqs, data.stft_abs);
        xlabel('\DeltaTime [sec]');
        ylabel('Frequency [Hz]');
        title('abs(STFT)');
        ax = gca;
    end

    function ax = plotStftAngle()
        imagesc(data.stft_time, data.stft_frqs, data.stft_angle);
        xlabel('\DeltaTime [sec]');
        ylabel('Frequency [Hz]');
        title('angle(STFT)');
        ax = gca;
    end

    function ax = plotRawSig()
        plot((1:length(data.ppg_raw)) ./ data.ppg_sr, data.ppg_raw, 'LineWidth', 2);
        xlabel('\DeltaTime [sec]');
        title('Raw PPG signal');
        axis tight
        ax = gca;
    end

    function ax = plotBP()
        Lims = [1 length(bp_win)] ./ dbDataItem.samplingRate;
        plot((1:length(bp_win)) ./ dbDataItem.samplingRate, bp_win, ...
            Lims , [sys sys], Lims, [dias dias], ...
            'LineWidth', 2);
        xlabel('\DeltaTime [sec]');
        ylabel('BP [mmHg]');
        title('Blood Pressure');
        legend('Continuous Arterial BP', 'Numeric Systolic BP', 'Numeric Diastolic BP');
        axis tight
        ax = gca;
    end
end