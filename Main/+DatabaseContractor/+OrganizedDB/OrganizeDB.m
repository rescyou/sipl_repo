function [ totalDbTimeInSec ] = OrganizeDB( inputFolder, outputFolder )
%ORGANIZEDB Split signals in DB
%   inputFolder - folder with DB mat files (created by DownloadDB)

    %% init
    totalDbTimeInSec = 0;
    
    saveEveryBytes = 2^30; % 1GB
    
    %% make output folder
    if ~exist(outputFolder,'dir')
        disp(['Making folder: ' outputFolder]);
        mkdir(outputFolder);
    end
    
    %% get mat files
    files = dir(inputFolder);

    filenames = Utils.sort_nat({files.name});
    filenames = filenames(3:end);
    
    %% 
    dbData = [];
    dbSavedPartsConter = 1;
    partsConter = 0;
    
    for i = 1:length(filenames)
        fprintf('file %d out of %d\t', i, length(filenames));
        t = tic;
        dd = load([inputFolder '/' filenames{i}]);
        
        BP = dd.BP;
        PPG = dd.PPG;
        
        if PPG.input_N0 > PPG.endIndex_N
            fprintf('\tOUT OF REGION... skipped\n');
            continue;
        end

        abp = BP.signals(:,1);
        ppg = PPG.signals(:,1);

        assert(BP.samplingRate(1) == PPG.samplingRate(1));

        bitMap = ~isnan(abp) & ~isnan(ppg);

        abp_parts_to_add = Utils.SplitSignal(abp, bitMap);
        ppg_parts_to_add = Utils.SplitSignal(ppg, bitMap);
        
        data = [];
        for j = 1:length(ppg_parts_to_add)
            data.BP = abp_parts_to_add{j};
            data.PPG = ppg_parts_to_add{j};
            data.samplingRate = PPG.samplingRate(1);
            data.fromRecord = PPG.recordName;
            data.bpType = BP.type;
            
            dbData = [dbData data];
            
            totalDbTimeInSec = totalDbTimeInSec + length(ppg_parts_to_add{j}) / PPG.samplingRate(1);
            partsConter = partsConter + 1;
        end

        fprintf('\tfinished... %d sec | %d parts | total %d parts | total %d hr | total %d days\n', ...
            toc(t), length(ppg_parts_to_add), partsConter, totalDbTimeInSec/60/60, ...
            totalDbTimeInSec/60/60/24);
        
        %% save of needed
        s = whos('dbData');
        if s.bytes > saveEveryBytes
            saveMe();
            dbSavedPartsConter = dbSavedPartsConter + 1;
            dbData = [];
        end
        
    end
    
    %%
    saveMe();
    
    %%
    function saveMe()
        if isempty(dbData)
            return
        end
        disp('saving...');
        save([outputFolder '/dbData_' num2str(dbSavedPartsConter)], 'dbData');
    end

end



