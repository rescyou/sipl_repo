function [ppgMatrix, resultsMatrix] = MakeDbForNN(organizedDbInputFolder, outputFolder, window_size_in_sec)
%MAKEDBIMAGES Make db for NN - trying to put all DB in one mat file...
%This is RAW signals db
%DEPRECATED

    %% init 1
    startIndex = 1;
    stopIndex = 0.6e6;
    N = stopIndex - startIndex + 1;
    
    %% init 2
    windowSize = [];
    
    %% make output folder
    if ~exist(outputFolder,'dir')
        disp(['Making folder: ' outputFolder]);
        mkdir(outputFolder);
    end
    
    %% read inputs
    files = dir(organizedDbInputFolder);

    filenames = Utils.sort_nat({files.name});
    filenames = filenames(3:end);
    
    %% define outputs
    ppgMatrix = [];
    resultsMatrix = [];
    
    %% Main loop
    done = false;
    counter = 1;
    
    % over files
    for ifile = 1:length(filenames)
        disp(['Loading... ' filenames{ifile} '... counter = ' num2str(counter)]);
        s = load([organizedDbInputFolder '/' filenames{ifile}]);
        dbData = s.dbData;
        
        if isempty(windowSize)
            windowSize = floor(window_size_in_sec * dbData(1).samplingRate);
            %ppgMatrix = zeros(N, windowSize);
            %resultsTable = table(zeros(N,1), zeros(N,1), zeros(N,1));
            %resultsTable.Properties.VariableNames = {'systolic','diastolic','pulse'};
            resultsMatrix = zeros(N,3);
            resultsMatrixHeaders = {'systolic','diastolic','pulse'};
        end
        
        % over info in files
        for dbDataItem = dbData
            sigLen = length(dbDataItem.PPG);
            
            for iwin = 1:windowSize:sigLen
                if iwin+windowSize-1 > sigLen
                    continue
                end

                range = iwin:iwin+windowSize-1;
                
                if counter >= startIndex
                    if counter == startIndex
                        disp(['STARTED from ' num2str(counter)]);
                    end
                    makeAndSaveWindow(dbDataItem, range);
                end
                
                counter = counter + 1;
                
                if counter >= stopIndex
                    done = true;
                    break;
                end
            end
            if done
                break;
            end
        end
        if done
            break;
        end
    end
    
    %% save all
    ff = sprintf('%s/%s.%s', outputFolder, 'nnData', 'mat');        
    save(ff, 'ppgMatrix', 'resultsMatrix', 'resultsMatrixHeaders', '-V7.3');
    
    %% aux functions
    function makeAndSaveWindow(dbDataItem, range)
        ppg_win = dbDataItem.PPG(range);
        bp_win = dbDataItem.BP(range);
        
        [sys, dias] = bpCalc(bp_win);
        pulse = pulseCalc(bp_win, dbDataItem.samplingRate);
        
        ppgMatrix(counter, :) = ppg_win(:)';
        resultsMatrix(counter, :) = [sys, dias, pulse];
    end

    function [systolic, diastolic] = bpCalc(bpWin)
        % continuous BP to numeric sys, diast bp values
        p = findpeaks(bpWin);
        systolic = mean(p(p >= mean(p)));
        p = -findpeaks(-bpWin);
        diastolic = mean(p(p <= mean(p)));
        
        if isempty(systolic)
            systolic = nan;
        end
        
        if isempty(diastolic)
            diastolic = nan;
        end
        
        if nargout == 1
            systolic = [systolic, diastolic];
        end
    end

    function pulse = pulseCalc(bpWin, sr)
        % get pulse in BPM
        pulse = SignalAnalyzer.FindDominantFrequency(bpWin, sr)*60;
        if isempty(pulse)
            pulse = nan;
        end
    end
end



