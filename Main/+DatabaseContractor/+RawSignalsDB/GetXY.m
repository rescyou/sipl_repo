function [ X, Y_regression, Y_classification ] = GetXY( myDb, typeIdx, numberOfBuckets )
%GETXY Summary of this function goes here
%   Detailed explanation goes here

    if isempty(myDb)
        NewDatabase.LoadDb;
        myDb = evalin('base', 'myDb');
    end
    if nargin < 2
        typeIdx = 1;
    end
    if nargin < 3
        numberOfBuckets = 50;
    end
    
    fprintf('Using only %s\n', myDb.resultsMatrixHeaders{typeIdx});
    
    Y_regression = myDb.resultsMatrix(:, typeIdx);
    
    nanIdx = isnan(Y_regression);
    Y_regression = Y_regression(~nanIdx);
    X = myDb.ppgMatrix(:,:,:,~nanIdx);
    
    if nargout >= 3
        [p, c] = lloyds(Y_regression, numberOfBuckets);
        [i, Y_classification] = quantiz(Y_regression, p, c);
        Y_classification = categorical(Y_classification)';
    end

end

