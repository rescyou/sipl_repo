function [ pulse ] = signal2pulse( bloodPressureSignal, samplingRate )
%SIGNAL2PULSE get pulse in BPM

    pulse = SignalAnalyzer.FindDominantFrequency(bloodPressureSignal, samplingRate) * 60;

end

