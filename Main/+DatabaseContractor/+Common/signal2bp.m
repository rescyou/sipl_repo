function [ systolic, diastolic ] = signal2bp( bloodPressureSignal )
%SIGNAL2BP get continuous BP to numeric sys, diast bp values

    % continuous BP to numeric sys, diast bp values
    p = findpeaks(bloodPressureSignal);
    systolic = mean(p(p >= mean(p)));
    p = -findpeaks(-bloodPressureSignal);
    diastolic = mean(p(p <= mean(p)));

    if nargout == 1
        systolic = [systolic, diastolic];
    end

end

