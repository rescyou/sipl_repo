classdef PARAMS
    %PARAMS params for database
    
    properties (Constant)
        CUTOFF_FRQS = [0.5 20];
        
        TESTS_FOLDER = 'Tests';
        
    end
    
    methods (Static)
        function [testName, testFolderPath, logPath] = startTest(title)
            clc;
            d = dbstack(1);
            
            if ~isempty(d)
                mfile = d.file;
            else
                mfile = 'UNKNOWN';
            end
            
            testName = ['Test_' mfile '__' strrep(title,' ','_') '___' datestr(now, 'dd-mm-yyyy_HH-MM-SS')];
            
            testFolderPath = [PARAMS.TESTS_FOLDER '\' testName];
            
            Utils.MakeFolder(testFolderPath);
            
            logPath = [testFolderPath '\log.txt'];
            diary(logPath);
            disp(['Test folder path: ' testFolderPath]);
        end
    end
    
end

