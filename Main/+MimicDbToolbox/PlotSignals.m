function PlotSignals( PPG, BP, mode )
%PLOTSIGNALS Summary of this function goes here
%   Detailed explanation goes here
    
    if nargin < 3
        mode = 2;
    end

    titleTxt = ['PPG and BP from record ' PPG.recordName];
    X = 'Time [sec]';
    
    switch mode
        case 0
            plot(PPG.timeInSec, PPG.signals, BP.timeInSec, BP.signals);
            title(titleTxt);
            legend([PPG.signalsNames(:); BP.signalsNames(:)]);
            xlabel(X);
        case 1
            yyaxis left
            plot(PPG.timeInSec, PPG.signals);
            ylabel(PPG.units);
            
            yyaxis right
            plot(BP.timeInSec, BP.signals);
            ylabel(BP.units);
            
            legend([PPG.signalsNames(:); BP.signalsNames(:)]);
            xlabel(X);
            
            title(titleTxt);
        case 2
            subplot(1, 2, 1);
            plot(PPG.timeInSec, PPG.signals);
            %ylabel(PPG.units);
            xlabel(X);
            legend(PPG.signalsNames);
            ax1 = gca;
            title('Photoplethysmogram');
            
            subplot(1, 2, 2);
            plot(BP.timeInSec, BP.signals);
            ylabel(BP.units);
            xlabel(X);
            legend(BP.signalsNames);
            ax2 = gca;
            title('Blood Pressure');
            
            linkaxes([ax1, ax2], 'x');
            suptitle(titleTxt);
    end
end

