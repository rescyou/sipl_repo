function [ SIGNALS_OUT ] = DownloadSignals( recordFolderName, signalTypes, N, N0, isNumericRecord, prefixTxt )
%DOWNLOADSIGNALS returns a signal from a record
%   recordFolder is like 30/3000086/
%   N by default it reads all of the file
    
    if nargin < 3
        N = [];
    end
    
    if nargin < 4 || isempty(N0) || N0 < 1
        N0 = 1;
    end
    
    if nargin < 5 || isempty(isNumericRecord)
        isNumericRecord = false;
    end
    
    if nargin < 6 || ~ischar(prefixTxt)
        prefixTxt = '';
    end
    
    if ischar(signalTypes)
        signalTypes = {signalTypes};
    end
    
    input_N0 = N0;
    input_N = N;
    
    %% get record number
    recordNumber = strsplit(recordFolderName, '/');
    recordNumber = recordNumber{2};
    
    %% define record names
    baseRecordName = ['mimic2wdb/' recordFolderName];
    nRecord = [baseRecordName recordNumber 'n'];
    waveRecord = [baseRecordName recordNumber];
    
    %% make full record path
    if isNumericRecord
        fullRecordPath = nRecord;
    else
        fullRecordPath = waveRecord;
    end
    
    %%
    disp([prefixTxt 'Reading record: ' fullRecordPath ' for ' strjoin(signalTypes, ', ')]);
    
    %% get signal info
    disp([prefixTxt 'Reading info...']);
    t = tic;
    [allIds, SIGNALS, IDS, siginfo] = ...
        GetSignalInRecordInfo(fullRecordPath, signalTypes);
    t = toc(t);
    disp([prefixTxt 'Done reading info - ' num2str(t) ' sec; signal is ' num2str(SIGNALS{1}.fullLengthSec) ' sec long; with ' num2str(SIGNALS{1}.fullLengthSamples) ' samples']);
    
    %% check duration
    SamplesNum = [SIGNALS{:}];
    SamplesNum = min([SamplesNum.fullLengthSamples]);
    
    if isempty(N)
        N = SamplesNum;
    end
    
    if N > SamplesNum
%         if N0 > SamplesNum
%             SIGNALS_OUT = [];
%             return;
%         end
        
        N0 = max(1, N0 - (N - SamplesNum));
        N = SamplesNum;
    end
    
    %% read
    fprintf('%sReading signals data... [%d, %d] => %.1e samples => %d sec\n', ...
        prefixTxt, N0, N, N-N0+1, (N-N0)./SIGNALS{1}.samplingRate(1));
    
    t = tic;
    
    SIGNALS_tm = [];
    SIGNALS_signals = [];
    
    sizeOfPartsToDownload = 1e6;
    groupsStartIdx = round(linspace(N0, N, ceil((N-N0)/sizeOfPartsToDownload) + 1));
    groupsEndIdx = groupsStartIdx(2:end)-1;
    groupsStartIdx = groupsStartIdx(1:end-1);
    
    for ig = 1:length(groupsStartIdx)
        startPoint = groupsStartIdx(ig);
        endPoint = groupsEndIdx(ig);
        fprintf('%s     Reading part [%d, %d] => %.1e samples\n', prefixTxt, startPoint, endPoint, endPoint-startPoint+1);
        tt = tic;
        
        isDone = false;
        while ~isDone
            try
                [SIGNALS_tm_TEMP, SIGNALS_signals_TEMP, ~] = rdsamp(fullRecordPath, allIds, endPoint, startPoint);
                isDone = true;
            catch
                fprintf('%s     Failed rdsamp... trying again\n', prefixTxt);
            end
        end
        
        SIGNALS_tm = [SIGNALS_tm; SIGNALS_tm_TEMP];
        SIGNALS_signals = [SIGNALS_signals; SIGNALS_signals_TEMP];
        fprintf('%s     Done... %d sec\n', prefixTxt, toc(tt));
    end
    t = toc(t);
    disp([prefixTxt 'Done reading signals data - ' num2str(t) ' sec']);
    
    %% return structs
    SIGNALS_OUT = [];
    for i = 1:length(IDS)
        SIGNAL = SIGNALS{i};
        [~, ids] = ismember(IDS{i}, allIds);
        
        SIGNAL_tm = SIGNALS_tm;
        SIGNAL_signals = SIGNALS_signals(:, ids);
        
        SIGNAL.type = signalTypes{i};
        SIGNAL.timeInSec = SIGNAL_tm;
        SIGNAL.durationInSec = SIGNAL_tm(end) - SIGNAL_tm(1); % sec
        SIGNAL.signals = SIGNAL_signals;

        SIGNAL.recordName = recordFolderName;
        SIGNAL.fullRecordPath = fullRecordPath;
        SIGNAL.indexesInRecord = IDS{i};

        SIGNAL.input_isNumericRecord = isNumericRecord;
        SIGNAL.input_N0 = input_N0;
        SIGNAL.input_N = input_N;

        SIGNAL.startIndex_N0 = N0;
        SIGNAL.endIndex_N = N;
        
        SIGNALS_OUT = [SIGNALS_OUT SIGNAL];
    end

end

function [allIds, SIGNALS, IDS, siginfo] = ...
    GetSignalInRecordInfo(recordAddr, signalDescriptions)

    [siginfo, Fs] = wfdbdesc(recordAddr);
    allIds = [];
    SIGNALS = [];
    for i = 1:length(signalDescriptions)
        ids = find(cellfun(@(x) ~isempty(strfind(x, signalDescriptions{i})), {siginfo.Description}));
        allIds = [allIds ids];
        SIGNAL.samplingRate = Fs(ids); % Hz
        SIGNAL.fullLengthSamples = [siginfo(ids).LengthSamples];
        SIGNAL.fullLengthSec = SIGNAL.fullLengthSamples ./ SIGNAL.samplingRate;
        SIGNAL.signalsNames = {siginfo(ids).Description};
        SIGNAL.gain = {siginfo(ids).Gain};
        SIGNAL.units = gain2unit(SIGNAL.gain);
        SIGNALS{i} = SIGNAL;
        IDS{i} = ids;
    end
end

function s = gain2unit(g)
    s = g{1};
    s = strsplit(s, '/');
    if length(s) > 1
        s = s{2};
    else
        s = '';
    end
end
