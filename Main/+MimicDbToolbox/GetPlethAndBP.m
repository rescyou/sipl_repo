function [ PPG, BP ] = GetPlethAndBP( recordFolder, secondsToRead, plotMe, recordNamesList )
%GETPLETHANDBP returns NBP and PPG signals from a record
%   recordFolder is like 30/3000086/
%   secondsToRead by default it reads all of the file

    %% check params
    if isnumeric(recordFolder)
        if nargin >= 4 && ~isempty(recordNamesList)
            recordFolder = recordNamesList{recordFolder};
        elseif evalin('base','exist(''recordNames'',''var'') == 1')
            recordFolder = evalin('base',['recordNames{' num2str(recordFolder) '}']);
        else
            s = load('+MimicDbToolbox\recordNames.mat');
            recordFolder = s.recordNames{recordFolder};
        end
    end
    
    assert(sum(recordFolder == '/') == 2, 'wrong format');
    
    if nargin < 2
        secondsToRead = [];
    end
    
    if nargin < 3
        plotMe = nargout == 0;
    end
    
    %%
    disp(['Reading record: ' recordFolder ' for ' num2str(secondsToRead) ' sec']);
    
    %% get record number
    recordNumber = strsplit(recordFolder, '/');
    recordNumber = recordNumber{2};
    
    %% define record names
    baseRecordName = ['mimic2wdb/' recordFolder];
    nRecord = [baseRecordName recordNumber 'n'];
    waveRecord = [baseRecordName recordNumber];
    
    %% get numeric waves info
    disp('Reading NBP info...');
    t = tic;
    [Nbp_Ids, NBP_Fs, Nbp_SamplesNum, NBP_Duration, NBP_Names, NBP_Gain] = ...
        GetSignalInRecordInfo(nRecord, 'NBP');
    t = toc(t);
    disp(['Done NBP info - ' num2str(t) ' sec']);
    
    %% get waves info
    disp('Reading PPG info...');
    t = tic;
    [PPG_Ids, PPG_Fs, PPG_SamplesNum, PPG_Duration, PPG_Names, PPG_Gain] = ...
        GetSignalInRecordInfo(waveRecord, 'PLETH');
    t = toc(t);
    disp(['Done PPG info - ' num2str(t) ' sec']);
    
    %% calc samples2read
    Nbp_samples2read = [];
    PPG_samples2read = [];
    if ~isempty(secondsToRead)
        Nbp_samples2read = round(max(secondsToRead .* NBP_Fs));
        PPG_samples2read = round(max(secondsToRead .* PPG_Fs));
    end
    
    %% read
    disp('Reading NBP signals...');
    t = tic;
    [NBP_tm, NBP_signals, ~]=rdsamp(nRecord, Nbp_Ids, Nbp_samples2read);
    t = toc(t);
    disp(['Done NBP signals - ' num2str(t) ' sec']);
    
    disp('Reading PPG signal...');
    t = tic;
    [PPG_tm, PPG_signals, ~]=rdsamp(waveRecord, PPG_Ids, PPG_samples2read);
    t = toc(t);
    disp(['Done PPG signal - ' num2str(t) ' sec']);
    
    %% plot
    if plotMe
        yyaxis left
        plot(PPG_tm, PPG_signals);
        yyaxis right
        plot(NBP_tm, NBP_signals);
        legend([PPG_Names(:); NBP_Names(:)]);
        xlabel('Time in sec');
    end
    
    %% return structs
    BP.type = 'NBP';
    BP.timeInSec = NBP_tm;
    BP.duration = NBP_tm(end) - NBP_tm(1);
    BP.samplingRate = NBP_Fs;
    BP.signalsNames = NBP_Names;
    BP.signals = NBP_signals;
    BP.gain = NBP_Gain;
    BP.units = gain2unit(NBP_Gain);
    BP.recordName = recordFolder;
    
    PPG.type = 'PLETH';
    PPG.timeInSec = PPG_tm;
    PPG.duration = PPG_tm(end) - PPG_tm(1);
    PPG.samplingRate = PPG_Fs;
    PPG.signalsNames = PPG_Names;
    PPG.signals = PPG_signals;
    PPG.gain = PPG_Gain;
    PPG.units = gain2unit(PPG_Gain);
    PPG.recordName = recordFolder;

end

function [ids, fs, numOfSamples, durationInSec, names, gain] = ...
    GetSignalInRecordInfo(recordAddr, signalDescription)

    [siginfo, Fs] = wfdbdesc(recordAddr);
    
    ids = find(cellfun(@(x) ~isempty(strfind(x, signalDescription)), {siginfo.Description}));
    fs = Fs(ids);
    numOfSamples = [siginfo(ids).LengthSamples];
    durationInSec = numOfSamples ./ fs;
    names = {siginfo(ids).Description};
    gain = {siginfo(ids).Gain};
end

function s = gain2unit(g)
    s = g{1};
    s = strsplit(s, '/');
    if length(s) > 1
        s = s{2};
    else
        s = '';
    end
end



