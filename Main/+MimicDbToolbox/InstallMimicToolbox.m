% Use this script to download and install the Mimic Toolbox for Matlab

%% change this:
% This is the path for the installation folder. Change this if you want
installationFolder = [pwd '\MimicToolbox'];

%% create installation folder if needed
if ~exist(installationFolder,'dir')
    mkdir(installationFolder);
end

%% download toolbox
[old_path]=which('rdsamp');if(~isempty(old_path)) rmpath(old_path(1:end-8)); end
wfdb_url='http://physionet.org/physiotools/matlab/wfdb-app-matlab/wfdb-app-toolbox-0-9-10.zip';
[filestr,status] = urlwrite(wfdb_url,'wfdb-app-toolbox-0-9-10.zip');%Octave users may have to download manually
unzip('wfdb-app-toolbox-0-9-10.zip');
cd wfdb-app-toolbox-0-9-10;cd mcode
addpath(pwd);savepath


