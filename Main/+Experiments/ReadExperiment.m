function [ experiment ] = ReadExperiment( experimentPathOrIndex, baseFolder )
%READEXPERIMENT reads the data from an experiment folder
    
    %% init
    if nargin < 2
        baseFolder = '+Experiments\ExperimentsData';
    end
    
    if isnumeric(experimentPathOrIndex)
        experimentPath = ['+Experiments\ExperimentsData\EXP_' num2str(experimentPathOrIndex)];
    elseif ischar(experimentPathOrIndex)
        if isdir(experimentPathOrIndex)
            experimentPath = experimentPathOrIndex;
        else
            experimentPath = [baseFolder '\' experimentPathOrIndex];
        end
    end
    
    %% read
    files = dir(experimentPath);
    files = files(3:end);
    
    oximeterSignals = [];
    %oximeterSignalsTables = [];
    videoObject = [];
    oximeterSignalsNames = [];
    oximeterSignalsFolders = [];
    
    for f=1:length(files)
        
        filePath = [experimentPath '\' files(f).name];
        
        if files(f).isdir
            xlsPath = findVoltsFile(filePath);
            [ signals, oximeterSignalsNames ] = Experiments.ReadOximeterSignals(xlsPath);
            oximeterSignals = [oximeterSignals {signals}];
            %oximeterSignalsTables = [oximeterSignalsTables {t}];
            oximeterSignalsFolders = [oximeterSignalsFolders {files(f).name}];
        elseif contains(upper(files(f).name), '.MOV')
            videoObject = VideoReader(filePath);
        end
    end
    
    %% make result struct
    experiment.path = experimentPath;
    
    experiment.iphoneVideo = videoObject;
    
    experiment.oximeterSignalsNames = oximeterSignalsNames;
    experiment.oximeterSignalsFolders = oximeterSignalsFolders;
    experiment.oximeterSignals = oximeterSignals;
    
    experiment.iphoneSR = videoObject.FrameRate;
    experiment.oximeterSR = 500;
    %experiment.oximeterSignalsTables = oximeterSignalsTables;

end

function xlsPath = findVoltsFile(folder)
    a = dir(folder);
    a = a(3:end);
    
    indexes = cellfun(@(x) ~isempty(strfind(x, 'Volts')), {a.name});
    xlsPath = cellfun(@(x) {[folder '\' x]}, {a(indexes).name});
    if ~isempty(xlsPath)
        xlsPath = xlsPath{1};
    end
end