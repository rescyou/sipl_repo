%load('Exp1_Signals.mat')
%addpath ../..
oximeterSignalCut = oximeterSignal(5848:end);

oximeterSignalCut2= oximeterSignalCut - mean(oximeterSignalCut);

oximeterSignalCut2 = ...
        SignalAnalyzer.ButterBandpass(oximeterSignalCut2, 500, 4, 0.5, 5);

[ oximeterSignalCut3, baseline, lowPeaksLocs, outX ] = ...
    PPGAnalyzer.RemoveBaselineAlg( oximeterSignalCut2, false, false, 500);

is = iphoneSignal(cyclesLocsIphone(1):end);
os = oximeterSignalCut3(lowPeaksLocs(1):end);

plot((1:length(is)) ./ srIphone, is./max(is), ...
    (1:length(os))./500, os ./ max(os))

%%
ws = 5;
featuresIphone = FeatureExtractor.FeatureExtractor(iphoneSignal, cyclesLocsIphone, ws, srIphone);
featuresOximeter = FeatureExtractor.FeatureExtractor(oximeterSignalCut3, lowPeaksLocs, ws, 500);

%%
Printers.PrintScattersOfStructs(featuresIphone, featuresOximeter(1:length(featuresIphone)), 'iphone','oximeter')

