function [ ] = PrintFeatureExp( varargin )
%UNTITLED2 Summary of this function goes here
%order of input: oxS1E1,oxL1E1,oxS2E1,oxL2E1,ipSE1,ipLE1,oxS1E2,oxL1E2,oxS2E2,oxL2E2,ipSE2,ipLE2,ipFE1,ipFE2,ws
Snames = {'Fox1E1','Fox2E1','FipE1','Fox1E2','Fox2E2','FipE2'};
for kk=1:6
    if mod(kk,3)==0
        SR = varargin{12+kk/3};
    else
        SR = 500;
    end
    norm = @(x) x ./ (sum(x) / SR);
    eval(['[~,',Snames{kk},', featureNames ] =FeatureExtractor.FeatureExtractor( norm(varargin{2*(kk)-1}), varargin{2*kk},varargin{15}, SR);']);
end
for ii=1:length(featureNames)
    figure('units','normalized','outerposition',[0 0 1 1]);
    %     h.FontSize = 22;
    % oximeter oximeter same hand
    ax1=subplot(2,2,1);
    [x,y] = Utils.CutbyShort(Fox1E1(ii,:),Fox2E1(ii,:));
    scatter(x,y);axis equal;
    if(length(unique(x))>1 && length(unique(y))>1)
    [ fitted , Rs ] = Utils.removeOutliersData(x',y');
    x1 = x; y1=y;
    nanIndx = isnan(x1) | isnan(y1);
    y1(nanIndx) = [];
    x1(nanIndx) = [];
    [fit3,g3] = fit(x1',y1','poly1','Robust','on');
    hold on;
    plot(fitted,'r');
    plot(fit3,'m');
    hold off;
    legend('data',['fitF: R^2=',num2str(Rs)],['fitR: R^2=',num2str(g3.rsquare)]);
    end
    title(['oximeter1 vs oxsimeter2 same hand, R^2:', num2str(max(g3.rsquare,Rs))]);
    xlabel('oximeter1');
    ylabel('oximeter2');
    % oximeter iphone same hand
    ax2 = subplot(2,2,2);
    [x,y] = Utils.CutbyShort(Fox1E1(ii,:),FipE1(ii,:));
    scatter(x,y);axis equal;
    if(length(unique(x))>1 && length(unique(y))>1)
    [ fitted , Rs ] = Utils.removeOutliersData(x',y');
    x1 = x; y1=y;
    nanIndx = isnan(x1) | isnan(y1);
    y1(nanIndx) = [];
    x1(nanIndx) = [];
    [fit3,g3] = fit(x1',y1','poly1','Robust','on');
    hold on;
    plot(fitted,'r');
    plot(fit3,'m');
    hold off;
    legend('data',['fitF: R^2=',num2str(Rs)],['fitR: R^2=',num2str(g3.rsquare)]);
    end
    title(['oximeter1 vs iphone same hand, R^2:', num2str(max(g3.rsquare,Rs))]);
    xlabel('iphone');
    ylabel('oximeter1');
    %oximeter oximeter different hand
    ax3 = subplot(2,2,3);
    [x,y] = Utils.CutbyShort(Fox1E2(ii,:),Fox2E2(ii,:));
    scatter(x,y);axis equal;
    if(length(unique(x))>1 && length(unique(y))>1)
    [ fitted , Rs ] = Utils.removeOutliersData(x',y');
    x1 = x; y1=y;
    nanIndx = isnan(x1) | isnan(y1);
    y1(nanIndx) = [];
    x1(nanIndx) = [];
    [fit3,g3] = fit(x1',y1','poly1','Robust','on');
    hold on;
    plot(fitted,'r');
    plot(fit3,'m');
    hold off;
    legend('data',['fitF: R^2=',num2str(Rs)],['fitR: R^2=',num2str(g3.rsquare)]);
    end
    title(['oximeter1 vs oxsimeter2 different hand, R^2:', num2str(max(g3.rsquare,Rs))]);
    xlabel('oximeter1');
    ylabel('oximeter2');
    % oximeter iphone different hand
    ax4= subplot(2,2,4);
    [x,y] = Utils.CutbyShort(Fox1E2(ii,:),FipE2(ii,:));
    scatter(x,y);axis equal;
    if(length(unique(x))>1 && length(unique(y))>1)
    [ fitted , Rs ] = Utils.removeOutliersData(x',y');
    x1 = x; y1=y;
    nanIndx = isnan(x1) | isnan(y1);
    y1(nanIndx) = [];
    x1(nanIndx) = [];
    [fit3,g3] = fit(x1',y1','poly1','Robust','on');
    hold on;
    plot(fitted,'r');
    plot(fit3,'m');
    hold off;
    legend('data',['fitF: R^2=',num2str(Rs)],['fitR: R^2=',num2str(g3.rsquare)]);
    end
    title(['oximeter1 vs iphone different hand, R^2:', num2str(max(g3.rsquare,Rs))]);
    xlabel('oximeter1');
    ylabel('iphone');
    h=suptitle([Utils.Fieldname2Pretty(featureNames{ii}),' comparison']);
end

