clc;
% expNumSH = 1;
% expNumDH = 3;
% 
% %%
% eSH = Experiments.ReadExperiment(expNumSH);% same hand
% eDH = Experiments.ReadExperiment(expNumDH);% different hand
% 
% %%
% import Experiments.*
% [oxSigSH1, oxLocsSH1] = GetOxySignal(eSH, 1);
% [oxSigDHs, oxLocsDHs] = GetOxySignal(eDH, 1);
% [oxSigSH2, oxLocsSH2] = GetOxySignal(eSH, 2);
% [oxSigDHd, oxLocsDHd] = GetOxySignal(eDH, 2);
% [ ipSigSH, ~, ipLocsSH, ~, ~, ~, ~ ] = ...
% Video2PPG.Extract_Art1(eSH.iphoneVideo);
% [ ipSigDH, ~, ipLocsDH, ~, ~, ~, ~ ] = ...
% Video2PPG.Extract_Art1(eDH.iphoneVideo);

%%
% ws = 5;
% featuresIphoneSH = FeatureExtractor.FeatureExtractor(ipSigSH, ipLocsSH, ws, eSH.iphoneSR);
% featuresOximeterSH1 = FeatureExtractor.FeatureExtractor(oxSigSH1, oxLocsSH1, ws, eSH.oximeterSR);
% featuresOximeterSH2 = FeatureExtractor.FeatureExtractor(oxSigSH2, oxLocsSH2, ws, eSH.oximeterSR);
% featuresIphoneDH = FeatureExtractor.FeatureExtractor(ipSigDH, ipLocsDH, ws, eDH.iphoneSR);
% featuresOximeterDHs = FeatureExtractor.FeatureExtractor(oxSigDHs, oxLocsDHs, ws, eDH.oximeterSR);
% featuresOximeterDHd = FeatureExtractor.FeatureExtractor(oxSigDHd, oxLocsDHd, ws, eDH.oximeterSR);
% 
% %
% Printers.PrintScattersOfStructs(featuresIphone, featuresOximeter1, 'iphone', 'oximeter1');
% Printers.PrintScattersOfStructs(featuresIphone, featuresOximeter2, 'iphone', 'oximeter2');
% Printers.PrintScattersOfStructs(featuresOximeter1, featuresOximeter2, 'oximeter1', 'oximeter2');
%%
import Experiments.FeatureExpP.*
eSH = Experiments.ReadExperiment(1);% same hand
eDH = Experiments.ReadExperiment(3);% different hand
load('exp1.mat');
load('exp3.mat');
PrintFeatureExp( oxSig1E1,oxLocs1E1,oxSig2E1,oxLocs2E1,ipSigE1,ipLocsE1,oxSig1E3,oxLocs1E3,oxSig2E3,oxLocs2E3,ipSigE3,ipLocsE3,eSH.iphoneSR,eDH.iphoneSR,5);





