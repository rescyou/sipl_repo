function [ oxySignals, cycleLocations ] = GetCleanOximeterSignals( experimentStruct )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    oxySignals = cell(size(experimentStruct.oximeterSignals));
    cycleLocations = cell(size(experimentStruct.oximeterSignals));
    
    for i=1:length(experimentStruct.oximeterSignals)
        oxySignal = experimentStruct.oximeterSignals{i};
        oxySignal = oxySignal(:, 5);

        [ oxySignals{i}, xTime, cycleLocations{i}, signal_after_noise_cleaning, baseline ] = ...
            PPGAnalyzer.DenoiseAndRemoveBaseline( oxySignal, experimentStruct.oximeterSR );
    end

end

