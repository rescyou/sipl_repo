function [ signals, names, signalsTable ] = ReadOximeterSignals( filename )
%READOXIMETERSIGNALS Summary of this function goes here
%   Detailed explanation goes here

    signals = dlmread(filename, '\t', 6, 0);
    names = {'RED', 'RED_AMBIENT', 'IR', 'IR_AMBIENT', 'RED-RED_AMBIENT', 'IR-IR_AMBIENT'};
    
    if nargout >= 3
        signalsTable = array2table(signals, 'VariableNames', ...
            cellfun(@(x) {strrep(strrep(x,'_','__'),'-','_')}, names));
    end
end

