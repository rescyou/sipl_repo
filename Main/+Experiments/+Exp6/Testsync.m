final2= final/max(final);
[ ChRED2, baseline, lowPeaksLocs, outX ] = PPGAnalyzer.RemoveBaselineAlg(ChRED,false,false,500);
ChRED2=ChRED2/max(ChRED2);
startIndex = 10883 + 70;
startTime = startIndex/500;
plot((1:length(final))./vidObj.FrameRate + startTime,final2,(1:length(ChRED))./500,chred3);

%%
cycleLocsOximeter = lowPeaksLocs;
cycleLocsIphone = PPGAnalyzer.cycle_detect(final);
startIndexLoc = 24;

signalIphone = final2;
signalOximeter = chred3;

featuresIphone = FeatureExtractor.FeatureExtractor(signalIphone, cycleLocsIphone, 2, fr);

cycleLocsOximeterToUse = cycleLocsOximeter(startIndexLoc:startIndexLoc + length(cycleLocsIphone) - 1);
featuresOximeter = FeatureExtractor.FeatureExtractor(signalOximeter, cycleLocsOximeterToUse, 2, 500);



%%

Printers.PrintScattersOfStructs(featuresIphone, featuresOximeter, 'featuresIphone', 'featuresOximeter');


%%



clearvars featuresOximeter featuresIphone;
for i=1:length(cycleLocsIphone)-1
    if any(i==[5,13,24,28])
        continue;
    end
    try
    figure;    
    cycleOximeter = chred3(cycleLocsOximeter(startIndexLoc + i):cycleLocsOximeter(startIndexLoc + i + 1));
    cycleIphone = final2(cycleLocsIphone(i):cycleLocsIphone(i+1));
    plot((1:length(cycleIphone))./vidObj.FrameRate, cycleIphone, (1:length(cycleOximeter))./500, cycleOximeter);
    l=legend('video data','oximeter');
    l.FontSize=24;
    xlabel('time[s]','FontSize',24);
    title(num2str(i));
    featuresOximeter(i) = FeatureExtractor.TimeDomainFeaturesFromCycle(cycleOximeter, 500);
    featuresIphone(i) = FeatureExtractor.TimeDomainFeaturesFromCycle(cycleIphone, vidObj.FrameRate);
    catch
        disp(i)
    end
end
%%

fs=fieldnames(featuresIphone);
for i=1:length(fs)
    v1 = [featuresIphone.(fs{i})];
    v2 = [featuresOximeter.(fs{i})];
    figure
    scatter(v1,v2);
    title(['scatter plot of ',fs{i}],'FontSize',24);
    xlabel('video data values','FontSize',18);
    ylabel('oximeter data values','FontSize',18);
    %saveas(gcf,['img/sct',num2str(i),'.png']);
end


