function [ iphoneSignal, iphoneCycleLocations ] = GetCleanPpgSignal( experimentStruct )
%GETCLEANPPGSIGNAL Summary of this function goes here
%   Detailed explanation goes here

    s = Video2PPG.Extract_Art1(experimentStruct.iphoneVideo);
    iphoneSignal = s.final;
    iphoneCycleLocations = s.cyclesLocations;

end

