exps = [];

for i=1:4
    v = [];
    
    e = Experiments.ReadExperiment(i);
    v.path = e.path;
    v.oximeterSignalsFolders = e.oximeterSignalsFolders;
    
    [ oxySignals, cycleLocations ] = Experiments.GetCleanOximeterSignals(e);
    
    v.oxSig1 = oxySignals{1};
    v.oxLocs1 = cycleLocations{1};
    
    v.oxSig2 = oxySignals{2};
    v.oxLocs2 = cycleLocations{2};
    
    [v.ipSig, v.ipLocs] = Experiments.GetCleanPpgSignal(e);
    
    v.iphoneSR = e.iphoneSR;
    v.oximeterSR = e.oximeterSR;
    
    exps = [exps v];
end

save('experiments_1_4.mat','exps');
