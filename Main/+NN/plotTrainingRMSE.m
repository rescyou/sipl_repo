function plotTrainingRMSE(info)
%PLOTTRAININGACCURACY Summary of this function goes here
%   Detailed explanation goes here

    persistent plotObj
    %persistent plotObj2

    if info.State == "start"
        plotObj = animatedline;
        xlabel("Iteration")
        ylabel("Training Accuracy")
    elseif info.State == "iteration"
        addpoints(plotObj,info.Iteration,double(info.TrainingRMSE))
        drawnow limitrate nocallbacks
    end

end

