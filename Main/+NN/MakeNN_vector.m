function [ net ] = MakeNN_1( imageSize )
%MAKENN_1 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    imageSize = [3750 1 1];
end

net = [...
    imageInputLayer(imageSize, 'Name', 'Input Layer');
    
    convolution2dLayer([1250 1], 2^4, 'Stride', 1);
    reluLayer();
    maxPooling2dLayer([5 1], 'Stride', [4 1]);
    
    convolution2dLayer([250 1], 2^5, 'Stride', 1);
    reluLayer();
    maxPooling2dLayer([6 1], 'Stride', [5 1]);
    
    convolution2dLayer([75 1], 1, 'Stride', 1);
    reluLayer();
    
    %fullyConnectedLayer(1024, 'Name', 'fc1');
    %reluLayer();
    %dropoutLayer(0.5, 'Name', 'drop1');
    %reluLayer();
    %softmaxLayer();
    %classificationLayer()];
    regressionLayer()];
end

