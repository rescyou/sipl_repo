function plotAccuracy(info)
%PLOTACCURACYCLASSIFICATION Summary of this function goes here
%   Detailed explanation goes here

    persistent plotObjTraining
    persistent plotObjValidation

    if info.State == "start"
        plotObjTraining = animatedline('Color', 'k');
        
        if isfield(info, 'ValidationAccuracy')
            plotObjValidation = animatedline('Marker','o', 'Color', 'b');
            legend('Training', 'Validation');
        else
            plotObjValidation = [];
            legend('Training');
        end
        
        xlabel("Iteration")
        ylabel("Accuracy")
    elseif info.State == "iteration"
        addpoints(plotObjTraining, info.Iteration, info.TrainingAccuracy)
        if ~isempty(plotObjValidation) && ~isempty(info.ValidationAccuracy)
            addpoints(plotObjValidation, info.Iteration, info.ValidationAccuracy)
        end
        drawnow limitrate nocallbacks
    end

end

