function plotAccuracyAndLoss(info)
%PLOTACCURACYCLASSIFICATION Summary of this function goes here
%   Detailed explanation goes here

    persistent titleTxt

    if info.State == "start"
        ax = gca;
        titleTxt = ax.Title.String;
    end

    subplot(2,1,1);
    NN.plotters.plotAccuracy(info);
    
    
    subplot(2,1,2);
    NN.plotters.plotLoss(info);
    
    suptitle(titleTxt);

end

