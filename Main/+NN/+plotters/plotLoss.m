function plotLoss(info)
%PLOTACCURACYCLASSIFICATION Summary of this function goes here
%   Detailed explanation goes here

    persistent plotObjTraining
    persistent plotObjValidation

    if info.State == "start"
        plotObjTraining = animatedline('Color', 'k');
        
        if isfield(info, 'ValidationLoss')
            plotObjValidation = animatedline('Marker','o', 'Color', 'b');
            legend('Training', 'Validation');
        else
            plotObjValidation = [];
            legend('Training');
        end
        
        xlabel("Iteration")
        ylabel("Loss")
    elseif info.State == "iteration"
        addpoints(plotObjTraining, info.Iteration, double(info.TrainingLoss))
        if ~isempty(plotObjValidation) && ~isempty(info.ValidationLoss)
            addpoints(plotObjValidation, info.Iteration, double(info.ValidationLoss))
        end
        drawnow limitrate nocallbacks
    end

end

