function plotRMSE(info)
%PLOTACCURACYCLASSIFICATION Summary of this function goes here
%   Detailed explanation goes here

    persistent plotObjTraining
    persistent plotObjValidation

    if info.State == "start"
        plotObjTraining = animatedline('Color', 'k');
        
        if isfield(info, 'ValidationRMSE')
            plotObjValidation = animatedline('Marker','o', 'Color', 'b');
            legend('Training', 'Validation');
        else
            plotObjValidation = [];
            legend('Training');
        end
        
        xlabel("Iteration")
        ylabel("RMSE")
    elseif info.State == "iteration"
        if info.TrainingRMSE < 1e4
            addpoints(plotObjTraining, info.Iteration, double(info.TrainingRMSE))
            if ~isempty(plotObjValidation) && ~isempty(info.ValidationRMSE) && info.ValidationRMSE < 1e4
                addpoints(plotObjValidation, info.Iteration, double(info.ValidationRMSE))
            end
            drawnow limitrate nocallbacks
        end
    end

end

