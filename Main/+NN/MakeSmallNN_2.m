function [ net ] = MakeSmallNN_2( imageSize )
%MAKENN_1 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    imageSize = [36 980 1];
end

net = [...
    imageInputLayer(imageSize, 'Name', 'Input Layer');
    convolution2dLayer([30 90], 64,'Name', 'conv1');
    reluLayer();
%     maxPooling2dLayer([2 2], 'Stride', [2 3], 'Name', 'pool1');
%     convolution2dLayer([7 3], 64, 'Name', 'conv2');
%     reluLayer();
%     maxPooling2dLayer([2 2], 'Stride', [2 3], 'Name', 'pool2');
%     
% %     fullyConnectedLayer(1024, 'Name', 'fc1');
% %     reluLayer();
% %     dropoutLayer(0.5, 'Name', 'drop1');
    fullyConnectedLayer(1, 'Name', 'fc2');
    reluLayer();
%     dropoutLayer(0.5, 'Name', 'drop2');
    regressionLayer('Name','Regression Out Put Layer')];
end

