function [ net ] = MakeSmallNN_1_classification( imageSize, classes )
%MAKENN_1 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    imageSize = [36 98 1];
end

net = [...
    imageInputLayer(imageSize, 'Name', 'Input Layer');
    
    convolution2dLayer([6 5], 20,'Name', 'conv1');
    %reluLayer();
    maxPooling2dLayer([2 2], 'Stride', [2 2], 'Name', 'pool1');
    
    convolution2dLayer([4 4], 50, 'Name', 'conv2');
    %reluLayer();
    maxPooling2dLayer([2 2], 'Stride', [2 2], 'Name', 'pool2');
    
    convolution2dLayer([4 4], 500, 'Name', 'conv2');
    %reluLayer();
    
    convolution2dLayer([1 1], 10, 'Name', 'conv2');
    %reluLayer();
    
%     fullyConnectedLayer(1024, 'Name', 'fc1');
%     reluLayer();
%     dropoutLayer(0.5, 'Name', 'drop1');
    fullyConnectedLayer(classes, 'Name', 'fc2');
    reluLayer();
    dropoutLayer(0.5, 'Name', 'drop2');
    softmaxLayer();
    classificationLayer('Name','ClassificationOutputLayer')];
end

