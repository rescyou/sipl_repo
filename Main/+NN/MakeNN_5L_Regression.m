function [ net ] = MakeNN_5L_Regression( imageSize )
%MakeNN_5L Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    imageSize = [159 98 3];
end

net = [
    imageInputLayer(imageSize, 'Name', mfilename());
    
    convolution2dLayer([12 11], 64, 'Stride', 1);
    reluLayer();
    maxPooling2dLayer([2 2], 'Stride', 2);
    
    convolution2dLayer([9 7], 64*2, 'Stride', 1);
    reluLayer();
    maxPooling2dLayer([2 2], 'Stride', 2);
    
    convolution2dLayer([8 6], 64*4, 'Stride', 1);
    reluLayer();
    maxPooling2dLayer([2 2], 'Stride', 2);
    
    convolution2dLayer([6 4], 64*8, 'Stride', 1);
    reluLayer();
    maxPooling2dLayer([2 2], 'Stride', 2);
    
    convolution2dLayer([4 2], 1, 'Stride', 1);
    reluLayer();
    
    %fullyConnectedLayer(25, 'Name', 'fc1');
    %reluLayer();
    %dropoutLayer(0.5, 'Name', 'drop1');
    %reluLayer();
    %softmaxLayer();
    %classificationLayer()
    
    regressionLayer()
    
    ];
end

