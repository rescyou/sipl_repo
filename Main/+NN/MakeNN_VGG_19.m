function [ layers ] = MakeNN_VGG_19()
%MAKENN_VGG_19 Summary of this function goes here
%   Detailed explanation goes here

layers = importCaffeNetwork('VGG_ILSVRC_19_layers_deploy.prototxt','VGG_ILSVRC_19_layers.caffemodel');
layers = layers.Layers;

l = convolution2dLayer(layers(2).FilterSize, layers(2).NumFilters, 'Stride', layers(2).Stride, 'Padding', layers(2).Padding, 'NumChannels', 1, 'Name', 'convNew');
l.Weights = mean(layers(2).Weights, 3);
l.Bias = layers(2).Bias;
l.WeightLearnRateFactor = layers(2).WeightLearnRateFactor;
l.WeightL2Factor = layers(2).WeightL2Factor;
l.BiasLearnRateFactor = layers(2).BiasLearnRateFactor;
l.BiasL2Factor = layers(2).BiasL2Factor;
layers(2) = l;

s = 512*3;%*layers(39).OutputSize;
l = fullyConnectedLayer(layers(39).OutputSize, 'Name', 'fc2');
l.Weights = layers(39).Weights(1:layers(39).OutputSize, 1:s);
l.Bias = layers(39).Bias;
l.WeightLearnRateFactor = layers(39).WeightLearnRateFactor;
l.WeightL2Factor = layers(39).WeightL2Factor;
l.BiasLearnRateFactor = layers(39).BiasLearnRateFactor;
l.BiasL2Factor = layers(39).BiasL2Factor;
layers(39) = l;

layers(46) = fullyConnectedLayer(1, 'Name', 'fc1');
layers(47) = reluLayer();
layers(48) = regressionLayer('Name','Regression Out Put Layer');

end

