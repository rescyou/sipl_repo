getDiaryName = @(x) ['LOGS/Log__' strrep(x,' ','_') '__' datestr(now, 'dd-mm-yyyy_HH-MM-SS') '.txt'];

%% create network architecture based on alexNet from scretch
layers = NN.MakeNN;

%% make db
[X, Y] = NN.MakeDB(1e6, 'DB_NN_Full');
imageSize = size(X);
layers(1)= imageInputLayer(imageSize(1:3));

%%
noNansIdx = ~(isnan(Y(:,1)) | isnan(Y(:,2)));
Y1 = Y(noNansIdx, :);
X1 = X(:,:,:,noNansIdx);
%% training the network
options = trainingOptions('sgdm',...
    'CheckpointPath','NN_checkpoint');
%% g
diary(getDiaryName('NN'));
finalNet = trainNetwork(X1,Y1,layers,options) ;
diary off
