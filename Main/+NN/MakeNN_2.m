function [ net ] = MakeNN_2( imageSize )
%MAKENN_1 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    imageSize = [100 100 1];
end

net = [...
    imageInputLayer(imageSize, 'Name', 'Input Layer');
    convolution2dLayer([3 5], 64, 'Stride', 1,'Name', 'conv1');
    reluLayer();
    convolution2dLayer([3 5], 64, 'Stride', 1,'Name', 'conv2');
    reluLayer();
    convolution2dLayer([3 5], 64, 'Stride', 1,'Name', 'conv3');
    reluLayer();
    maxPooling2dLayer([2 2],'Stride', 2, 'Name', 'pool1');
    convolution2dLayer([1 3], 128, 'Stride',1, 'Padding', [0 1], 'Name', 'conv4');
    reluLayer();
    convolution2dLayer([1 3], 128, 'Stride',1, 'Padding', [0 1], 'Name', 'conv5');
    reluLayer();
    convolution2dLayer([1 3], 128, 'Stride',1, 'Padding', [0 1], 'Name', 'conv6');
    reluLayer();
    maxPooling2dLayer([2 3], 'Stride', 2, 'Name', 'pool2');
    convolution2dLayer([1 3], 256, 'Stride', 1, 'Padding', [0 1], 'Name', 'conv7');
    reluLayer();
    convolution2dLayer([1 3], 256, 'Stride', 1, 'Padding', [0 1], 'Name', 'conv8');
    reluLayer();
    convolution2dLayer([1 3], 256, 'Stride', 1, 'Padding', [0 1], 'Name', 'conv9');
    reluLayer();
    maxPooling2dLayer([1 2], 'Stride', 2, 'Name', 'pool3');
    fullyConnectedLayer(1024, 'Name', 'fc1');
    reluLayer();
    dropoutLayer(0.5, 'Name', 'drop1');
    fullyConnectedLayer(1, 'Name', 'fc2');
    reluLayer();
    %dropoutLayer(0.5, 'Name', 'drop2');
    regressionLayer('Name','Regression Out Put Layer')];
end

