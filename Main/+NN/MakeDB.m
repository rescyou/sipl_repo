function [ X, Y ] = MakeDB( numberOfInputsToUse, dbFolder )
%MAKEDB make input DB for trainNetwork
%   X - 4-D array of images
%   Y - systolic and diastolic output values
    
    if nargin < 1 || isempty(numberOfInputsToUse) || numberOfInputsToUse < 1
        numberOfInputsToUse = inf;
    end

    if nargin < 2 || isempty(dbFolder)
        dbFolder = 'DB_NN';
    end
    
    %% get mat file names
    files = dir(dbFolder);
    files = {files(3:end).name};
    files = files(1:min(numberOfInputsToUse, end));

    %% make X Y
    d = load([dbFolder '/' files{1}]);

    X = nan([size(d.stftImage) 1 length(files)]);
    Y = nan(length(files), 2);

    for i = 1:length(files)
        d = load([dbFolder '/' files{i}]);

        X(:, :, 1, i) = d.stftImage;
        Y(i, :) = [d.systolic, d.diastolic];
    end

end

