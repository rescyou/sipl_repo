function [ net ] = StftDeepNet( imageSize, numOfClasses )
%MAKENN_1 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    imageSize = [159 98 3];
end

params = [
16	11	1	1
1	1	1	1
10	7	1	1
1	1	1	1
8	7	1	1
2	2	2	2
5	3	1	1
2	2	2	2
5	3	1	1
2	2	2	2
4	3	1	1
2	2	2	2
5	3	1	1
];

net = imageInputLayer(imageSize, 'Name', mfilename());

for i = 1:2:size(params, 1) - 2
    net = [net;
        convolution2dLayer(params(i, 1:2), max(2^i, 2^4), 'Stride', params(i, 3:4));
        ];
    if i >= 3
        net = [net; dropoutLayer(0.5)];
    end
    
    net = [net;
        reluLayer();
        maxPooling2dLayer(params(i+1, 1:2), 'Stride', params(i+1, 3:4))
        ];
        
end

net = [net; 
    convolution2dLayer(params(end, 1:2), numOfClasses, 'Stride', params(end, 3:4));
    
    %fullyConnectedLayer(25);
    %reluLayer();
    %dropoutLayer(0.5);
    %reluLayer();
    softmaxLayer();
    classificationLayer()
    %regressionLayer()
    ];
    
end

