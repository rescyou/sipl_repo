function plotTrainingAccuracy(info)
%PLOTTRAININGACCURACY Summary of this function goes here
%   Detailed explanation goes here

    persistent plotObj

    if info.State == "start"
        plotObj = animatedline;
        xlabel("Iteration")
        ylabel("Training Accuracy")
    elseif info.State == "iteration"
        addpoints(plotObj,info.Iteration,info.TrainingAccuracy)
        drawnow limitrate nocallbacks
    end

end

