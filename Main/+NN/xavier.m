function layersNew = xavier(layers)
%XAVIER  Xavier filter initialization.
%   WEIGHTS = XAVIER(H, W, C, N) initializes N filters of support H x
%   W and C channels using Xavier method. WEIGHTS = {FILTERS,BIASES}is
%   a cell array containing both filters and biases.
%
% See also:
% Glorot, Xavier, and Yoshua Bengio.
% "Understanding the difficulty of training deep feedforward neural networks."
% International conference on artificial intelligence and statistics. 2010.

layersNew = [];
for layer = layers
    
    if isa(layer, 'nnet.cnn.layer.Convolution2DLayer')
        n = prod(layer.FilterSize) * layer.NumFilters;
        scale = sqrt(2 / n);
        fs = [1 1 1 layer.NumFilters];
        fs(1:length(layer.FilterSize)) = layer.FilterSize;
        layer.Weights = randn(fs, 'single') * scale;
        layer.Bias = zeros(1, 1, layer.NumFilters, 'single');
    elseif isa(layer, 'nnet.cnn.layer.Convolution2DLayer')
        n = layer.OutputSize;
        scale = sqrt(2 / prod(n));
        layer.Weights = randn(n, 'single') * scale;
        layer.Bias = zeros(n, 1, 'single');
    end
    
    layersNew = [layersNew layer];
end
