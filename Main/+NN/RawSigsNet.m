function [ net ] = RawSigsNet( imageSize, numOfClasses )
%MAKENN_1 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    imageSize = [3750 1 1];
end

params = [
1200	1	1	1
1	1	1	1
500	1	1	1
2	1	2	1
200	1	1	1
2	1	1	1
200	1	1	1
3	1	2	1
180	1	1	1
2	1	2	1
67	1	2	1
];

net = imageInputLayer(imageSize, 'Name', mfilename());

for i = 1:2:size(params, 1) - 2
    net = [net;
        convolution2dLayer(params(i, 1:2), max(2^i, 2^4), 'Stride', params(i, 3:4));
        reluLayer();
        maxPooling2dLayer(params(i+1, 1:2), 'Stride', params(i+1, 3:4))
        ];
end

net = [net; 
    convolution2dLayer(params(end, 1:2), numOfClasses, 'Stride', params(end, 3:4));
    
    %fullyConnectedLayer(25);
    %reluLayer();
    %dropoutLayer(0.5);
    %reluLayer();
    softmaxLayer();
    classificationLayer()
    %regressionLayer()
    ];
    
end

