function [ finalNet ] = NN_Run_Aux( inputCellTrain, inputCellValidation, ...
    numOfTrainExamples, numOfValidationExamples, ...
    predictFnc, ...
    exampleSize, layers, trainingOpt, testNameOrig, functionsToAdd, plotConfusionMatrixFunc )

    if nargin < 11
        plotConfusionMatrixFunc = [];
    end

    [testNameCompact, testName, logPath] = PARAMS.startTest(testNameOrig);
    
    checkPointFolder = [testName '\NN_Checkpoint'];
    Utils.MakeFolder(checkPointFolder);
    
    figurePath = [testName '\accuracy.fig'];
    figurePNGPath = [testName '\accuracy.png'];
    
    figureConfPath_train = [testName '\confusionmat_train.fig'];
    figureConfPNGPath_train = [testName '\confusionmat_train.png'];
    figureConfPath_validation = [testName '\confusionmat_validation.fig'];
    figureConfPNGPath_validation = [testName '\confusionmat_validation.png'];
    
    moreDataPath = [testName '\nnInfo.mat'];
    
    %% init figure
    %figure;
    a=gca;
    testNameOrig=a.Title.String{1};
    title({strrep(strrep(testNameOrig,'__',' | '), '_', ' '); 
        sprintf('InitialLearnRate %g | Momentum %g | MiniBatchSize %g | L2Regularization %g | MaxEpochs %g', ...
            trainingOpt.InitialLearnRate, trainingOpt.Momentum, trainingOpt.MiniBatchSize, trainingOpt.L2Regularization, 50)});
    set(gcf, 'units', 'normalized', 'outerposition', [0.25 0.2 0.5 0.6]);
    
    %% get callers list
    d = dbstack();
    callersList = struct2table(d)
    
    %% make trainingOpt    
    moreParams = {};
    
    if contains(version, 'R2017b')
        moreParams = [moreParams {'Shuffle', 'every-epoch'}];
        
        if ~isempty(inputCellValidation)
            valSet = inputCellValidation;
            if length(inputCellValidation) == 1
                valSet = inputCellValidation{1};
            end

            moreParams = [moreParams {'ValidationData', valSet}];
        end
    end
    
    trainingOpt = Utils.CopyTrainingOptions(trainingOpt, ...
        moreParams{:}, ...
        'CheckpointPath', checkPointFolder, ...
        'OutputFcn', functionsToAdd);
    
    %% print inputs
    testNameCompact
    numOfTrainExamples
    numOfValidationExamples
    exampleSize
    trainingOpt
    layers
    
    %% train
    finalNet = [];
    accuracy_train = -1;
    accuracy_validation = -1;
    err = [];
    nnInfo = [];
    try
        finalNet = trainNetwork(inputCellTrain{:}, layers, trainingOpt);
        
        %% predict preformance
        [accuracy_train, accuracy_validation, nnInfo] = ...
            predictFnc(finalNet, inputCellTrain, inputCellValidation);
    catch e
        disp(getReport(e));
        err = e;
    end
    
    %% finish test
    % savefig
    savefig(figurePath);
    saveas(gcf, figurePNGPath);
    
    % confMat
    moreFiles = {};
    if ~isempty(plotConfusionMatrixFunc) && ~isempty(finalNet)
        [f_train, f_validation] = plotConfusionMatrixFunc(finalNet, inputCellTrain, inputCellValidation, nnInfo);
        figure(f_train);
        savefig(figureConfPath_train);
        saveas(gcf, figureConfPNGPath_train);
        moreFiles = [moreFiles, {figureConfPath_train, figureConfPNGPath_train}];
        
        figure(f_validation);
        savefig(figureConfPath_validation);
        saveas(gcf, figureConfPNGPath_validation);
        moreFiles = [moreFiles, {figureConfPath_validation, figureConfPNGPath_validation}];
    end
    
    % prepare mail
    mailTitle = ['Test Results - ' testNameCompact];
    if ~isempty(err)
        mailTitle = ['ERROR ' mailTitle];
        if contains(err.message, 'CUDA_ERROR_OUT_OF_MEMORY')
            try
                disp('trying to reset GPU device...')
                reset(gpuDevice());
            catch ee
                disp('failed to reset GPU device')
                disp(getReport(ee));
            end
        end
    end
    
    spliter = '============================';
    mailBody = {
        testName;
        ' '; spliter; ' ';
        'Callers list:';
        evalc('disp(callersList)')
        ' '; spliter; ' ';
        ['Matlab version: ' version()];
        ' '; spliter; ' ';
        ['Train set size: ' num2str(numOfTrainExamples)];
        ['Validation set size: ' num2str(numOfValidationExamples)];
        ['Input size: ' num2str(exampleSize)];
        ' '; spliter; ' ';
        ['Net used (' layers(1).Name '; ' num2str(length(layers)) ' layers):'];
        evalc('disp(layers)'); 
        ' '; spliter; ' ';
        'Training options used:';  
        evalc('disp(trainingOpt)'); 
        ' '; spliter; ' ';
        ['Accuracy of training: ' num2str(accuracy_train)];
        ['Accuracy of validation: ' num2str(accuracy_validation)];
        };
    
    if ~isempty(err)
        mailBody = [{'ERROR'; getReport(err); ' '; spliter; ' '}; mailBody];
    end
    
    % nnInfo
    nnInfo.testNameCompact = testNameCompact;
    nnInfo.numOfTrainExamples = numOfTrainExamples;
    nnInfo.numOfValidationExamples = numOfValidationExamples;
    nnInfo.exampleSize = exampleSize;
    nnInfo.mailBody = mailBody;
    save(moreDataPath, 'nnInfo');
    
    % disp mail
    disp('====== MAIL BODY ======');
    fprintf('%s\n', strjoin(mailBody,'\n'));
    diary off
    
    % send mail
    disp('sending mail...');
    Utils.SendMail(mailTitle, ...
        mailBody, ...
        [{figurePath, figurePNGPath, moreDataPath, logPath}, moreFiles]);
    disp('mail sent');
    

end