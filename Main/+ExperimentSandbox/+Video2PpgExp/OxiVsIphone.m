clc;
close all
exp1 = Experiments.ReadExperiment(1);

ox = ExperimentSandbox.Video2PpgExp.GetOxFromExp(exp1);
ips = [];
err = [];

load('+ExperimentSandbox/+Video2PpgExp/all_articles_ppgs_exp1_try2.mat')
s = struct2array(s);
for i = 1:length(s)
    ip = s(i);
    [e, ox_fix, ip] = ExperimentSandbox.Video2PpgExp.GetErrors(ox, ip, [], [], 1);
    e
    err = [err e];
    ips = [ips ip];
    disp('----');
end

%% show res
clc
for e = err
    if ~isempty(strfind(e.fullName, 'inv 1'))
        continue;
    end
    e
    e.cycles_fit_coeff_mse
    disp('----');
end

%% err to table
headers = arrayfun(@(e) {[num2str(e.artId) e.channel]}, err);
err_c = [];
for e = err
    ee = [];
    %ee.mse_total = e.mse_total;
    %ee.xcorr_total = e.xcorr_total;
    ee.mse1 = e.mse1;
    ee.mse2 = e.mse2;
    ee.xcorr1 = e.xcorr1;
    ee.xcorr2 = e.xcorr2;
    ee.cycles_fit_coeff_mse = e.cycles_fit_coeff_mse_total;
    err_c = [err_c ee];
end
t = struct2table(err_c);
headers = {'A3.1', 'A3.2', 'A3.1_Flip', 'A3.2_Flip', 'A1', 'A1_Flip', 'A2', 'A2_Flip'};
t.Properties.RowNames = headers;
t=t([5, 6,7,8,1,3,2,4],:)

YourArray = table2array(t)'
% YourNewTable = array2table(YourArray.');
% YourNewTable.Properties.RowNames = t.Properties.VariableNames;
% YourNewTable.Properties.VariableNames = t.Properties.RowNames;
% YourNewTable

%% plot1
figure;
leg = [];
hold on;
arr = [{ox_fix}, arrayfun(@(x){x},ips)];
for i = 1:length(arr)
    q = arr{i};
    if q.inverse
        continue
    end
    plot(q.x_cut, q.y_cut);
    leg = [leg {[q.name ' inv ' num2str(q.inverse)]}];
end
hold off;
legend(leg);

%% plot2
I = [3 4 1 2];
ips_toplot = ips([ips.artId]>0);

headers = {'A3.1', 'A3.2', 'A1', 'A2'};
headers = headers(I);

XNAME = 'Delta Time [sec]';
axs = [];

figure;
subplot(3,2,1:2);
plot(ox_fix.x_cut, ox_fix.y_cut);
axs = [axs gca];
xlabel(XNAME);
title('Oximeter');

j = 3;
for q = ips_toplot(I)
    subplot(3,2,j);    
    plot(q.x_cut, q.y_cut);
    xlabel(XNAME);
    axs = [axs gca];
    title(headers{j-2});
    j = j + 1;
end
linkaxes(axs, 'x');
hold off;

%%
%q = Video2PPG.CleanAndMakeStruct(q, [], [], [0.5 7]);


