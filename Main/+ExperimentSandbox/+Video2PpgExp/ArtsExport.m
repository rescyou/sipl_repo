s = [];
%%
exp1 = Experiments.ReadExperiment(1);
[ s.art18_tec1, s.art18_tec2, s.art18_tec1_inverse, s.art18_tec2_inverse ] = Video2PPG.Extract_Art18(exp1.iphoneVideo);

%%
exp1 = Experiments.ReadExperiment(1);
[s.art1, s.art1_inverse] = Video2PPG.Extract_Art1(exp1.iphoneVideo);

%%
exp1 = Experiments.ReadExperiment(1);
[s.art15, s.art15_inverse] = Video2PPG.Extract_Art15(exp1.iphoneVideo);

%%
save('+ExperimentSandbox/+Video2PpgExp/all_articles_ppgs_exp1_try2', 's');
