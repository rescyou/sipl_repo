function [ err, ox, ip ] = GetErrors( ox_info, ip_info, windowTime, startingCycleIdx, plotMe )
%GETERRORS Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 3 || isempty(windowTime)
        windowTime = 10;
    end
    if nargin < 4 || isempty(startingCycleIdx)
        startingCycleIdx = 1;
    end
    if nargin < 5
        plotMe = nargout == 0;
    end
    
    ox = modifyStruct(ox_info);
    ip = modifyStruct(ip_info);
    
    err.name = ip.name;
    err.fullName = [ip.name ' inv ' num2str(ip.inverse)];
    err.artId = ip.artId;
    err.channel = ip.channel;

    disp(err.fullName);
    
    %% if plotMe
    if plotMe
        myTitle = @(s) title(s, 'FontSize', 22);
        myXLabel = @(s) xlabel(s, 'FontSize', 18);
        myTimeXLabel = @() myXLabel('Time [sec]');
        myDeltaTimeXLabel = @() myXLabel('\Delta Time [sec]');
        mySuptitle = @(s) set(suptitle(s), 'FontSize', 24, 'FontWeight', 'bold');
        fixPlot = @(h) set(h, 'LineWidth', 2, 'MarkerSize', 10);
        myText = @(x, y, s) text(x, y, ['   ' s], 'FontSize', 16);
    end
    
    %% fix and interpolate
    ip_y_int = interp1(ip.x_cut, ip.y_cut, ox.x_cut);
    ip_y_int(isnan(ip_y_int)) = [];
    [ox.y_fix, ip.y_fix] = Utils.CutbyShort(ox.y_cut, ip_y_int);
    SR = ox.samplingRate;
    X = (0:length(ox.y_fix)-1) ./ SR;
    
    ox.name = 'oximeter';
    ox.inverse = false;
    
    %% total mse and xcorr
    err.mse_total = MSE(ox.y_fix, ip.y_fix);
    err.xcorr_total = max(xcorr(ox.y_fix, ip.y_fix));
    
    %% norm funcs
    norm1 = @(x) x ./ (sum(x) / SR);
    norm2 = @(x) x ./ max(x);
    
    %% Go over windows
    err.mse1 = 0;
    err.mse2 = 0;
    err.xcorr1 = 0;
    err.xcorr2 = 0;
    
    windowSize = round(windowTime * SR);
    windowIdxs = 1:windowSize:length(ox.y_fix);
    N = length(windowIdxs)-1;
    for widx = 1:N
        wi = windowIdxs(widx);
        wni = windowIdxs(widx+1);
        r = wi:wni;
        
        oxw = ox.y_fix(r);
        ipw = ip.y_fix(r);
        
        %% mse
        err.mse1 = err.mse1 + MSE(norm1(oxw), norm1(ipw));
        err.mse2 = err.mse2 + MSE(norm2(oxw), norm2(ipw));
        
        %% xcorr
        err.xcorr1 = err.xcorr1 + max(xcorr(norm1(oxw), norm1(ipw)));
        err.xcorr2 = err.xcorr2 + max(xcorr(norm2(oxw), norm2(ipw)));
        
        if plotMe && widx == min(5, N)
            figure;
            leg = {'Oximeter', 'iPhone'};
            axsMse = [];
            norms = {norm1, norm2};
            
            for j = 1:2
                nn = norms{j};
                i = j*2 - 1;
                
                subplot(2, 2, i);
                fixPlot(plot(X(r), nn(oxw), X(r), nn(ipw)));
                myTimeXLabel();
                legend(leg);
                myTitle(sprintf('mse%g = %g', j, MSE(nn(oxw), nn(ipw))));
                axsMse = [axsMse gca];
                
                subplot(2, 2, i + 1);
                [xcorr1, lags1] = xcorr(nn(oxw), nn(ipw));
                lags1 = lags1 ./ SR;
                fixPlot(plot(lags1, xcorr1));
                hold on;
                [mxcorr1, ixcorr1] = max(xcorr1);
                fixPlot(plot(lags1(ixcorr1), mxcorr1, 'o'));
                myText(lags1(ixcorr1), mxcorr1, sprintf('(%g, %g)', lags1(ixcorr1), mxcorr1));
                myTitle(sprintf('xcorr%g = %g', j, max(xcorr1)));
                myDeltaTimeXLabel();
            end
            
            linkaxes(axsMse, 'x');
            mySuptitle(err.fullName);
        end
        
    end
    
    err.mse1 = err.mse1 ./ N;
    err.mse2 = err.mse2 ./ N;
    err.xcorr1 = err.xcorr1 ./ N;
    err.xcorr2 = err.xcorr2 ./ N;
    
    %% cycle fit
    err.cycles_fit_coeff_iphone = 0;
    err.cycles_fit_coeff_oximeter = 0;
    err.cycles_fit_coeff_mse = 0;
    err.cycles_fit_coeff_mse_total = 0;
    err.cycles_fit_coeff_mse_total_avg = 0;
    
    N = min(length(ox.cl), length(ip.cl)) - 1;
    
    for i = 1:N
        ip_c_range = ip.cl(i):ip.cl(i+1);
        ox_c_range = ox.cl(i):ox.cl(i+1);
        
        ipc = ip.y(ip_c_range);
        oxc = ox.y(ox_c_range);
        
        draw = 0;
        [ip_fitresult, ip_gof, ip_coeffs] = ...
            PPGAnalyzer.CreateCycleFit(ipc, ip.samplingRate, draw);
        [ox_fitresult, ox_gof, ox_coeffs] = ...
            PPGAnalyzer.CreateCycleFit(oxc, ox.samplingRate, draw);
        
        ip_coeffs = ip_coeffs(2:end,:); % b, c
        ox_coeffs = ox_coeffs(2:end,:); % b, c
        
        err.cycles_fit_coeff_iphone = err.cycles_fit_coeff_iphone + ip_coeffs;
        err.cycles_fit_coeff_oximeter = err.cycles_fit_coeff_oximeter + ox_coeffs;
        err.cycles_fit_coeff_mse = err.cycles_fit_coeff_mse + (ip_coeffs - ox_coeffs).^2;
        err.cycles_fit_coeff_mse_total_avg = err.cycles_fit_coeff_mse_total_avg + (ip_coeffs - ox_coeffs).^2 ./ 6;
        
        if plotMe && i == min(49, N)
            figure;
            
            leg = {'Oximeter', 'iPhone'};
            
            draw = 1;
            subplot(2,1,1);
            PPGAnalyzer.CreateCycleFit(oxc, ox.samplingRate, draw);
            myTitle(leg{1});
            
            subplot(2,1,2);
            PPGAnalyzer.CreateCycleFit(ipc, ip.samplingRate, draw);
            myTitle(leg{2});
            
            mySuptitle({err.fullName;['coeff\_mse = ' num2str(MSE(ip_coeffs, ox_coeffs))]});
        end
    end
    
    err.cycles_fit_coeff_iphone = err.cycles_fit_coeff_iphone ./ N;
    err.cycles_fit_coeff_oximeter = err.cycles_fit_coeff_oximeter ./ N;
    err.cycles_fit_coeff_mse = err.cycles_fit_coeff_mse ./ N;
    err.cycles_fit_coeff_mse_total = MSE(err.cycles_fit_coeff_iphone, err.cycles_fit_coeff_oximeter);
    err.cycles_fit_coeff_mse_total_avg = err.cycles_fit_coeff_mse_total_avg ./ N;

    %% aux functions
    function in = modifyStruct(in)
        norm = @(x) x ./ (sum(x) / in.samplingRate);

        in.y = norm(in.final);
        in.x = in.xTime;

        in.range = in.cyclesLocations(startingCycleIdx):in.cyclesLocations(end);
        in.y_cut = in.y(in.range);
        in.x_cut = in.x(in.range) - in.x(in.range(1));
        
        in.cl = in.cyclesLocations;
    end
end

function m = MSE(x, y)
    m = sum((y(:)-x(:)) .^ 2) ./ numel(x);
end

