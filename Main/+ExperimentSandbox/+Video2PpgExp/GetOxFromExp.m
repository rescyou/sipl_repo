function [ ox_info ] = GetOxFromExp( exp, i )
%GETOXFROMEXP Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 2
        i = 1;
    end

    [ oxySignals, cycleLocations ] = Experiments.GetCleanOximeterSignals( exp );
    ox_info.final = oxySignals{i};
    ox_info.cyclesLocations = cycleLocations{i};
    ox_info.xTime = (0:length(ox_info.final)-1) ./ exp.oximeterSR;
    ox_info.samplingRate = exp.oximeterSR;

end

