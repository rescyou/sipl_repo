
exp1 = Experiments.ReadExperiment(1);

channel = 'Y';
[ ppg_tec1, ppg_tec2 ] = Video2PPG.Extract_Art18( exp1.iphoneVideo, channel );

%%
close all
ppgs = {ppg_tec1, ppg_tec2};
axs = [];

for i=1:2
    ppg = ppgs{i};
    
    figure;
    subplot(3,1,1);
    plot(ppg.xTime, ppg.raw);
    title('Raw');
    xlabel('Time [sec]');
    axs = [axs gca];

    subplot(3,1,2);
    plot(ppg.xTime, ppg.signal_after_noise_cleaning);
    title('BPF');
    xlabel('Time [sec]');
    axs = [axs gca];

    subplot(3,1,3);
    %plot(ppg.xTime, ppg.final);
    Printers.DrawVerticalLinesOnGraph(ppg.final,ppg.cyclesLocations,exp1.iphoneSR);
    title('final');
    xlabel('Time [sec]');
    axs = [axs gca];
end

linkaxes(axs, 'x');