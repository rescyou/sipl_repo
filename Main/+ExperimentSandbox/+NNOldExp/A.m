getDiaryName = @(x) ['LOGS/Log__' strrep(x,' ','_') '__' datestr(now, 'dd-mm-yyyy_HH-MM-SS') '.txt'];

%%
% diary(getDiaryName('Rebuild DB from images'));
% DatabaseContractor.MakeDatabaseStructFromImages('../DB_NN_FULL');
% diary off

%%
% layers = NN.xavier(layers);

outputFolder = 'NN_checkpoint_NEW_sys_Classes';

if ~exist(outputFolder,'dir')
    disp(['Making folder: ' outputFolder]);
    mkdir(outputFolder);
end

load('FFF2.mat');

%%
% data = databaseStruct.modifyPathsFnc(databaseStruct);
% data = data(:,1:2);
databaseStruct.bigDataTbl = databaseStruct.bigDataTbl(randsample(size(databaseStruct.bigDataTbl, 1),2000),:);
data = databaseStruct.getTblFunc_SysOnly(databaseStruct);
%data = table( data{:,1}, categorical(data{:,2} > mean(data{:,2})));
%data = data(1:100000,:);
%data = databaseStruct.modifyTblFnc(databaseStruct);

data.systolic = categorical(discretize(data{:,2}, 10));

imageSize = databaseStruct.imageSize;

layers = NN.MakeSmallNN_1_classification(imageSize, length(unique(data.systolic)));
%layers(1) = imageInputLayer(imageSize);
%%
diary(getDiaryName('NN Training Sys'));
%options = trainingOptions('sgdm','MaxEpochs',20, 'CheckpointPath','NN_checkpoint');
%options = trainingOptions('sgdm', 'CheckpointPath',outputFolder);
options = trainingOptions('sgdm', 'MaxEpochs',500,'L2Regularization',0,'LearnRateSchedule','piecewise','LearnRateDropFactor',0.9,'InitialLearnRate',0.1);
finalNet = trainNetwork(data, data.Properties.VariableNames{2}, layers, options);
% layers(end) = softmaxLayer();
% layers(end+1) = classificationLayer();
% layers(1) = imageInputLayer([36 98 3]);
% imds = imageDatastore(fullfile(rootFolder, categories), 'LabelSource', 'foldernames');
% imds.Files = imds.Files(1:20);
% imds.ReadFcn = @(filename)readAndPreprocessImage(filename);
% finalNet = trainNetwork(imds2, layers, options);

diary off



%%
% Note that other CNN models will have different input size constraints,
% and may require other pre-processing steps.
    function Iout = readAndPreprocessImage(filename)
                
        I = imread(filename);
        
        % Some images may be grayscale. Replicate the image 3 times to
        % create an RGB image. 
        if ismatrix(I)
            I = cat(3,I,I,I);
        end
        
        % Resize the image as required for the CNN. 
        Iout = imresize(I, [36 98]);  
        
        % Note that the aspect ratio is not preserved. In Caltech 101, the
        % object of interest is centered in the image and occupies a
        % majority of the image scene. Therefore, preserving the aspect
        % ratio is not critical. However, for other data sets, it may prove
        % beneficial to preserve the aspect ratio of the original image
        % when resizing.
    end

