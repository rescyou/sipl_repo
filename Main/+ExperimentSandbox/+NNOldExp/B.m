getDiaryName = @(x) ['LOGS/Log__' strrep(x,' ','_') '__' datestr(now, 'dd-mm-yyyy_HH-MM-SS') '.txt'];

%%
% layers = NN.xavier(layers);

outputFolder = 'NN_checkpoint_B_sys_Classes';

if ~exist(outputFolder,'dir')
    disp(['Making folder: ' outputFolder]);
    mkdir(outputFolder);
end

%%
data = resultsMatrix(:,1);
data(isnan(data))=0;

%systolic = categorical(discretize(data, 10));

dbSize = size(ppgMatrix);

%% NN
layers = NN.MakeNN_vector();

layers(1) = imageInputLayer(dbSize(1:3));
%%
diary(getDiaryName('B NN Training Sys'));
%options = trainingOptions('sgdm','MaxEpochs',20, 'CheckpointPath','NN_checkpoint');
%options = trainingOptions('sgdm', 'CheckpointPath',outputFolder);
options = trainingOptions('sgdm', 'MaxEpochs',500,'L2Regularization',0,'LearnRateSchedule','piecewise','LearnRateDropFactor',0.9,'InitialLearnRate',0.01);
finalNet = trainNetwork(ppgMatrix, data, layers, options);
% layers(end) = softmaxLayer();
% layers(end+1) = classificationLayer();
% layers(1) = imageInputLayer([36 98 3]);
% imds = imageDatastore(fullfile(rootFolder, categories), 'LabelSource', 'foldernames');
% imds.Files = imds.Files(1:20);
% imds.ReadFcn = @(filename)readAndPreprocessImage(filename);
% finalNet = trainNetwork(imds2, layers, options);

diary off
