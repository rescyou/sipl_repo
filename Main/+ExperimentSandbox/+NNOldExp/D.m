%%
clc
getDiaryName = @(x) ['LOGS/Log__' strrep(x,' ','_') '__' datestr(now, 'dd-mm-yyyy_HH-MM-SS') '.txt'];
diary(getDiaryName('D NN Training Sys 1e4 jump classification'));

%% load DB if needed - only systolic
if ~exist('dataCreated','var')
    %numberOfBuckets = 50;
    %typeIdx = 1;
    %[ X, Y_regression, Y_classification ] = NewDatabase.GetXY([], typeIdx, numberOfBuckets);
    clear all;
    load('+NewDatabase/nnData_Sys.mat');
    
    dataCreated = true;
end

%% params setup
N = 1e5
numberOfBuckets = 25

goodIdx = Y_regression >= 70 & Y_regression <= 190;
goodIdx = goodIdx(randperm(length(goodIdx), N));

XX = X - mean(X,4);
XX = XX(:, :, :, goodIdx);

%%
Y = Y_regression(goodIdx);

[p, c] = lloyds(Y, numberOfBuckets);
[i, Y_classification] = quantiz(Y, p, c);
Y_classification = categorical(Y_classification)';

data = Y_classification;
dbSize = size(XX)

%% checkpoint output folder
% outputFolder = 'NN_checkpoint_C_sys_classification';
% 
% if ~exist(outputFolder,'dir')
%     disp(['Making folder: ' outputFolder]);
%     mkdir(outputFolder);
% end

%% NN
layers = NN.MakeNN_vector_classification(dbSize(1:3), numel(unique(Y_classification)));
% layers = NN.xavier(layers);

%layers(1) = imageInputLayer();
%%
N = 1e5;

%%
%myTitle = sprintf('

%options = trainingOptions('sgdm','MaxEpochs',20, 'CheckpointPath','NN_checkpoint');
%options = trainingOptions('sgdm', 'CheckpointPath',outputFolder);
options = trainingOptions('sgdm', ...
    'MaxEpochs', 500, ...
    'MiniBatchSize', 128, ...
    'Momentum', 0.9, ...
    'L2Regularization', 0, ...
    ...%'LearnRateSchedule', 'piecewise', ...
    'LearnRateDropFactor', 0.9, ...
    'InitialLearnRate', 0.01)
finalNet = trainNetwork(XX, data, layers, options);
% layers(end) = softmaxLayer();
% layers(end+1) = classificationLayer();
% layers(1) = imageInputLayer([36 98 3]);
% imds = imageDatastore(fullfile(rootFolder, categories), 'LabelSource', 'foldernames');
% imds.Files = imds.Files(1:20);
% imds.ReadFcn = @(filename)readAndPreprocessImage(filename);
% finalNet = trainNetwork(imds2, layers, options);

diary off

