clc;

folder = 153;
secondsToRead = 100;
plotMe = 0;

[ PPG, NBP ] = MimicDbToolbox.GetPlethAndBP(folder, secondsToRead, plotMe);

%%
close all;
figure;
MimicDbToolbox.PlotSignals(PPG, NBP, 0);
figure;
MimicDbToolbox.PlotSignals(PPG, NBP, 1);
figure;
MimicDbToolbox.PlotSignals(PPG, NBP, 2);
