function [ ] = PrintFeatureExp( varargin )
%UNTITLED2 Summary of this function goes here
%order of input: oxS1E1,oxL1E1,oxS2E1,oxL2E1,ipSE1,ipLE1,oxS1E2,oxL1E2,oxS2E2,oxL2E2,ipSE2,ipLE2,ipFE1,ipFE2,ws
Snames = {'Fox1E1','Fox2E1','FipE1','Fox1E2','Fox2E2','FipE2'};
for kk=1:6
    if mod(kk,3)==0
       SR = varargin{12+kk/3};
    else
       SR = 500;
    end
        eval(['[~,',Snames{kk},', featureNames ] =FeatureExtractor.FeatureExtractor( varargin{2*(kk)-1}, varargin{2*kk},varargin{15}, SR);']);
end
for ii=1:length(featureNames)
    figure('units','normalized','outerposition',[0 0 1 1]);
    h=suptitle([Utils.Fieldname2Pretty(featureNames{ii}),' comparison']);
    h.FontSize = 22;
    subplot(2,2,1)
    [x,y] = Utils.CutbyShort(Fox1E1(ii,:),Fox2E1(ii,:));
    scatter(x,y);axis tight;
    title('oximeter1 vs oxsimeter2 same hand')
    xlabel('oximeter1');
    ylabel('oximeter2');
    subplot(2,2,2)
    [x,y] = Utils.CutbyShort(Fox1E1(ii,:),FipE1(ii,:));
    scatter(x,y);axis tight;
    title('oximeter1 vs iphone same hand')
    xlabel('iphone');
    ylabel('oximeter1');
    subplot(2,2,3)
    [x,y] = Utils.CutbyShort(Fox1E2(ii,:),Fox2E2(ii,:));
    scatter(x,y);axis tight;
    title('oximeter1 vs oxsimeter2 different hand')
    xlabel('oximeter1');
    ylabel('oximeter2');
    subplot(2,2,4)
    [x,y] = Utils.CutbyShort(Fox1E2(ii,:),FipE2(ii,:));
    scatter(x,y);axis tight;
    title('oximeter1 vs iphone different hand')
    xlabel('oximeter1');
    ylabel('iphone');
end

