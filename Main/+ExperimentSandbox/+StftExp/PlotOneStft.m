function [ axTop, axBottom, p ] = PlotOneStft( signal, sr, locs, windowSizeInCycles, i, overlapPercentage, titleTxt )
%PLOTONESTFT Summary of this function goes here
%   Detailed explanation goes here

    [ f, t, p, pt, pb ] = Stft.StftAnalyzer(signal, sr, locs, windowSizeInCycles, overlapPercentage);

    subplot(2, 3, i);
    axTop = PlotMe(p);
    
    subplot(2, 3, i + 3);
    axBottom = PlotMe(pt);

    function ax = PlotMe(pp)
        imagesc(t, f, pp);
        xlabel('Time [sec]');
        ylabel('Frequency [Hz]');

        title(titleTxt);
        ax = gca;
    end
end



