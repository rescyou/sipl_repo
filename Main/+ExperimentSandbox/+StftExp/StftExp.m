close all;
clc;

import(Utils.ImportPackage(mfilename('fullpath')));
load('cleanExperiments_1_4.mat');

windowSizesInCycles = [2 5 10 15 20];
overlapPercentage = 95;

%%
h = figure('units','normalized','outerposition',[0 0 1 1]);

%% exp 1 - same hand
PlotForOneExp(exps(1), windowSizesInCycles, overlapPercentage, ...
    'exp 1: Same hand test', 'iPhone', 'Oximeter 1', 'Oximeter 2');

%% exp 3 - different hand
PlotForOneExp(exps(3), windowSizesInCycles, overlapPercentage, ...
    'exp 3: Different hands test', 'iPhone', 'Oximeter 1 - same hand', 'Oximeter 2 - other hand');
