function [ h ] = PlotForOneExp( cleanExperiment, windowSizesInCycles, overlapPercentage,...
    figTitle, titleIphone, titleOx1, titleOx2 )
%PLOTFORONEEXP Summary of this function goes here
%   Detailed explanation goes here

    import(Utils.ImportPackage(mfilename('fullpath')));
    
    e = cleanExperiment;

    for ws = windowSizesInCycles
        %h = figure('units','normalized','outerposition',[0 0 1 1]);

        [ax1t, ax1b, p_iphone] = PlotOneStft(e.ipSig, e.iphoneSR, e.ipLocs, ws, 1, overlapPercentage, titleIphone);
        [ax2t, ax2b, p_ox1] = PlotOneStft(e.oxSig1, e.oximeterSR, e.oxLocs1, ws, 2, overlapPercentage, titleOx1);
        [ax3t, ax3b, p_ox2] = PlotOneStft(e.oxSig2, e.oximeterSR, e.oxLocs2, ws, 3, overlapPercentage, titleOx2);
        linkaxes([ax1t ax2t ax3t],'xy');
        linkaxes([ax1b ax2b ax3b],'xy');
        txt = [figTitle ' - window size: ' num2str(ws) ' [sec]; overlap: ' num2str(overlapPercentage) '%'];
        suptitle(txt);
        
%         disp(txt);
%         disp('Errors:');
%         
%         mse = @(x,y) sum((y(:)-x(:)) .^ 2) ./ numel(x);
%         errsObjs = {p_iphone, p_ox1, p_ox2};
%         errs = zeros(length(errsObjs));
%         for ii=1:length(errsObjs)
%             for jj=1:length(errsObjs)
%                 xx = XC(errsObjs{ii}, errsObjs{jj});
%                 errs(ii,jj) = max(max(xx));
%             end
%         end
%         disp(num2str(errs))
        
        drawnow
        saveas(gcf, [strrep(figTitle,':','_') ' - window size ' num2str(ws) '.png']);
        %pause(0.75)
    end

end

function xx = XC(x,y)
    if all(size(x) > size(y))
        xx = normxcorr2(y, x);
    else
        xx = normxcorr2(x, y);
    end
end


