function [f, targetMat, resultsMat, confMat, bins, poorMansRmse] = plotConfMat(target, results)
%PLOTCONFMAT Summary of this function goes here
%   Detailed explanation goes here

    bins = unique([target; results]);
    
    targetMat = zeros(length(bins), length(target));
    resultsMat = zeros(length(bins), length(results));
    
    for i = 1:length(bins)
        b = bins(i);
        targetMat(i, target == b) = 1;
        resultsMat(i, results == b) = 1;
    end
    
    f = plotconfusion(targetMat, resultsMat);
    [~, confMat] = confusion(targetMat, resultsMat);
    confMat = confMat ./ sum(confMat, 1);
    imagesc(confMat);
    g = gca;
    g.YTickLabel = [categories(bins); {''}];
    g.XTickLabel = [categories(bins); {''}];
    Utils.MakeFigureFullScreen(f);
    
    poorMansRmse = RMSE(c2d(target), c2d(results))
    stdVal = std(c2d(target) - c2d(results))
    
    g.Title.String = sprintf('%s\nRMSE ~ %g', g.Title.String, poorMansRmse);

end

function [ rmse ] = RMSE( x, y )
    rmse = sqrt(sum((x(:) - y(:)).^2) ./ numel(x));
end

function doubleVector = c2d(categoricalVector)
    doubleVector = str2double(cellstr(categoricalVector));
end
