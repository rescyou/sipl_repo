
testName =

    'Tests\Test___WorkingWithMatsDb_Try1_OnRam_StftImages_---_NN_Training_Sys_classification_Mat_DB_---_reading_from_folder_-_DB_NN_Full_Mats_new4___05-07-2017_21-19-29'


numOfValues =

        1000


numOfClasses =

    10


options = 

  <a href="matlab:helpPopup nnet.cnn.TrainingOptionsSGDM" style="font-weight:bold">TrainingOptionsSGDM</a> with properties:

                     Momentum: 0.9000
             InitialLearnRate: 0.0500
    LearnRateScheduleSettings: [1�1 struct]
             L2Regularization: 0
                    MaxEpochs: 5000
                MiniBatchSize: 20
                      Verbose: 1
             VerboseFrequency: 50
                      Shuffle: 'once'
               CheckpointPath: 'Tests\Test___WorkingWithMatsDb_Try1_OnRam_StftImages_---_NN_Training_Sys_classification_Mat_DB_---_reading_from_folder_-_DB_NN_Full_Mats_new4___05-07-2017_21-19-29\NN_Checkpoint'
         ExecutionEnvironment: 'auto'
                   WorkerLoad: []
                    OutputFcn: @NN.plotTrainingAccuracy

Training on single GPU.
Initializing image normalization.
|=========================================================================================|
|     Epoch    |   Iteration  | Time Elapsed |  Mini-batch  |  Mini-batch  | Base Learning|
|              |              |  (seconds)   |     Loss     |   Accuracy   |     Rate     |
|=========================================================================================|
|            1 |            1 |         0.28 |       2.3096 |       10.00% |       0.0500 |
|            1 |           50 |        12.35 |       2.3026 |        5.00% |       0.0500 |
|            2 |          100 |        25.00 |       2.3026 |        5.00% |       0.0500 |
|            3 |          150 |        36.73 |       2.3026 |        5.00% |       0.0500 |
|            4 |          200 |        47.45 |       2.3026 |        5.00% |       0.0500 |
|            5 |          250 |        58.18 |       2.3026 |        5.00% |       0.0500 |
|            6 |          300 |        68.84 |       2.3026 |        5.00% |       0.0500 |
|            7 |          350 |        79.56 |       2.3026 |        5.00% |       0.0500 |
|            8 |          400 |        90.30 |       2.3026 |        5.00% |       0.0500 |
|            9 |          450 |       101.03 |       2.3026 |        5.00% |       0.0500 |
|           10 |          500 |       111.72 |       2.3026 |        5.00% |       0.0500 |
|           11 |          550 |       122.37 |       2.3026 |        5.00% |       0.0500 |
|           12 |          600 |       132.71 |       2.3026 |        5.00% |       0.0500 |
|           13 |          650 |       143.12 |       2.3026 |        5.00% |       0.0500 |
|           14 |          700 |       153.57 |       2.3026 |        5.00% |       0.0500 |
|           15 |          750 |       164.01 |       2.3026 |        5.00% |       0.0500 |
|           16 |          800 |       174.44 |       2.3026 |        5.00% |       0.0500 |
|           17 |          850 |       185.01 |       2.3026 |        5.00% |       0.0500 |
|           18 |          900 |       195.50 |       2.3026 |        5.00% |       0.0500 |
|           19 |          950 |       206.20 |       2.3026 |        5.00% |       0.0500 |
|           20 |         1000 |       216.66 |       2.3026 |        5.00% |       0.0500 |
|           21 |         1050 |       227.18 |       2.3026 |        5.00% |       0.0500 |
|           22 |         1100 |       237.65 |       2.3026 |        5.00% |       0.0500 |
|           23 |         1150 |       248.09 |       2.3026 |        5.00% |       0.0500 |
|           24 |         1200 |       258.55 |       2.3026 |        5.00% |       0.0500 |
|           25 |         1250 |       269.05 |       2.3026 |        5.00% |       0.0500 |
|           26 |         1300 |       279.47 |       2.3026 |        5.00% |       0.0500 |
|           27 |         1350 |       290.34 |       2.3026 |        5.00% |       0.0500 |
|           28 |         1400 |       300.89 |       2.3026 |        5.00% |       0.0500 |
|           29 |         1450 |       311.41 |       2.3026 |        5.00% |       0.0500 |
|           30 |         1500 |       321.88 |       2.3026 |        5.00% |       0.0500 |
|           31 |         1550 |       332.35 |       2.3026 |        5.00% |       0.0500 |
|           32 |         1600 |       342.79 |       2.3026 |        5.00% |       0.0500 |
|           33 |         1650 |       353.29 |       2.3026 |        5.00% |       0.0500 |
|           34 |         1700 |       363.78 |       2.3026 |        5.00% |       0.0500 |
|           35 |         1750 |       374.23 |       2.3026 |        5.00% |       0.0500 |
|           36 |         1800 |       384.62 |       2.3026 |        5.00% |       0.0500 |
|           37 |         1850 |       395.04 |       2.3026 |        5.00% |       0.0500 |
|           38 |         1900 |       405.47 |       2.3026 |        5.00% |       0.0500 |
|           39 |         1950 |       415.94 |       2.3026 |        5.00% |       0.0500 |
|           40 |         2000 |       426.42 |       2.3026 |        5.00% |       0.0500 |
|           41 |         2050 |       436.86 |       2.3026 |        5.00% |       0.0500 |
|           42 |         2100 |       447.31 |       2.3026 |        5.00% |       0.0500 |
|           43 |         2150 |       457.74 |       2.3026 |        5.00% |       0.0500 |
|           44 |         2200 |       468.14 |       2.3026 |        5.00% |       0.0500 |
|           45 |         2250 |       478.58 |       2.3026 |        5.00% |       0.0500 |
|           46 |         2300 |       489.01 |       2.3026 |        5.00% |       0.0500 |
|           47 |         2350 |       499.45 |       2.3026 |        5.00% |       0.0500 |
|           48 |         2400 |       509.90 |       2.3026 |        5.00% |       0.0500 |
|           49 |         2450 |       520.40 |       2.3026 |        5.00% |       0.0500 |
|           50 |         2500 |       530.82 |       2.3026 |        5.00% |       0.0500 |
|           51 |         2550 |       541.25 |       2.3026 |        5.00% |       0.0500 |
|           52 |         2600 |       551.66 |       2.3026 |        5.00% |       0.0500 |
|           53 |         2650 |       562.17 |       2.3026 |        5.00% |       0.0500 |
|           54 |         2700 |       572.58 |       2.3026 |        5.00% |       0.0500 |
|           55 |         2750 |       583.07 |       2.3026 |        5.00% |       0.0500 |
|           56 |         2800 |       593.55 |       2.3026 |        5.00% |       0.0500 |
|           57 |         2850 |       604.07 |       2.3026 |        5.00% |       0.0500 |
|           58 |         2900 |       614.60 |       2.3026 |        5.00% |       0.0500 |
|           59 |         2950 |       624.99 |       2.3026 |        5.00% |       0.0500 |
|           60 |         3000 |       635.36 |       2.3026 |        5.00% |       0.0500 |
|           61 |         3050 |       645.80 |       2.3026 |        5.00% |       0.0500 |
|           62 |         3100 |       656.27 |       2.3026 |        5.00% |       0.0500 |
|           63 |         3150 |       666.76 |       2.3026 |        5.00% |       0.0500 |
|           64 |         3200 |       677.18 |       2.3026 |        5.00% |       0.0500 |
|           65 |         3250 |       687.63 |       2.3026 |        5.00% |       0.0500 |
|           66 |         3300 |       698.17 |       2.3026 |        5.00% |       0.0500 |
|           67 |         3350 |       708.61 |       2.3026 |        5.00% |       0.0500 |
|           68 |         3400 |       719.04 |       2.3026 |        5.00% |       0.0500 |
|           69 |         3450 |       729.52 |       2.3026 |        5.00% |       0.0500 |
|           70 |         3500 |       739.98 |       2.3026 |        5.00% |       0.0500 |
|           71 |         3550 |       750.39 |       2.3026 |        5.00% |       0.0500 |
|           72 |         3600 |       760.90 |       2.3026 |        5.00% |       0.0500 |
|           73 |         3650 |       771.44 |       2.3026 |        5.00% |       0.0500 |
|           74 |         3700 |       781.82 |       2.3026 |        5.00% |       0.0500 |
|           75 |         3750 |       792.31 |       2.3026 |        5.00% |       0.0500 |
|           76 |         3800 |       802.72 |       2.3026 |        5.00% |       0.0500 |
|           77 |         3850 |       813.19 |       2.3026 |        5.00% |       0.0500 |
|           78 |         3900 |       823.58 |       2.3026 |        5.00% |       0.0500 |
|           79 |         3950 |       834.00 |       2.3026 |        5.00% |       0.0500 |
|           80 |         4000 |       844.43 |       2.3026 |        5.00% |       0.0500 |
|           81 |         4050 |       854.92 |       2.3026 |        5.00% |       0.0500 |
|           82 |         4100 |       865.43 |       2.3026 |        5.00% |       0.0500 |
|           83 |         4150 |       875.86 |       2.3026 |        5.00% |       0.0500 |
|           84 |         4200 |       886.32 |       2.3026 |        5.00% |       0.0500 |
|           85 |         4250 |       896.82 |       2.3026 |        5.00% |       0.0500 |
|           86 |         4300 |       907.23 |       2.3026 |        5.00% |       0.0500 |
|           87 |         4350 |       917.75 |       2.3026 |        5.00% |       0.0500 |
|           88 |         4400 |       928.19 |       2.3026 |        5.00% |       0.0500 |
|           89 |         4450 |       938.74 |       2.3026 |        5.00% |       0.0500 |
|           90 |         4500 |       949.16 |       2.3026 |        5.00% |       0.0500 |
|           91 |         4550 |       959.65 |       2.3026 |        5.00% |       0.0500 |
|           92 |         4600 |       970.38 |       2.3026 |        5.00% |       0.0500 |
|           93 |         4650 |       980.88 |       2.3026 |        5.00% |       0.0500 |
|           94 |         4700 |       991.34 |       2.3026 |        5.00% |       0.0500 |
|           95 |         4750 |      1001.89 |       2.3026 |        5.00% |       0.0500 |
|           96 |         4800 |      1012.36 |       2.3026 |        5.00% |       0.0500 |
|           97 |         4850 |      1022.86 |       2.3026 |        5.00% |       0.0500 |
|           98 |         4900 |      1033.33 |       2.3026 |        5.00% |       0.0500 |
|           99 |         4950 |      1043.80 |       2.3026 |        5.00% |       0.0500 |
|          100 |         5000 |      1054.25 |       2.3026 |        5.00% |       0.0500 |
|          101 |         5050 |      1064.67 |       2.3026 |        5.00% |       0.0500 |
|          102 |         5100 |      1075.12 |       2.3026 |        5.00% |       0.0500 |
|          103 |         5150 |      1085.57 |       2.3026 |        5.00% |       0.0500 |
|          104 |         5200 |      1096.06 |       2.3026 |        5.00% |       0.0500 |
|          105 |         5250 |      1106.54 |       2.3026 |        5.00% |       0.0500 |
|          106 |         5300 |      1117.01 |       2.3026 |        5.00% |       0.0500 |
|          107 |         5350 |      1127.41 |       2.3026 |        5.00% |       0.0500 |
|          108 |         5400 |      1137.83 |       2.3026 |        5.00% |       0.0500 |
|          109 |         5450 |      1148.32 |       2.3026 |        5.00% |       0.0500 |
|          110 |         5500 |      1158.71 |       2.3026 |        5.00% |       0.0500 |
|          111 |         5550 |      1169.19 |       2.3026 |        5.00% |       0.0500 |
|          112 |         5600 |      1179.76 |       2.3026 |        5.00% |       0.0500 |
|          113 |         5650 |      1190.31 |       2.3026 |        5.00% |       0.0500 |
|          114 |         5700 |      1200.77 |       2.3026 |        5.00% |       0.0500 |
|          115 |         5750 |      1211.26 |       2.3026 |        5.00% |       0.0500 |
|          116 |         5800 |      1221.63 |       2.3026 |        5.00% |       0.0500 |
|          117 |         5850 |      1232.05 |       2.3026 |        5.00% |       0.0500 |
|          118 |         5900 |      1242.52 |       2.3026 |        5.00% |       0.0500 |
|          119 |         5950 |      1253.16 |       2.3026 |        5.00% |       0.0500 |
|          120 |         6000 |      1263.90 |       2.3026 |        5.00% |       0.0500 |
|          121 |         6050 |      1274.38 |       2.3026 |        5.00% |       0.0500 |
|          122 |         6100 |      1284.76 |       2.3026 |        5.00% |       0.0500 |
|          123 |         6150 |      1295.21 |       2.3026 |        5.00% |       0.0500 |
|          124 |         6200 |      1305.62 |       2.3026 |        5.00% |       0.0500 |
|          125 |         6250 |      1316.11 |       2.3026 |        5.00% |       0.0500 |
|          126 |         6300 |      1326.56 |       2.3026 |        5.00% |       0.0500 |
|          127 |         6350 |      1337.01 |       2.3026 |        5.00% |       0.0500 |
|          128 |         6400 |      1347.48 |       2.3026 |        5.00% |       0.0500 |
{Operation terminated by user during <a href="matlab:matlab.internal.language.introspective.errorDocCallback('nnet.internal.cnn.util.boundAwayFromZero', 'C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\+util\boundAwayFromZero.m', 11)" style="font-weight:bold">nnet.internal.cnn.util.boundAwayFromZero</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\+util\boundAwayFromZero.m',11,0)">line 11</a>)


In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('nnet.internal.cnn.layer.CrossEntropy/forwardLoss', 'C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\+layer\CrossEntropy.m', 124)" style="font-weight:bold">nnet.internal.cnn.layer.CrossEntropy/forwardLoss</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\+layer\CrossEntropy.m',124,0)">line 124</a>)
            loss = -sum(sum(T.*log(nnet.internal.cnn.util.boundAwayFromZero(Y))) ./numObservations );

In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('nnet.internal.cnn.SeriesNetwork/loss', 'C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\SeriesNetwork.m', 262)" style="font-weight:bold">nnet.internal.cnn.SeriesNetwork/loss</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\SeriesNetwork.m',262,0)">line 262</a>)
            loss = this.Layers{this.indexOutputLayer}.forwardLoss(predictions, response);

In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('nnet.internal.cnn.Trainer/train', 'C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\Trainer.m', 77)" style="font-weight:bold">nnet.internal.cnn.Trainer/train</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\+nnet\+internal\+cnn\Trainer.m',77,0)">line 77</a>)
                    miniBatchLoss = net.loss( predictions, response );

In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('trainNetwork', 'C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\trainNetwork.m', 131)" style="font-weight:bold">trainNetwork</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017a_rel\toolbox\nnet\cnn\trainNetwork.m',131,0)">line 131</a>)
    trainedNet = trainer.train(trainedNet, dispatcher);

In <a href="matlab:matlab.internal.language.introspective.errorDocCallback('WorkingWithMatsDb_Try1_OnRam_StftImages', 'H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\WorkingWithMatsDb_Try1_OnRam_StftImages.m', 36)" style="font-weight:bold">WorkingWithMatsDb_Try1_OnRam_StftImages</a> (<a href="matlab: opentoline('H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\WorkingWithMatsDb_Try1_OnRam_StftImages.m',36,0)">line 36</a>)
} 
load('H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\DB_NN_Full_Mats_new4\data_00000001__win_005__sys_96.8709__dias_53.7565__pulse_100__ovlp_0.95.mat')
size(data.ppg_raw)

ans =

        3750           1

size(data.ppg_raw,1:3)
{Error using <a href="matlab:matlab.internal.language.introspective.errorDocCallback('size')" style="font-weight:bold">size</a>
Dimension argument must be a positive integer scalar within indexing range.
} 
WorkingWithMatsDb_Try2_OnRam_RawSignals
Making folder: Tests\Test_WorkingWithMatsDb_Try2_OnRam_RawSignals.m__NN_Training_Sys_classification_Mat_DB_---_reading_from_folder_-_DB_NN_Full_Mats_new4___05-07-2017_22-58-40
