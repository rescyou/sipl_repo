
testFolderPath =

    'Tests\Test_NN_Run_OnRam_RawSignals.m__NN_Run_OnRam_RawSignals_1__numOfSignals_96000__numOfClasses_20___09-07-2017_20-13-37'

Making folder: Tests\Test_NN_Run_OnRam_RawSignals.m__NN_Run_OnRam_RawSignals_1__numOfSignals_96000__numOfClasses_20___09-07-2017_20-13-37\NN_Checkpoint

index =

     1


options = 

  <a href="matlab:helpPopup nnet.cnn.TrainingOptionsSGDM" style="font-weight:bold">TrainingOptionsSGDM</a> with properties:

                     Momentum: 0.9000
             InitialLearnRate: 7.0000e-04
    LearnRateScheduleSettings: [1�1 struct]
             L2Regularization: 1.0000e-04
                    MaxEpochs: 120
                MiniBatchSize: 100
                      Verbose: 1
             VerboseFrequency: 50
               ValidationData: {[3750�1�1�24000 double]  [24000�1 categorical]}
          ValidationFrequency: 50
           ValidationPatience: 5
                      Shuffle: 'every-epoch'
               CheckpointPath: 'Tests\Test_NN_Run_OnRam_RawSignals.m__NN_Run_OnRam_RawSignals_1__numOfSignals_96000__numOfClasses_20___09-07-2017_20-13-37\NN_Checkpoint'
         ExecutionEnvironment: 'auto'
                   WorkerLoad: []
                    OutputFcn: {@NN.plotTrainingAccuracy  @(info)NN.stopTrainingAtThreshold(info,95)}
                  PlotsToShow: {}

Training on single GPU.
Initializing image normalization.
|=======================================================================================================================|
|     Epoch    |   Iteration  | Time Elapsed |  Mini-batch  |  Mini-batch  |  Validation  |  Validation  | Base Learning|
|              |              |  (seconds)   |     Loss     |   Accuracy   |     Loss     |   Accuracy   |     Rate     |
|=======================================================================================================================|
|            1 |            1 |         0.42 |       2.9991 |        3.00% |       2.9966 |        4.57% |       0.0007 |
|            1 |           50 |        27.20 |       2.9732 |        4.00% |       2.9556 |        7.28% |       0.0007 |
|            1 |          100 |        52.37 |       2.9943 |        7.00% |       2.9411 |        7.26% |       0.0007 |
|            1 |          150 |        77.77 |       2.9250 |        8.00% |       2.9344 |        7.39% |       0.0007 |
|            1 |          200 |       103.19 |       2.9408 |        7.00% |       2.9347 |        7.54% |       0.0007 |
|            1 |          250 |       128.70 |       2.8778 |       13.00% |       2.9306 |        7.58% |       0.0007 |
|            1 |          300 |       187.07 |       2.9518 |        5.00% |       2.9292 |        8.17% |       0.0007 |
|            1 |          350 |       212.38 |       2.9179 |       11.00% |       2.9283 |        8.00% |       0.0007 |
|            1 |          400 |       237.99 |       2.9062 |        9.00% |       2.9284 |        7.73% |       0.0007 |
|            1 |          450 |       263.51 |       2.9513 |        8.00% |       2.9258 |        7.58% |       0.0007 |
|            1 |          500 |       289.14 |       2.9411 |        8.00% |       2.9228 |        7.40% |       0.0007 |
|            1 |          550 |       314.66 |       2.8878 |       13.00% |       2.9211 |        8.33% |       0.0007 |
|            1 |          600 |       340.26 |       2.9258 |        6.00% |       2.9198 |        8.18% |       0.0007 |
|            1 |          650 |       365.80 |       2.9116 |        6.00% |       2.9140 |        8.25% |       0.0007 |
|            1 |          700 |       578.05 |       2.8863 |        8.00% |       2.9133 |        7.89% |       0.0007 |
|            1 |          750 |       823.25 |       2.9544 |        9.00% |       2.9137 |        8.46% |       0.0007 |
|            1 |          800 |       848.86 |       2.9094 |       10.00% |       2.9141 |        7.90% |       0.0007 |
|            1 |          850 |       874.38 |       2.8971 |        8.00% |       2.9106 |        8.20% |       0.0007 |
|            1 |          900 |       899.97 |       2.9376 |        8.00% |       2.9042 |        8.63% |       0.0007 |
|            1 |          950 |       925.59 |       2.9083 |        7.00% |       2.9023 |        8.87% |       0.0007 |
|            2 |         1000 |       951.17 |       2.8642 |       11.00% |       2.9037 |        8.47% |       0.0007 |
|            2 |         1050 |       976.74 |       2.8642 |       13.00% |       2.9062 |        8.32% |       0.0007 |
|            2 |         1100 |      1002.51 |       2.8428 |       11.00% |       2.9087 |        8.08% |       0.0007 |
|            2 |         1150 |      1028.12 |       2.9056 |        9.00% |       2.9002 |        8.58% |       0.0007 |
|            2 |         1200 |      1053.75 |       2.8716 |        9.00% |       2.9037 |        8.77% |       0.0007 |
|            2 |         1250 |      1079.43 |       2.8514 |       12.00% |       2.8976 |        8.95% |       0.0007 |
|            2 |         1300 |      1232.02 |       2.8783 |        7.00% |       2.8830 |        8.95% |       0.0007 |
|            2 |         1350 |      1911.98 |       2.9158 |        7.00% |       2.8832 |        8.59% |       0.0007 |
|            2 |         1400 |      1995.88 |       2.8325 |       11.00% |       2.8812 |        8.51% |       0.0007 |
|            2 |         1450 |      2050.74 |       2.8773 |       11.00% |       2.8920 |        8.93% |       0.0007 |
|            2 |         1500 |      2552.35 |       2.8443 |       11.00% |       2.8742 |        8.68% |       0.0007 |
|            2 |         1550 |      3189.92 |       2.8137 |       10.00% |       2.8697 |        8.75% |       0.0007 |
|            2 |         1600 |      3771.62 |       2.9176 |       10.00% |       2.8664 |        9.28% |       0.0007 |
|            2 |         1650 |      4328.30 |       2.8661 |       10.00% |       2.8702 |        8.65% |       0.0007 |
|            2 |         1700 |      4881.18 |       2.8453 |        9.00% |       2.8681 |        9.17% |       0.0007 |
|            2 |         1750 |      5459.11 |       2.8751 |       11.00% |       2.8781 |        8.30% |       0.0007 |
|            2 |         1800 |      6072.42 |       2.8119 |       10.00% |       2.8587 |        9.69% |       0.0007 |
  <a href="matlab:helpPopup nnet.internal.cnn.util.CNNException" style="font-weight:bold">CNNException</a> with properties:

    UnderlyingCause: [1�1 MException]
         identifier: 'parallel:gpu:device:UnknownCUDAError'
            message: 'An unexpected error occurred during CUDA execution. The CUDA error was:CUDA_ERROR_OUT_OF_MEMORY'
              cause: {[1�1 MException]}
              stack: [3�1 struct]

Error using <a href="matlab:matlab.internal.language.introspective.errorDocCallback('trainNetwork', 'C:\Program Files\MATLAB\R2017b\toolbox\nnet\cnn\trainNetwork.m', 110)" style="font-weight:bold">trainNetwork</a> (<a href="matlab: opentoline('C:\Program Files\MATLAB\R2017b\toolbox\nnet\cnn\trainNetwork.m',110,0)">line 110</a>)
An unexpected error occurred during CUDA execution. The CUDA error was:
CUDA_ERROR_OUT_OF_MEMORY

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('NN_Run_OnRam_RawSignals', 'H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\NN_Run_OnRam_RawSignals.m', 63)" style="font-weight:bold">NN_Run_OnRam_RawSignals</a> (<a href="matlab: opentoline('H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\NN_Run_OnRam_RawSignals.m',63,0)">line 63</a>)
        finalNet = trainNetwork(X, Y, layers, options);

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('WorkingWithMatsDb_Try2_OnRam_RawSignals', 'H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\WorkingWithMatsDb_Try2_OnRam_RawSignals.m', 36)" style="font-weight:bold">WorkingWithMatsDb_Try2_OnRam_RawSignals</a> (<a href="matlab: opentoline('H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\WorkingWithMatsDb_Try2_OnRam_RawSignals.m',36,0)">line 36</a>)
    NN_Run_OnRam_RawSignals(X, YC, Xv, YCv, layers, iopt);

Caused by:
    Error using <a href="matlab:matlab.internal.language.introspective.errorDocCallback('gpuArray/subsasgn')" style="font-weight:bold">gpuArray/subsasgn</a>
    An unexpected error occurred during CUDA execution. The CUDA error was:
    CUDA_ERROR_OUT_OF_MEMORY
  <a href="matlab:helpPopup MException" style="font-weight:bold">MException</a> with properties:

    identifier: 'MATLAB:UndefinedFunction'
       message: 'Undefined function or variable 'finalNet'.'
         cause: {}
         stack: [2�1 struct]

Undefined function or variable 'finalNet'.

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('NN_Run_OnRam_RawSignals', 'H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\NN_Run_OnRam_RawSignals.m', 74)" style="font-weight:bold">NN_Run_OnRam_RawSignals</a> (<a href="matlab: opentoline('H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\NN_Run_OnRam_RawSignals.m',74,0)">line 74</a>)
    YPred = classify(finalNet,X);

Error in <a href="matlab:matlab.internal.language.introspective.errorDocCallback('WorkingWithMatsDb_Try2_OnRam_RawSignals', 'H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\WorkingWithMatsDb_Try2_OnRam_RawSignals.m', 36)" style="font-weight:bold">WorkingWithMatsDb_Try2_OnRam_RawSignals</a> (<a href="matlab: opentoline('H:\Users\shayshi\Documents\rescyou\sipl_repo\Main\WorkingWithMatsDb_Try2_OnRam_RawSignals.m',36,0)">line 36</a>)
    NN_Run_OnRam_RawSignals(X, YC, Xv, YCv, layers, iopt);
