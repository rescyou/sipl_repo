function [ Xtrain, Ytrain, Ytrain_categorical, ...
    Xvalidation, Yvalidation, Yvalidation_categorical, ...
    trainFiles, validationFiles ] = ...
    ToRamStftImages( inputMatDbFilesFolder, numOfValues, numOfClasses, percentOfTrainSet, yParamName )
%TORAMSTFTIMAGES Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 4
        percentOfTrainSet = [];
    end
    
    if nargin < 5
        yParamName = [];
    end
    
    [ Xtrain, Ytrain, Ytrain_categorical, ...
    Xvalidation, Yvalidation, Yvalidation_categorical, ...
    trainFiles, validationFiles ] = ...
    MatFilesDatabase.ToRam.ToRamAux( @MatFilesDatabase.DataElemReaders.DataElem2AdvStftImage, inputMatDbFilesFolder, numOfValues, numOfClasses, percentOfTrainSet, yParamName );

end

