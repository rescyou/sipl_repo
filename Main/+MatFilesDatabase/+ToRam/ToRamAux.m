function [ Xtrain, Ytrain, Ytrain_categorical, ...
    Xvalidation, Yvalidation, Yvalidation_categorical, ...
    trainFiles, validationFiles ] = ...
    ToRamAux( dataElem2ValueFnc, inputMatDbFilesFolder, numOfValues, numOfClasses, percentOfTrainSet, yParamName )
%ToRamAux Summary of this function goes here
%   Detailed explanation goes here

    if nargin < 4 || isempty(percentOfTrainSet)
        percentOfTrainSet = 0.8;
    end
    
    if percentOfTrainSet > 1
        percentOfTrainSet = percentOfTrainSet ./ 100;
    end
    
    if nargin < 6 || isempty(yParamName)
        yParamName = 'systolic';
    end

    files = MatFilesDatabase.ReadAndShuffleFiles(inputMatDbFilesFolder);
    N = length(files);
    
    YtrainAll = [];
    Xtrain = [];
    Xvalidation = [];
    
    N = min(N, numOfValues);
    N_train = floor(N * percentOfTrainSet);
    N_val = N - N_train;
    
    files = files(1:N);
    trainFiles = files(1:N_train);
    validationFiles = files((N_train+1):end);
    
    h1 = waitbar(N, ['Loading to RAM ' num2str(N) ' examples']);
    
    for i=1:N
        waitbar(i/N, h1);
        s = load([inputMatDbFilesFolder '\' files(i).name]);
        data = s.data;
        
        value = dataElem2ValueFnc(data);
        
        if isempty(Xtrain)
            Xtrain = zeros([size(value,1) size(value,2) size(value,3) N_train]);
            Xvalidation = zeros([size(value,1) size(value,2) size(value,3) N_val]);
            YtrainAll = zeros(N, 1);
        end
        
        if i <= N_train
            Xtrain(:,:,:,i) = value;
        else
            Xvalidation(:,:,:,i - N_train) = value;
        end
        
        yValue = data.(yParamName);
        if isempty(yValue)
            yValue = nan;
        end
        YtrainAll(i) = yValue;
    end
    
    close(h1);
    
    Ytrain = YtrainAll(1:N_train);
    Yvalidation = YtrainAll((N_train+1):end);
    
    %% clear nans
    nanIdxesTrain = isnan(Ytrain);
    if any(nanIdxesTrain)
        Ytrain(nanIdxesTrain) = [];
        Xtrain(:, :, :, nanIdxesTrain) = [];
        N_train = length(Ytrain);
    end
    
    nanIdxesValifdation = isnan(Yvalidation);
    if any(nanIdxesValifdation)
        Yvalidation(nanIdxesValifdation) = [];
        Xvalidation(:, :, :, nanIdxesValifdation) = [];
        N_val = length(Yvalidation);
    end
    
    nanIdxesAll = isnan(YtrainAll);
    if any(nanIdxesAll)
        YtrainAll(nanIdxesAll) = [];
        N = length(YtrainAll);
    end
    
    %% make categorical
    YtrainAll_categorical = MatFilesDatabase.PlaceInBuckets(YtrainAll, numOfClasses);
    
    Ytrain_categorical = YtrainAll_categorical(1:N_train);
    Yvalidation_categorical = YtrainAll_categorical((N_train+1):end);

end

