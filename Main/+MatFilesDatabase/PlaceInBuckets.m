function [ bucketedValues ] = PlaceInBuckets( inputData, numberOfBuckets )
%PLACEINBUCKETS quantiz data with lloyds

    [p, c] = lloyds(inputData, numberOfBuckets);
    [i, bucketedValues] = quantiz(inputData, p, c);
    bucketedValues = categorical(bucketedValues)';
    
end

