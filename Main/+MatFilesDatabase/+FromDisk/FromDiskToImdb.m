function [imdb, imdbv] = FromDiskToImdb(inputMatDbFilesFolder, numOfValues, numOfClasses, percentOfTrainSet)
%FROMDISKTOIMDB Summary of this function goes here
%   Detailed explanation goes here

    systolic = getLables(inputMatDbFilesFolder);    
    Y_categorical = MatFilesDatabase.PlaceInBuckets(systolic, numOfClasses);
    
    imdb = imageDatastore(inputMatDbFilesFolder);
    imdb.Labels = Y_categorical;
    imdb = shuffle(imdb);
    
    numOfValues = min(numOfValues, length(imdb.Files));    
    imdb = partition(imdb, ceil(length(imdb.Files)/numOfValues), 1);
    
    if nargin < 4 || isempty(percentOfTrainSet)
        percentOfTrainSet = 0.8;
    end
    
    if percentOfTrainSet > 1
        percentOfTrainSet = percentOfTrainSet ./ 100;
    end
    
    if percentOfTrainSet == 1
        percentOfTrainSet = percentOfTrainSet - eps;
    end
    
    [imdb, imdbv] = splitEachLabel(imdb, percentOfTrainSet, 'randomized');
    

end

function lbls = getLables(imagesFolderPath)
    files = dir(imagesFolderPath);
    files = files(~[files.isdir]);
    N = length(files);
    
    systolic = nan(N, 1);
    
    for fi = 1:N
        filename = files(fi).name(1:end-4);
        parts = strsplit(filename,{'_'});
        systolic(fi) = str2double(parts{6});
    end
    
    lbls = systolic;
end

