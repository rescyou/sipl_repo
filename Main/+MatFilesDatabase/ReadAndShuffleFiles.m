function [ files ] = ReadAndShuffleFiles( folder )
%READANDSHUFFLEFILES get file names from a folder in a random order

    files = dir(folder);
    files = files(~[files.isdir]);
    files = files(randperm(length(files)));

end

