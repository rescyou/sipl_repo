function [ image ] = DataElem2AdvStftImage( dataElem )
%DataElem2AdvStftImage Summary of this function goes here
%   Detailed explanation goes here
    
    image = cat(3, dataElem.stft_abs, dataElem.stft_angle, zeros(size(dataElem.stft_angle)));
    idxs = dataElem.stft_frqs >= PARAMS.CUTOFF_FRQS(1) & dataElem.stft_frqs <= PARAMS.CUTOFF_FRQS(2);
    image = image(idxs, :, :);
end

