function varargout = DrawVerticalLinesOnGraph( y, lineIndexes, samplingRate )
%DrawVerticalLinesOnGraph draws vertical lines on the graph y at
%   lineIndexes

    X = 1:length(y);
    if ~(nargin < 3 || isempty(samplingRate) || samplingRate == 0)
        X = X ./ samplingRate;
    end
    
    h = plot(X, y);
    if nargout >= 1
        varargout{1} = h;
    end
    
    yL = get(gca,'YLim');
    lineIndexes = lineIndexes + 1;
    for i=1:length(lineIndexes)
        line([X(lineIndexes(i)) X(lineIndexes(i))], yL, 'Color', 'r');
    end
    
end

