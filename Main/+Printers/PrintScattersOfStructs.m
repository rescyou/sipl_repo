function PrintScattersOfStructs( structArr1, structArr2, struct1_name, struct2_name, pretext )
%PRINTSCATTERSOFSTRUCT 

    minLen = min(length(structArr1), length(structArr2));
    structArr1 = structArr1(1:minLen);
    structArr2 = structArr2(1:minLen);

    if nargin < 5
        pretext = '';
    end
    if nargin < 3
        struct1_name = inputname(1);
        struct2_name = inputname(2);
    end
    
    fs = fieldnames(structArr1);
    for i = 1:length(fs)
        fn = fs{i};
        
        v1 = {structArr1.(fn)};
        v2 = {structArr2.(fn)};
        
        if isMatrix(v1{1})
            plotMatrixScatter(v1, v2, [pretext, fn]);
            continue;
        end
        
        v1 = [v1{:}];
        v2 = [v2{:}];
        
        if isstruct(v1(1))
            Printers.PrintScattersOfStructs(v1, v2, struct1_name, struct2_name, [pretext, fn, '.']);
            continue;
        end
        plotScatter(v1, v2, [pretext, fn]);
    end

    %% Aux functions
    function res = isMatrix(var)
        res = ismatrix(var) && numel(var) > 1;
    end

    function plotScatter(v1, v2, fieldname)
        figure
        scatter(v1, v2);

        title(['scatter plot of ', fieldname], 'FontSize', 24);
        xlabel(struct1_name, 'FontSize', 18);
        ylabel(struct2_name, 'FontSize', 18);

        %saveas(gcf,['img/sct',num2str(i),'.png']);
    end

    function plotMatrixScatter(v1, v2, fieldname)
        v1 = cellfun(@(x) {x(:)}, v1);
        v1 = [v1{:}];
        
        v2 = cellfun(@(x) {x(:)}, v2);
        v2 = [v2{:}];
        
        for j = 1:size(v1, 1)
            plotScatter(v1(j, :), v2(j, :), [fieldname '(' num2str(j) ')']);
        end
    end
end

