function [ dominantFrq ] = PrintDominantFrq( signal, samplingRate, titleStr, lineColor )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    titleFontSize = 35;
    xlabelFontSize = 23;
    lWidth = 2;
    
    %%
    T = titleStr;
    F = fft(signal);
    fs = samplingRate;
    N = length(F);
    F = abs(F(1:ceil(end/2)));
    xf = ((1:length(F)) - 1) ./ N .* fs;
    
    h = plot(xf, F, 'LineWidth', lWidth);
    if ~(nargin < 4)
        h.Color = lineColor;
    end    
    
    hold on;
    [mY, mX] = max(F);
    mX = xf(mX);
    plot(mX, mY, 'ro', 'LineWidth', lWidth);
    text(mX, mY, ['   ', 'Dominant Frequency = ', num2str(mX), ' Hz'], 'FontSize', 22);
    dominantFrq = mX;
    hold off;

    h = gca;
    h.YTick = [];
    h.XTick = h.XTick(1):(h.XTick(2)-h.XTick(1))/4:h.XTick(end);
    h.LineWidth = 1.5;
    h.FontSize = 18;
    xlabel('frequency [Hz]', 'FontSize', xlabelFontSize);
    title(T, 'FontSize', titleFontSize);

end

