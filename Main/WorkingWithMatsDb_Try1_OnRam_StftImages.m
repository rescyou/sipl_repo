%% params
clc
inputMatFilesFolder = 'DB_NN_Full_Mats_new4';

numOfValues = 6e4
numOfClasses = 20
maxEpocs = 40
percentOfTrainSet = 90

%%
if ~(exist('X', 'var') && exist('Xv', 'var') && (size(X, 4) + size(Xv, 4)) >= numOfValues)
    [X,Y,YC, Xv,Yv,YCv, ~, ~] = MatFilesDatabase.ToRam.ToRamStftImages(inputMatFilesFolder, numOfValues, numOfClasses, percentOfTrainSet);
else
    fprintf('Using %d numOfValues\n', size(X, 4) + size(Xv, 4));
end

%%
imSize = size(X);
imSize = imSize(1:3);
layers = NN.StftDeepNetWithDropout(imSize, numel(unique([YC; YCv])));
%layers = NN.StftDeepNet(imSize, numel(unique([YC; YCv])));
%layers = NN.MakeNN_5L(imSize, numel(unique([YC; YCv])));

%% common options
commonOpts = {
    'MaxEpochs', maxEpocs, ...
    'Momentum', 0.9, ...
    };

if contains(version, 'R2017b')
    commonOpts = [commonOpts {'ValidationPatience', inf,...
        ...%'PlotsToShow', {'training-progress-plot'}...
        }];
end

%% train options
miniBatchSize1 = 128;
miniBatchSize2 = 300;
all_options = [
    
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0.004, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0.04, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0.2, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0.004, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0.04, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize1, ...
        'L2Regularization', 0.2, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0.004, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0.04, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0.2, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.01);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0.004, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0.04, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
    trainingOptions('sgdm', ...
        commonOpts{:}, ...
        'MiniBatchSize', miniBatchSize2, ...
        'L2Regularization', 0.2, ...
        'LearnRateSchedule', 'piecewise', ...
        'LearnRateDropFactor', 0.9, ...
        'InitialLearnRate', 0.004);
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    ];

%%
functionsToAdd = {@NN.plotters.plotAccuracy, ...
        @(info) NN.stopTrainingAtThreshold(info, 95)};
    
for iopt = 1:length(all_options)
    %reset(gpuDevice());
    opt = all_options(iopt);
    NN_Run_OnRam(X, YC, Xv, YCv, layers, opt, ['T1_StftImages_' num2str(iopt)], functionsToAdd);
end
