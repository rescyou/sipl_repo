%%
% with images

%%
clc
getDiaryName = @(x) ['LOGS/Log__' strrep(x,' ','_') '__' datestr(now, 'dd-mm-yyyy_HH-MM-SS') '.txt'];
diary(getDiaryName('NN Training Sys classification Mat DB'));

%%
inputMatFilesFolder = 'DB_NN_Full_Mats_new4_images';
imdb = imageDatastore(inputMatFilesFolder);%,'FileExtensions','.mat');
[ systolic, diastolic, pulse ] = DatabaseContractor.MakeLabels(inputMatFilesFolder);

files = imdb.Files;

systolic = systolic(1:length(files));
goodIdx = ~isnan(systolic);
files = files(goodIdx);
lbl = systolic(goodIdx);

%imdb.ReadFcn = @ReadMyMat;

%%
N = min(1e2, length(lbl));
selectedIdx = randperm(length(lbl), N);
files = files(selectedIdx);
lbl = lbl(selectedIdx);

%%
numberOfBuckets = 25;

[p, c] = lloyds(lbl, numberOfBuckets);
[i, Y_classification] = quantiz(lbl, p, c);
Y_classification = categorical(Y_classification)';

lbl = Y_classification;

%%
imdb.Files = files;
imdb.Labels = lbl;

%%
layers = NN.MakeNN_5L([159 98 3], numel(unique(lbl)));

%% train
options = trainingOptions('sgdm', ...
    'MaxEpochs', 5000, ...
    'MiniBatchSize', 100, ...
    'Momentum', 0.9, ...
    'L2Regularization', 0, ...
    ...%'LearnRateSchedule', 'piecewise', ...
    'LearnRateDropFactor', 0.9, ...
    'InitialLearnRate', 0.05)
finalNet = trainNetwork(imdb, layers, options);



