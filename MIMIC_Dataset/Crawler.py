import urllib.request as ur

baseAddr = 'https://physionet.org/physiobank/database/mimic2wdb/'


def getRecordsAddrs(i):
    i2s = str(i)
    page = ur.urlopen(baseAddr + i2s + '/RECORDS')
    for l in page:
        l = l.decode().strip()
        yield l[:-1], (i2s + '/' + l)


def isGoodRecord(record):
    bAddr = baseAddr + record[1] + record[0]
    layout = bAddr + '_layout.hea'
    n_file = bAddr + 'n.hea'

    try:
        layoutPage = ur.urlopen(layout)
        for ll in layoutPage.readlines():
            line = ll.decode().strip()
            if line.endswith('PLETH'):
                print('found pleth')
                nPage = ur.urlopen(n_file)
                for nl in nPage.readlines():
                    line = nl.decode().strip()
                    if 'NBP' in line:
                        print('found nbp')
                        return True
                break
        #print(layout, n_file)
        #return ('PLETH' in layoutPage) and ('NBP' in nPage)
    except:
        pass
    return False


def countGoodsInRecord(i, outfile):
    c = 0
    j = 0
    for rec in getRecordsAddrs(i):
        if isGoodRecord(rec):
            c += 1
            outfile.write(rec[1] + '\n')
            outfile.flush()
        j += 1
        print(c, j)
    return c

if __name__ == '__main__':
    for i in range(36, 40):
        with open('OUT_' + str(i) + '.txt', 'w') as f:
            print(countGoodsInRecord(i, f))