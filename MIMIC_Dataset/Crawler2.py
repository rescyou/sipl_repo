import urllib.request as ur

baseAddr = 'https://physionet.org/physiobank/database/mimic2wdb/'

BP_TYPES = ('ABP', 'ART', 'CPP', 'CVP', 'FAP', 'ICP', 'LAP', 'PAP', 'RAP', 'UAP', 'UVP')


def getTxtFileName(type):
    return 'OUT_' + type + '.txt'


def initFiles():
    files = []
    for t in BP_TYPES:
        files.append((t, open(getTxtFileName(t), 'w')))
    return files


def closeFiles(files):
    for f in files:
        f[1].close()


def getRecordsAddrs(i):
    i2s = str(i)
    page = ur.urlopen(baseAddr + i2s + '/RECORDS')
    for l in page:
        l = l.decode().strip()
        yield l[:-1], (i2s + '/' + l)


def hasPleth(page):
    for ll in page.readlines():
        line = ll.decode().strip()
        if line.endswith('PLETH'):
            print('found pleth')
            return True
    return False


def findBPs(recordName, page, files):
    for ll in page.readlines():
        line = ll.decode().strip()
        for tt in (t for t in files if line.endswith(t[0])):
            print('found ' + tt[0])
            file = tt[1]
            file.write(recordName + '\n')
            file.flush()


def run():
    files = initFiles()

    for i in range(36, 40):
        for rec in getRecordsAddrs(i):
            runRecord(rec, files)

    closeFiles(files)


def runRecord(record, files):
    bAddr = baseAddr + record[1] + record[0]
    layout = bAddr + '_layout.hea'

    try:
        page = ur.urlopen(layout)
        if hasPleth(page):
            page = ur.urlopen(layout)
            findBPs(record[1], page, files)
    except:
        pass

if __name__ == '__main__':
    run()

