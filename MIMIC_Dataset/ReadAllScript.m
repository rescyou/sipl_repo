files = dir('OUT_*');
filenames = {files.name};

recordsInfos = [];

for i = 1:length(filenames)
    fname = filenames{i};
    tname = fname(5:end-4);
    
    recordNames = [];
    
    fid = fopen(fname);
    tline = fgetl(fid);
    while ischar(tline)
        %disp(tline)
        recordNames = [recordNames {tline}];
        tline = fgetl(fid);
    end
    fclose(fid);
    
    recordsInfo = [];
    recordsInfo.filename = fname;
    recordsInfo.type = tname;
    recordsInfo.recordNames = recordNames;
    
    recordsInfos = [recordsInfos recordsInfo];
end

save('recordsInfos', 'recordsInfos');
