clc
load('signal.mat');

addpath ../PPG_Extraction_Test;

%cycles = cycle_detect(signal);

[ outPPG, ~, cycles, ~ ] = RemoveBaselineAlg( signal);

outFuncs = 0;

for k = 1:length(cycles)-1
    figure
    cycle = outPPG(cycles(k):cycles(k+1));
    X = (1:length(cycle));
    opt = fitoptions('gauss2');
    n = length(cycle);
    m = max(cycle);
    opt.Lower = [0 0 0 0 0 0];
    opt.Upper = [m n n m n n];
    
    fitModel = fit(X', cycle', 'gauss2', opt);
    
    s = sym( formula( fitModel ) );
    cn = coeffnames( fitModel );
    cv = coeffvalues( fitModel );
    cv([2 5]) = cv([2 5]) + double(cycles(k));
    for i = 1:length( cv )
        s = subs( s, cn{i}, cv(i) );
    end
    
    outFuncs = outFuncs + s;
    
    plot(X, cycle, X, fitModel(X))
    legend('cycle','fit');
end

 X = cycles(1):cycles(end);
 %plot(X, outPPG(X), X, eval(subs(outFuncs,X)))
 %legend('cycle','fit');




